const week = [
    {
        "day": "Mon",
        "usage": 2.65,
        
    },
    {
        "day": "Tue",
        "usage": 2.45,
        
    },
    {
        "day": "Wed",
        "usage": 3.5,
        
    },
    {
        "day": "Thu",
        "usage": 2.2,
       
    },
    {
        "day": "Fri",
        "usage": 2.15,
        
    },
    {
        "day": "Sat",
        "usage": 3.65,
        
    },
    {
        "day": "Sun",
        "usage": 2.8,
       
    }

];

const month = [
    { month: "Jan", usage: 56.0 },
    { month: "Feb", usage: 62.0 },
    { month: "Mar", usage: 56.6 },
    { month: "Apr", usage: 59.32 },
    { month: "May", usage: 55.0 },
    { month: "Jun", usage: 60.0 },
    { month: "Jul", usage: 65.0 },
    { month: "Aug", usage: 61.2 },
    { month: "Sep", usage: 54.0 },
    { month: "Oct", usage: 55.45 },
    { month: "Nov", usage: 53.52 },
    { month: "Dec", usage: 54.7 }
];



module.exports = month;
// module.exports = month;
// need to send both
