
// import node modules
var express = require('express');
const path=require("path");
const mongoose=require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const socket=require('socket.io');
const fs = require('fs')


// importing routers
const leaderBoard=require('./routers/leaderboard');
const user=require('./routers/user');
const dailyintake=require('./routers/dailyintake');
const admin=require('./routers/Admin/admin');
const group=require('./routers/group');
const dashboard=require('./routers/dashboard');
const adminDashboard = require("./routers/adminDashboard");





// creating express server
const app = express();

// // initialize 3rd party modules 
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// database connection
// mongodb+srv://aqua:<password>@cluster0-cicni.gcp.mongodb.net/test?retryWrites=true&w=majority
mongoose.connect('mongodb+srv://aqua:admin@123@cluster0-cicni.gcp.mongodb.net/aqua?retryWrites=true&w=majority',{useNewUrlParser: true,useUnifiedTopology: true,}).
then(()=>{
    console.log("Database conneted");
}).
catch(err=>{
    console.error(err);
});


// initialize port
const port=process.env.PORT || 3000 ;
//const port=3000;



// listening given port and make soket connectio on given port
// const server=
var io=socket(app.listen(port,function(){
    console.log("listening")
}));

// +++++++++++++++++++++++++++++++++++++++++ Socket.io +++++++++++++++++++++++++++++++++++++++++++++++++++


io.on("connection",(userSocket)=>{
//    io.on("")
console.log(userSocket.id); 
userSocket.emit("news","HIIIII");
userSocket.on("message",data=>{
    console.log(data);
});
});






// +++++++++++++++++++++++++++++++++++++++++ Socket.io +++++++++++++++++++++++++++++++++++++++++++++++++++



// +++++++++++++++++++++++++++++++++++++++++ coding area +++++++++++++++++++++++++++++++++++++++++++++++++++




// use routers
app.use('/api/leaderBoard/',leaderBoard);
app.use('/api/admin/',admin);
app.use('/api/user/',user);
app.use('/api/group/',group);
app.use('/api/dailyintake/',dailyintake);
app.use('/api/sensors',require('./routers/sensors'));
app.use('/api/history', require('./routers/history'));
app.use("/api/dashboard", dashboard);
app.use("/api/adminDashboard", adminDashboard);

app.use("/api/feedback", require('./routers/feedback'));
app.use('/api/getfile/:path/:name',(req,res)=>{
    // var image=new ("./images/groups/Aqua commaaaa.jpg");
    console.log(req.params);
    try{
        if(fs.existsSync(__dirname+'/images/'+req.params.path+"/"+req.params.name)){
            res.sendFile(__dirname+'/images/'+req.params.path+"/"+req.params.name);
        }else{
            console.log("huu");
            res.sendFile(__dirname+'/images/'+"empty.jpg");
        }
    }catch(err){
        console.log("hii"+err);
        res.sendFile(__dirname+'/images/'+"empty.jpg");
    }

        
    
    
});

app.use('/*',(req,res)=>{
    console.log("hi")
    // res.sendFile(__dirname+'/site/index.html');
    res.send("hello from Aqua");
    
});

// +++++++++++++++++++++++++++++++++++++++++ coding area +++++++++++++++++++++++++++++++++++++++++++++++++++
module.exports = app; 
