const mongoose=require('mongoose');


const Schema = mongoose.schema;

const AskQuestionSchema=mongoose.Schema({
    userId:{type:String,require:true},
    question:{type:String,require:true},
    type:{type:String,require:true},
    status:{type:Boolean,require:true}
});
      
const AskQuestion=module.exports=mongoose.model("AskQuestion",AskQuestionSchema);