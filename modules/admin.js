const mongoose=require('mongoose');


const Schema = mongoose.schema;

const adminSchema=mongoose.Schema({
    name:{type:String,require:true},
    email:{type:String,require:true},
    password:{type:String,require:true}
});
      
const User=module.exports=mongoose.model("Admin",adminSchema);