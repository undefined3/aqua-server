const mongoose=require('mongoose');


const Schema = mongoose.schema;

const WaterRequirementSchema=mongoose.Schema({
    userId:{type:String,require:true},
    dailyTarget:{type:String,require:true},
    dailyUsage:{type:String,require:true},
    date:{type:String,require:true},
    shedule:{type:Array,require:true},
    breakfastTime:{type:String,require:true},
    lunchTime:{type:String,require:true},
    dinnerTime:{type:String,require:true},
    wakupTime:{type:String,require:true},
    sleepTime:{type:String,require:true},

});
      
const WaterRequirement=module.exports=mongoose.model("WaterRequirement",WaterRequirementSchema);