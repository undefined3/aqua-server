const mongoose = require("mongoose");

const Schema = mongoose.schema;

const userSchema = mongoose.Schema({
  name: { type: String, require: true },
  email: { type: String, require: true },
  password: { type: String, require: true },
  weight: { type: String, require: true },
  height: { type: String, require: true },
  gender: { type: String, require: true },
  breakfastTime: { type: String, require: true },
  lunchTime: { type: String, require: true },
  dinnerTime: { type: String, require: true },
  wakupTime: { type: String, require: true },
  sleepTime: { type: String, require: true },
  birthday: { type: String, require: true },
  CKD: { type: String, require: true },
  groupId: { type: String, require: true },
  activityLevel: { type: String, require: true },
  barcode: { type: String },
});

const User = (module.exports = mongoose.model("User", userSchema));

module.exports.getAllUsers = function (callback) {
  User.find(callback);
};

module.exports.getUserById = function (id, callback) {
  User.findById(id, callback);
};
