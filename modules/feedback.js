const mongoose=require('mongoose');


const Schema = mongoose.schema;

const FeedbackSchema=mongoose.Schema({
    name:{type:String,require:true},
    email:{type:String,require:true},
    feedback:{type:String,require:true},
    status:{type:Boolean,require:true},
});
      
const Feedback=module.exports=mongoose.model("Feedback",FeedbackSchema);