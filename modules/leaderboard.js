const mongoose=require('mongoose');


const Schema = mongoose.schema;

const LeaderBoardSchema=mongoose.Schema({
    date:{type:String,require:true},
    groupId:{type:String,require:true},
    userId:{type:String,require:true},
    score:{type:String,require:true}
});
      
const LeaderBoard=module.exports=mongoose.model("LeaderBoard",LeaderBoardSchema);
