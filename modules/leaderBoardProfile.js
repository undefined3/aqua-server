const mongoose=require('mongoose');


const Schema = mongoose.schema;

const LeaderBoardProfileSchema=mongoose.Schema({    
    userId:{type:String,require:true},
    userName:{type:String,require:true},
    score:{type:String,require:true},
    lastUpdate:{type:String,require:true},
    rank:{type:String,require:true},
    badge:{type:String,require:true},
    numberOfDays:{type:String,require:true},
    avatar:{type:String,require:true},
    groupId:{type:String,require:true},
    groupStatus:{type:Boolean,require:true}
});
      
const LeaderBoardProfile=module.exports=mongoose.model("LeaderBoardProfile",LeaderBoardProfileSchema);