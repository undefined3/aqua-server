const mongoose = require("mongoose");

const Schema = mongoose.schema;

const GroupSchema = mongoose.Schema({
  image: { type: String, require: true },
  groupName: { type: String, require: true },
  members: [
    {
      id: { type: String, require: true },
      status: { type: Boolean, require: true },
    },
  ],
  admin: { type: String, require: true },
});

const Group = (module.exports = mongoose.model("Group", GroupSchema));

module.exports.getAllGroups = function (callback) {
  Group.find(callback);
};
