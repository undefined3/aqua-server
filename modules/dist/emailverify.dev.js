"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.schema;
var EmailCodeSchema = mongoose.Schema({
  email: {
    type: String,
    require: true
  },
  code: {
    type: String,
    require: true
  }
});
var EmailCode = module.exports = mongoose.model("EmailCode", EmailCodeSchema);