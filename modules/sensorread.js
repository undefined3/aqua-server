const mongoose=require('mongoose');


const Schema = mongoose.schema;

const SensorReadSchema=mongoose.Schema({
    userId:{type:String,require:true},
    measurement:[{
        time:{type:String,require:true},
        waterLevel:{type:String,require:true},
        // phValue:{type:String,require:true},
        // chlorideValue:{type:String,require:true},
    }],
    date:{type:String,require:true},
    beverage:[{
        type:{type:String,require:true},
        amount:{type:String,require:true}
    }],


});
      
const SensorRead=module.exports=mongoose.model("SensorRead",SensorReadSchema);

