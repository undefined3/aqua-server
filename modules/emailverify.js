const mongoose=require('mongoose');


const Schema = mongoose.schema;

const EmailCodeSchema=mongoose.Schema({
    email:{type:String,require:true},
    code:{type:String,require:true},
});
      
const EmailCode=module.exports=mongoose.model("EmailCode",EmailCodeSchema);

