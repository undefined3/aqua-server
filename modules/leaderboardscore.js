const mongoose=require('mongoose');


const Schema = mongoose.schema;

const LeaderBoardScoreSchema=mongoose.Schema({    
    groupId:{type:String,require:true},
    type:{type:String,require:true},
    members:[{
        id:{type:String,require:true},
        score:{type:String,require:true}, 
    }],
    lastUpdate:{type:String,require:true},
});
      
const LeaderBoardScore=module.exports=mongoose.model("LeaderBoardScore",LeaderBoardScoreSchema);