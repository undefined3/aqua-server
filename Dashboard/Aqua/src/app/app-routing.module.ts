import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Admin/login/login.component';
import { LandingpageComponent } from './Admin/landingpage/landingpage.component';
import { DashboardComponent } from './Admin/dashboard/dashboard.component';
import { RegisterComponent } from './Admin/register/register.component';
import { HomeComponent } from './web/home/home.component';
import { AboutUsComponent } from './web/about-us/about-us.component';
import { FeaturesComponent } from './web/features/features/features.component';
import { ContactUsComponent } from './web/contact-us/contact-us.component';



const routes: Routes = [
  {path:"",component:HomeComponent},
  // {path:"aboutus",component:AboutUsComponent},
  // {path:"login",component:LoginComponent},
  // {path:"admin",component:LandingpageComponent,children:[
  //   {path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  //   {path:"dashboard",component:DashboardComponent, pathMatch: 'full'},
  //   {path:"addadmin",component:RegisterComponent},

  // ]},
  {path:"features", component:FeaturesComponent},
  {path:"contactus", component:ContactUsComponent},
  {path:"aboutus", component: AboutUsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
