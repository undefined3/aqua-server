
export class ContactUsModel{
    constructor(
    public contactId:Number,
    public title:String,
    public firstName:String,
    public lastName:String,
    public email:String,
    public feedback:String,
    
    public acceptTerms:Boolean,
    
    
  ){}
  }