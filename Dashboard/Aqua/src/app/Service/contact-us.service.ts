import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ContactUsService {
url: string = 'https://aquabackend.herokuapp.com/api/feedback/add';
constructor(private http: HttpClient) { }

  sendMessage(messageContent: any) {
    console.log("sendmessage service");
    return this.http.post(this.url,messageContent);
  }
}
