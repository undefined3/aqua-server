import { Injectable } from '@angular/core';
import {ServerDetail} from '../shared/backend';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // api=ServerDetail()
  constructor( private http: HttpClient) { }
  login(value){
    let url="/api/admin/login";
    return this.http.post(url,value);
  }
  register(value){
    let url="/api/admin/register";
    return this.http.post(url,value);
  }
}
