import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Admin/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


// material imports
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { LandingpageComponent } from './Admin/landingpage/landingpage.component';
import { SidebarComponent } from './Admin/sidebar/sidebar.component';
import { NavbarComponent } from './Admin/navbar/navbar.component';
import { HomeComponent } from './web/home/home.component';
import { AboutUsComponent } from './web/about-us/about-us.component';
import { DashboardComponent } from './Admin/dashboard/dashboard.component';
import { RegisterComponent } from './Admin/register/register.component';
import { FooterComponent } from './web/footer/footer.component';
import { ContactUsComponent } from './web/contact-us/contact-us.component';
// import {MatFormFieldModule} from '@angular/material/form-field';
//import { ChartsModule } from "ng2-charts";
import { FeaturesComponent } from './web/features/features/features.component';
import { IntroComponent } from './web/features/intro/intro.component';
import { ChartsComponent } from './web/features/charts/charts.component';
import { LeaderboardComponent } from './web/features/leaderboard/leaderboard.component';
import { ProfileComponent } from './web/features/profile/profile.component';
import { DashFeaturesComponent } from './web/features/dash-features/dash-features.component';
import { NormalNavComponent } from './web/normal-nav/normal-nav.component';
import { HeadingComponent } from './web/about-us/heading/heading.component';
import { HeaderComponent } from './web/contact-us/header/header.component';


// import {MatFormFieldModule} from '@angular/material/form-field';
// import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingpageComponent,
    SidebarComponent,
    NavbarComponent,
    HomeComponent,
    AboutUsComponent,
    DashboardComponent,
    RegisterComponent,
    FooterComponent,
    ContactUsComponent,
    FeaturesComponent,
    IntroComponent,
    ChartsComponent,
    LeaderboardComponent,
    ProfileComponent,
    DashFeaturesComponent,
    NormalNavComponent,
    HeadingComponent,
    HeaderComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // ChartsModule,
    // Mat modules
    FormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
