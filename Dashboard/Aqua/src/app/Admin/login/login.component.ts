import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {AuthService} from '../../Service/auth.service';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm=new FormGroup({
    email:new FormControl(''),
    password:new FormControl(''),
  });
  error=false;
  visisbility=true;
  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit() {
  }
  submit(){
    if(!this.loginForm.valid){
      return;
    }
    this.auth.login(this.loginForm.value).subscribe((data:any)=>{
      console.log(data);
      if(data.status){
        this.router.navigate(['/admin'])
      }else{
        this.error=true;
      }
    },error=>{
      console.log(error);
    });
    console.log(this.loginForm.value)
  }

}
