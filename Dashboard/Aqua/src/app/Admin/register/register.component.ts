import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/Service/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm=new FormGroup({
    name:new FormControl(''),
    email:new FormControl(''),
    password:new FormControl(''),
    repassword:new FormControl(''),
  });

  visisbility=true;
  error1=false;
  error2=false;
  constructor(private auth:AuthService,private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }
  addUser(){
    console.log(this.registerForm.value)
    if(!this.registerForm.valid){
      return;
    }else if(this.registerForm.value.password!=this.registerForm.value.repassword){
      this.error1=true;
      return;
    }else{
      this.error1=false;
      this.error2=false;
    }
    this.auth.register( this.registerForm.value).subscribe((data:any)=>{
      if(data.status){
        this._snackBar.open("Success", "close", {
          duration: 2000,
        });
        setTimeout(()=>{
          // window.location.reload();
        });
      }
    },error=>{
      console.log(error);
      this.error2=true;
    })

  }

}
