import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashFeaturesComponent } from './dash-features.component';

describe('DashFeaturesComponent', () => {
  let component: DashFeaturesComponent;
  let fixture: ComponentFixture<DashFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
