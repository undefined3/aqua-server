import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalNavComponent } from './normal-nav.component';

describe('NormalNavComponent', () => {
  let component: NormalNavComponent;
  let fixture: ComponentFixture<NormalNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
