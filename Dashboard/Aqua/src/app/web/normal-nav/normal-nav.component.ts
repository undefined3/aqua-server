import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-normal-nav',
  templateUrl: './normal-nav.component.html',
  styleUrls: ['./normal-nav.component.css']
})
export class NormalNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  activeChange(index){
    console.log("jiiii")
    var home=document.getElementById("home");
    var aboutus=document.getElementById("aboutus");
    var contactus=document.getElementById("contactus");
    var features=document.getElementById("features");
    aboutus.classList.remove("active");
    home.classList.remove("active");
    contactus.classList.remove("active");
    features.classList.remove("active");
    if(index==1){
      home.classList.add("active")
    }else if(index==2){
      aboutus.classList.add("active")
    }else if(index==3){
      features.classList.add("active")
    }else if(index==4){
      contactus.classList.add("active")
    }
  }
}
