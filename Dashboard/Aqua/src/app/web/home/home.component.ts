import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  slider=[true,false,false];
  i=0;
  constructor() {
    setInterval(()=>{
      this.slider=[false,false,false];
      this.slider[this.i%3]=true;
      this.i++;
    },6000);

  }
  sliderNext(){
    console.log(this.i%3);
    this.slider=[false,false,false];
    this.slider[this.i%3]=true;
    this.i++;
  }
  sliderPre(){
    this.slider=[false,false,false];
    this.slider[this.i%3]=true;
    if(this.i==0){
      this.i=2;
    }else{
      this.i++;
    }

  }

  ngOnInit() {
  }

}
