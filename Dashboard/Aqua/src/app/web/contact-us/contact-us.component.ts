import { Component, OnInit, HostListener } from '@angular/core';
// import { ConnectionService } from 'connection-service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactUsService } from '../../Service/contact-us.service';
// import { MustMatch } from '../helpers/must-match.validator';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent  {
  registerForm: FormGroup;
  submitted = false;

disabledSubmitButton: boolean = true;
optionsSelect: Array<any>;


  // constructor(private formBuilder: FormBuilder) { }
  @HostListener('input') oninput() {

    if (this.registerForm.valid) {
      this.disabledSubmitButton = false;
      }
    }


    constructor(private fb: FormBuilder, private contactUsService: ContactUsService) {

  this.registerForm = fb.group({
          // title: ['', Validators.required],
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          feedback: ['', Validators.required],
          // password: ['', [Validators.required, Validators.minLength(6)]],
          // confirmPassword: ['', Validators.required],

          acceptTerms: [false, Validators.requiredTrue]
      }, {
        // validator: MustMatch('password', 'confirmPassword')
      });
    }


  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }








  onSubmit() {
      this.submitted = true;
      console.log('onsubmit');
      console.log(this.registerForm.value)
      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }
      console.log(this.registerForm.value)

      this.contactUsService.sendMessage(this.registerForm.value).subscribe((data) => {
        alert('Your message has been sent.');
        this.registerForm.reset();
        this.registerForm.valid;
        location.reload();
        // this.disabledSubmitButton = true;
      }, error => {
        console.log('Error', error);
      });



      // alert('SUCCESS!!\n\n');
      // display form values on success
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));

    }
  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }
}
