const express = require('express');
const nodemailer = require('nodemailer');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');

const transporter = nodemailer.createTransport({

  host: 'smtp.gmail.com',
  provider: 'gmail',
  port: 465,
  secure: true,
  auth: {
    user: ' ', // Enter here email address from which you want to send emails
    pass: ' ' // Enter here password for email account from which you want to send emails
  },
  tls: {
  rejectUnauthorized: false
  }
});

app.use(bodyParser.json());

app.use(function (req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/send', function (req, res) {

  console.log("node");

  let title = req.body.title;
  let firstName = req.body.firstName;
  let lastName = req.body.lastName;
  let senderEmail = req.body.email;
  let feedback = req.body.feedback;
  let acceptTerms = req.body.acceptTerms;

  let mailOptions = {
    to: [' info@aqua.com'], // Enter here the email address on which you want to send emails from your customers
    // subject: title,
    from: firstName ,
    
    text: feedback,
    replyTo: senderEmail
  };

  if (firstName === '') {
    res.status(400);
    res.send({
    message: 'Bad request'
    });
    return;
  }

  if (lastName === '') {
    res.status(400);
    res.send({
    message: 'Bad request'
    });
    return;
  }

  if (senderEmail === '') {
    res.status(400);
    res.send({
    message: 'Bad request'
    });
    return;
  }

  if (title === '') {
    res.status(400);
    res.send({
    message: 'Bad request'
    });
    return;
  }

  if (feedback === '') {
    res.status(400);
    res.send({
    message: 'Bad request'
    });
    return;
  }

  if (acceptTerms) {
    mailOptions.to.push(senderEmail);
  }

  transporter.sendMail(mailOptions, function (error, response) {
    if (error) {
      console.log(error);
      res.end('error');
    } else {
      console.log('Message sent: ', response);
      res.end('sent');
    }
  });
});

app.listen(port, function () {
  console.log('Express started on port: ', port);
});