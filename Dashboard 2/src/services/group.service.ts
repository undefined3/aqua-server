import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";


@Injectable({
  providedIn: "root",
})
export class GroupService {
  constructor(private http: HttpClient) {}

  baseUri = "https://aquabackend.herokuapp.com/api/group";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  getAllGroups() {
    return this.http.get(`${this.baseUri}/groups`);
  }
}
