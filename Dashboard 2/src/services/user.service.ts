import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private http: HttpClient) {}

  baseUri = "https://aquabackend.herokuapp.com/api/user";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  getAllUsers() {
    return this.http.get(`${this.baseUri}/users`);
  }

  getUser(id) {
    return this.http.get(`${this.baseUri}/userbyid/${id}`);
  }
}
