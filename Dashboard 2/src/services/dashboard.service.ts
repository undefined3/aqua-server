import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  baseUri = "https://aquabackend.herokuapp.com/api/adminDashboard";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  getWeather(country) {
    return this.http.post(`${this.baseUri}/weather`,country);

  }

  getSummary(){
    return this.http.get(`${this.baseUri}/summary`);
  }
  getLeadboard(){
    return this.http.get(`${this.baseUri}/leadboard`);
  }


}
