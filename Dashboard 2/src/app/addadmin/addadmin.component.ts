import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-addadmin',
  templateUrl: './addadmin.component.html',
  styleUrls: ['./addadmin.component.scss']
})
export class AddadminComponent implements OnInit {

  registerForm=new FormGroup({
    name:new FormControl('',[
      Validators.required,
    ]),
    email:new FormControl('',[
      Validators.required,
      Validators.email
      // Validators.pattern("^[a-z]+[A-Z]+[@#%*&]+[a-z0-9.-]+\\.[a-z]{2,4}$")
    ]),
    password:new FormControl('',[
      Validators.required,
      Validators.minLength(8),
    ]),
    repassword:new FormControl('',
    [
      Validators.required,
      Validators.minLength(8),
    ]),
  });
  loader=false;
  constructor(
    private auth:AuthService,
    private _snackBar: MatSnackBar,
    ) { }



  getval(val){
    return this.registerForm.get(val);
  }
  ngOnInit() {
  }
  submit(){
    if(this.registerForm.invalid){
      this.getval("name").markAllAsTouched();
      this.getval("email").markAllAsTouched();
      this.getval("password").markAllAsTouched();
      this.getval("repassword").markAllAsTouched();
      return;

    }
    this.loader=true;
    this.auth.register(this.registerForm.value).subscribe((data:any)=>{
      if(data.status){
        this._snackBar.open("Successfully Registered", "close", {
          duration: 2000,
        });
        this.registerForm.reset();
        this.loader=false;

      }else{
        this._snackBar.open("Something went wrong", "close", {
          duration: 2000,
        });
        this.loader=false;

      }
    },err=>{
      this._snackBar.open("Something went wrong", "close", {
        duration: 2000,
      });
      this.loader=false;
    });
  }

}
