import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isloged=false;
  userDetail={name:"",email:""};

  constructor(
    private http: HttpClient,
    private router:Router
    ){
    var user=localStorage.getItem("user")
    if(localStorage.getItem("user")){
      this.isloged=true
      let temp=JSON.parse(user);
      this.userDetail.email=temp.email;
      this.userDetail.name=temp.name;
    }
  }
  getName(){
    return this.userDetail.name;
  }

  getEmail(){
    return this.userDetail.email;
  }
  public login(value){
    this.isloged=true;
    let url="https://aquabackend.herokuapp.com/api/admin/login";
    return this.http.post(url,value);
  }

  register(value){
    let url="https://aquabackend.herokuapp.com/api/admin/register";
    return this.http.post(url,value);
  }
  logout(){
    this.isloged=false;
    localStorage.removeItem("user");
    this.router.navigate(['/login']);

  }
  isLoged(){
    return this.isloged;
  }
}
