import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { FeedbackService } from '../feedback.service';
import {MatSnackBar} from '@angular/material/snack-bar';




@Component({
  selector: 'app-contactus-view',
  templateUrl: './contactus-view.component.html',
  styleUrls: ['./contactus-view.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ContactusViewComponent implements OnInit {
  dataSource1 =[];
  columnsToDisplay1 = ['Name', 'email', 'feedback'];
  dataSource2 =[];
  columnsToDisplay2 = ['Name', 'Type', 'Question'];
  expandedElement: PeriodicElement | null;
  loading=true;

  constructor(
    private feedback:FeedbackService,
    private _snackBar: MatSnackBar,
    ) {
    this.feedback.getAllFeedback().subscribe((data:any)=>{
      console.log(data);
      for(var i in data.data){
        this.dataSource1.push({
          Name:data.data[i].name,
          email:data.data[i].email,
          feedback:data.data[i].feedback,
          id:data.data[i]._id
        });
      }
      this.loading=false;
    });
    this.feedback.getAllQuestion().subscribe((data:any)=>{
      console.log(data);
      for(var i in data.data){
        this.dataSource2.push({
          Name:data.data[i].name,
          Type:data.data[i].type,
          Question:data.data[i].question,
          id:data.data[i]._id
        });
      }
      this.loading=false;
    });
   }

  ngOnInit() {
  }
  read(element){
    console.log(element);
    this.loading=true;
    this.feedback.updateFeedback(element.id).subscribe((data:any)=>{
      if(data.status){
        this.dataSource1=[];

        for(var i in data.data){
          this.dataSource1.push({
            Name:data.data[i].name,
            email:data.data[i].email,
            feedback:data.data[i].feedback,
            id:data.data[i]._id
          });
        }
        this.loading=false;
        this._snackBar.open("Success", "close", {
          duration: 2000,
        });
      }else{
        this.loading=false;
        this._snackBar.open("Somthing went wrong", "close", {
          duration: 2000,
        });
      }

    },err=>{
      this.loading=false;
        this._snackBar.open("Somthing went wrong", "close", {
          duration: 2000,
        });
    })

  }
  read2(element){
    console.log(element);
    this.loading=true;
    this.feedback.updateQuestion(element.id).subscribe((data:any)=>{
      if(data.status){
        this.dataSource2=[];

        for(var i in data.data){
          this.dataSource2.push({
            Name:data.data[i].name,
            Type:data.data[i].type,
            Question:data.data[i].question,
            id:data.data[i]._id
          });
        }
        this.loading=false;
        this._snackBar.open("Success", "close", {
          duration: 2000,
        });
      }else{
        this.loading=false;
        this._snackBar.open("Somthing went wrong", "close", {
          duration: 2000,
        });
      }

    },err=>{
      this.loading=false;
        this._snackBar.open("Somthing went wrong", "close", {
          duration: 2000,
        });
    })
  }

}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
  description: string;
}


