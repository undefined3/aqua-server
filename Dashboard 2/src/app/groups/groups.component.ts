import { Component, OnInit } from "@angular/core";
import { GroupService } from "src/services/group.service";

@Component({
  selector: "app-groups",
  templateUrl: "./groups.component.html",
  styleUrls: ["./groups.component.scss"],
})
export class GroupsComponent implements OnInit {
  groups = ([] = []);
  constructor(private groupService: GroupService) {
    this.groupService.getAllGroups().subscribe((data) => {
      data["data"].forEach((element) => {
        this.groups.push(element);
      });
    });
  }

  ngOnInit() {}
}
