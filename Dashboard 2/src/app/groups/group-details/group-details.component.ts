import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-group-details",
  templateUrl: "./group-details.component.html",
  styleUrls: ["./group-details.component.scss"],
})
export class GroupDetailsComponent implements OnInit {
  group;

  constructor(private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as { group };
    this.group = state.group;

    console.log(this.group.members);
  }

  ngOnInit() {}

  // members = this.group.members;
}
