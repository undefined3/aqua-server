import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "src/services/user.service";

@Component({
  selector: "app-group-member-card",
  templateUrl: "./group-member-card.component.html",
  styleUrls: ["./group-member-card.component.scss"],
})
export class GroupMemberCardComponent implements OnInit {
  @Input() member;

  constructor(private userService: UserService) {}

  name: string;
  email: string;

  ngOnInit() {
    // console.log(this.member);

    this.userService.getUser(this.member.id).subscribe((data) => {
      this.name = data["data"].name;
      this.email = data["data"].email;
    });
  }
}
