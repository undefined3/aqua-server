import { Component, OnInit, Input } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";

@Component({
  selector: "app-group-card",
  templateUrl: "./group-card.component.html",
  styleUrls: ["./group-card.component.scss"],
})
export class GroupCardComponent implements OnInit {
  @Input() group;

  constructor(private router: Router) {}

  ngOnInit() {}

  showGroup() {
    const navigationExtras: NavigationExtras = {
      state: { group: this.group },
    };
    this.router.navigate(["group-details"], navigationExtras);
  }
}
