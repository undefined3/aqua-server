import { Component, OnInit } from "@angular/core";
import { ViewEncapsulation } from "@angular/core";
import { DashboardService } from "src/services/dashboard.service";
import { first } from "rxjs/operators";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["../app.component.scss", "./dashboard.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit {
  weather;
  forecast;
  summary;
  users

  constructor(private dashboardService: DashboardService) {
    this.dashboardService.getWeather({ city: "Colombo" }).subscribe((data) => {
      this.weather = data[0].current;
      this.forecast = data[0].forecast;
    });
    this.dashboardService.getSummary().subscribe((data) => {

      this.summary = data;
    });
    this.dashboardService.getLeadboard().subscribe(data=>{
      console.log(data);
      this.users=data;
    })
  }

  ngOnInit() {}
}
