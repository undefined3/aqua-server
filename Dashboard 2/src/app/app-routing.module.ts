import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { DashboardComponent } from "./dashboard/dashboard.component";
import { FormsComponent } from "./forms/forms.component";
import { ButtonsComponent } from "./buttons/buttons.component";
import { TablesComponent } from "./tables/tables.component";
import { IconsComponent } from "./icons/icons.component";
import { TypographyComponent } from "./typography/typography.component";
import { AlertsComponent } from "./alerts/alerts.component";
import { AccordionsComponent } from "./accordions/accordions.component";
import { BadgesComponent } from "./badges/badges.component";
import { ProgressbarComponent } from "./progressbar/progressbar.component";
import { BreadcrumbsComponent } from "./breadcrumbs/breadcrumbs.component";
import { PaginationComponent } from "./pagination/pagination.component";
import { DropdownComponent } from "./dropdown/dropdown.component";
import { TooltipsComponent } from "./tooltips/tooltips.component";
import { CarouselComponent } from "./carousel/carousel.component";
import { TabsComponent } from "./tabs/tabs.component";
import { UsersComponent } from "./users/users.component";
import { GroupsComponent } from "./groups/groups.component";
import { GroupDetailsComponent } from "./groups/group-details/group-details.component";
import { CompetitionComponent } from "./competition/competition.component";
import { CompetitonDetailsComponent } from "./competition/competiton-details/competiton-details.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuardService } from "./auth-guard.service";
import { ContactusViewComponent } from "./contactus-view/contactus-view.component";
import { AddadminComponent } from './addadmin/addadmin.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/dashboard",
    pathMatch: "full",
    canActivate: [AuthGuardService],
  },
  { path: "login", component: LoginComponent },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "groups",
    component: GroupsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "competition",
    component: CompetitionComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "group-details",
    component: GroupDetailsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "competition-details",
    component: CompetitonDetailsComponent,
    canActivate: [AuthGuardService],
  },
  { path: "contactusView",
   component:ContactusViewComponent,
   canActivate: [AuthGuardService],
  },
  { path: "addadmin",
    component:AddadminComponent,
    canActivate: [AuthGuardService],
  },
  { path: "users", component: UsersComponent, canActivate: [AuthGuardService] },
  { path: "forms", component: FormsComponent },
  { path: "buttons", component: ButtonsComponent },
  { path: "tables", component: TablesComponent },
  { path: "icons", component: IconsComponent },
  { path: "typography", component: TypographyComponent },
  { path: "alerts", component: AlertsComponent },
  { path: "accordions", component: AccordionsComponent },
  { path: "badges", component: BadgesComponent },
  { path: "progressbar", component: ProgressbarComponent },
  { path: "breadcrumbs", component: BreadcrumbsComponent },
  { path: "pagination", component: PaginationComponent },
  { path: "dropdowns", component: DropdownComponent },
  { path: "tooltips", component: TooltipsComponent },
  { path: "carousel", component: CarouselComponent },
  { path: "tabs", component: TabsComponent },
  // { path: "contactusView", component:ContactusViewComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
