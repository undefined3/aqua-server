import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { AuthService } from '../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  constructor(
    private auth:AuthService,
    private _snackBar: MatSnackBar,
    private router: Router
    ) {}
  loader=false;
  mainerror=false;

  ngOnInit() {}

  form = new FormGroup({
    email: new FormControl("",[
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]
      ),
    password: new FormControl("",
      [Validators.required,
      ]),
  });

  get email() {
    return this.form.get("email");
  }
  get password() {
    return this.form.get("password");
  }
  submit() {
    // alert(this.form.value['email']);
    // alert(this.form.value['password']);
    console.log(this.email.invalid,this.password.invalid);

    if(this.form.invalid){
      this.email.markAsTouched();
      this.password.markAsTouched();
      return;
    }
    this.loader=true;
    this.auth.login(this.form.value).subscribe((data:any)=>{
      console.log(data);
      this.auth.userDetail=data.data;
      localStorage.setItem("user",JSON.stringify(data.data));
      if(data.status){
        // this.router.navigate(['/admin'])
        this.router.navigate(['/dashboard']);
        this._snackBar.open("Successfully Loged", "close", {
          duration: 2000,
        });

      }else{
        this._snackBar.open("Wrong Password or Email", "close", {
          duration: 2000,
        });
        this.mainerror=false;
        this.loader=false;

      }

    },error=>{
      console.log(error);
    });
    console.log(this.form.value['email'],this.form.value['password']);
    return;

    localStorage.setItem("username", this.form.value["email"]);
  }
}
