import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {


  constructor(private http: HttpClient) { }
  getAllFeedback(){
    return this.http.get("https://aquabackend.herokuapp.com/api/feedback/getall");
  }
  getAllQuestion(){
    return this.http.get("http://localhost:3000/api/feedback/getquestion");
  }
  updateQuestion(id){
    return this.http.get("http://localhost:3000/api/feedback/updatequestion/"+id);
  }
  updateFeedback(id){
    return this.http.get("https://aquabackend.herokuapp.com/api/feedback/update/"+id);
  }

}
