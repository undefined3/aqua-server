import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-competition-card",
  templateUrl: "./competition-card.component.html",
  styleUrls: ["./competition-card.component.scss"],
})
export class CompetitionCardComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  showCompetitonDetails() {
    this.router.navigate(["competition-details"]);
    //    this.router.navigate(['/heroes', { id: heroId }]);
  }
}
