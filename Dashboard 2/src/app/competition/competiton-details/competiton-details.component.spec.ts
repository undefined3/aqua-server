import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitonDetailsComponent } from './competiton-details.component';

describe('CompetitonDetailsComponent', () => {
  let component: CompetitonDetailsComponent;
  let fixture: ComponentFixture<CompetitonDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitonDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitonDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
