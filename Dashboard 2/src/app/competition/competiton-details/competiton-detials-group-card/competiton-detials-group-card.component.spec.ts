import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitonDetialsGroupCardComponent } from './competiton-detials-group-card.component';

describe('CompetitonDetialsGroupCardComponent', () => {
  let component: CompetitonDetialsGroupCardComponent;
  let fixture: ComponentFixture<CompetitonDetialsGroupCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitonDetialsGroupCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitonDetialsGroupCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
