var express = require('express');
const path = require('path');
const router = express.Router();
const SensorRead =require("../modules/sensorread");
const WaterRequirement=require("../modules/waterintake");
const User=require("../modules/user");
const leaderboard = require('../modules/leaderboard');


const heightToBottom=19.5;
const surfaceArea=38.5;

router.post("/get",async (req,res)=>{
    
    console.log(req.body.data);
    var sensorRead=req.body.data.split(",");
    // console.log(sensorRead);
    var waterLevel1,waterLevel2;
    sensorRead[0]=sensorRead[0].substr(1,sensorRead[0].length-1);
    var userId=sensorRead[0];
    // console.log(userId)
    console.log(sensorRead);
    
    waterLevel1=getReadIncm(sensorRead);
    
    waterLevel2=heightToBottom-microsecondsToCentimeters(sensorRead[4]);
    console.log("waterlevel1 :"+waterLevel1);
    console.log("waterlevel2 :"+waterLevel2);

    return;

    var now = new Date();
    var today = new Date(now.getFullYear(),now.getMonth(),now.getDate());

    var sensorVal=await SensorRead.findOne({date:today.getTime()}).exec();
    console.log(sensorVal);
    if(sensorVal){
        var measurement=sensorVal.measurement;
        // if(Math.abs(waterLevel1-waterLevel2)>5){
        //     console.log("wrong data");
        //     res.send("Data received");
        //     return;
        // }

        // var waterLevel=(parseInt(waterLevel1)+parseInt(waterLevel2))/2;
        var waterLevel=parseInt(waterLevel1);

        var newmeasurement={
            time:now.getTime(),
            waterLevel:(surfaceArea*waterLevel).toFixed(2),
        };
        var oldmeasurment=measurement[0];

        if((oldmeasurment.waterLevel-newmeasurement.waterLevel)>50){
            var waterrequirement=await WaterRequirement.findOne({userId,date:today.getTime()}).exec();
            var nowhour=now.getHours();
            if(waterrequirement==null){
                await getDailyRequirement(userId);   
            }else{
                var shedule=waterrequirement.shedule;
                for(var i=0;i<shedule.length;i++){
                    if(shedule[i].startTime.hour<=nowhour && shedule[i].endTime.hour>nowhour){
                        
                        shedule[i].archived=parseInt(shedule[i].archived)+(parseInt(oldmeasurment.waterLevel)- parseInt(newmeasurement.waterLevel));
                        waterrequirement.dailyUsage=parseInt(waterrequirement.dailyUsage)+(parseInt(oldmeasurment.waterLevel) - parseInt(newmeasurement.waterLevel));
                        
                        break;
                    }
                }
                await WaterRequirement.updateOne({userId,date:today.getTime()},{shedule,dailyUsage:waterrequirement.dailyUsage}).exec();
                var templeadboard=await leaderboard.findOne({userId,date:today.getTime()}).exec();
                if(templeadboard){
                    let score=(waterrequirement.dailyUsage/waterrequirement.dailyTarget)*100;
                    await leaderboard.updateOne({userId,date:today.getTime()},{$set:{score}}).exec();
                }else{
                    var newLeadboard=new leaderboard();
                    newLeadboard.score=(waterrequirement.dailyUsage/waterrequirement.dailyTarget)*100;
                    newLeadboard.date=today.getTime();
                    var group=await User.findById(userId).exec();
                    newLeadboard.userId=userId;
                    newLeadboard.groupId=group.groupId;
                    await newLeadboard.save();
                }
                
            }  
        }
        console.log("leeeeeeengthhhhhhhhh"+measurement.length);
        console.log(newmeasurement);
        
        // SensorRead.update({date:today.getTime()},{$set:{measurement:[newmeasurement,oldmeasurment]}}).exec();
    }else{
        var newsensor=new SensorRead();
        newsensor.userId=userId;
        newsensor.date=today.getTime();
        newsensor.measurement=[];
        // if(Math.abs(waterLevel1-waterLevel2)>5){
        //     res.send("Data received");
        //     return;
        // }
        var waterLevel=(parseInt(waterLevel1)+parseInt(waterLevel2))/2;

        var newmeasurement={
            time:now.getTime(),
            waterLevel:(surfaceArea*waterLevel).toFixed(2),
        };
        newsensor.measurement.push(newmeasurement);
        newsensor.save();

    }


    res.send("Data received");
    
});

function getReadIncm(val){
    
    if(val[1]<290){
        return 0;
    }else if(val[1]<340){
        return 1;
    }
    else if (val[1]<380){ 
        return 2; 
    }
    else if (val[1]<400){ 
        return 3; 
    } 
    else if (val[1]<410){ 
        return 4; 
    }else if(val[2]<100){
        return 5;
    }else if(val[2]<320){
        return 6;
    }else if(val[2]<370){
        return 7;
    }else if(val[2]<410){
        return 8;
    }else if(val[2]<480){
        return 9;
    }else if(val[3]<330){
        return 10;
    }else if(val[3]<365){
        return 11;
    }else if(val[3]<390){
        return 12;
    }else if(val[3]<420){
        return 13;
    }else if(val[3]<430){
        return 14;
    }else{
        return 15;
    }
    
}
function microsecondsToCentimeters(microseconds) {
    return parseInt(microseconds) / 29 / 2;
}
async function getDailyRequirement(userId){

    var now=new Date();
    
    var today=new Date(now.getFullYear(),now.getMonth(),now.getDate()).getTime();
    
    var user=await User.findById(userId).exec();
    let poundweight=user.weight*pound;  
    let intakeInOunce=poundweight*(2/3);
    let intakeInml=(intakeInOunce*ounce).toFixed(0);
    let wakupTime={
        hour:user.wakupTime.split(" ")[1]=='PM'?
        parseInt((user.wakupTime.split(" ")[0]).split(":")[0])+12:
        parseInt((user.wakupTime.split(" ")[0]).split(":")[0]),
        minute:parseInt((user.wakupTime.split(" ")[0]).split(":")[1])
    };
    
    let sleepTime={
        hour:user.sleepTime.split(" ")[1]=='PM'?
        parseInt((user.sleepTime.split(" ")[0]).split(":")[0])+12:
        parseInt((user.sleepTime.split(" ")[0]).split(":")[0]),
        minute:parseInt((user.sleepTime.split(" ")[0]).split(":")[1])
    }
    
    let breakfastTime={
        hour:user.breakfastTime.split(" ")[1]=='PM'?
        parseInt((user.breakfastTime.split(" ")[0]).split(":")[0])+12:
        parseInt((user.breakfastTime.split(" ")[0]).split(":")[0]),
        minute:parseInt((user.breakfastTime.split(" ")[0]).split(":")[1])
    }
    
    let lunchTime={
        hour:user.lunchTime.split(" ")[1]=='PM'?
        parseInt((user.lunchTime.split(" ")[0]).split(":")[0])+12:
        parseInt((user.lunchTime.split(" ")[0]).split(":")[0]),
        minute:parseInt((user.lunchTime.split(" ")[0]).split(":")[1])
    }
    let dinnerTime={
        hour:user.dinnerTime.split(" ")[1]=='PM'?
        parseInt((user.dinnerTime.split(" ")[0]).split(":")[0])+12:
        parseInt((user.dinnerTime.split(" ")[0]).split(":")[0]),
        minute:parseInt((user.dinnerTime.split(" ")[0]).split(":")[1])
    }
    
    let shedule=[];

    let ftarget=(((intakeInml-600)*2/5).toFixed(0)).split('.')[0];

    // After wakup
    let temp={
        target:300,
        archived:0,
        startTime:wakupTime,
        endTime:addTime(wakupTime,{hour:0,minute:30}),
    };
    shedule.push(temp);
    // During breakfast
    temp={
        target:0,
        archived:0,
        startTime:addTime(wakupTime,{hour:0,minute:30}),
        endTime:addTime(breakfastTime,{hour:1,minute:0}),
    };

    shedule.push(temp);
    // After breakfast
    temp={
        target:parseInt(ftarget),
        archived:0,
        startTime:addTime(breakfastTime,{hour:1,minute:0}),
        endTime:addTime(lunchTime,{hour:-1,minute:30}),
    };
    shedule.push(temp);
    // During lunch
    temp={
        target:0,
        archived:0,
        startTime:addTime(lunchTime,{hour:-1,minute:30}),
        endTime:addTime(lunchTime,{hour:1,minute:0}),
    };
    shedule.push(temp);

    // After lunch
    temp={
        target:parseInt((intakeInml-ftarget-600).toFixed(0).split('.')[0]),
        archived:0,
        startTime:addTime(lunchTime,{hour:1,minute:0}),
        endTime:addTime(dinnerTime,{hour:-1,minute:0}),
    };
    shedule.push(temp);

    // During dinner
    temp={
        target:0,
        archived:0,
        startTime:addTime(dinnerTime,{hour:-1,minute:0}),
        endTime:addTime(dinnerTime,{hour:0,minute:30}),
    };
    shedule.push(temp);

    // After dinner
    temp={
        target:300,
        archived:0,
        startTime:addTime(dinnerTime,{hour:0,minute:30}),
        endTime:addTime(sleepTime,{hour:-1,minute:40}),
    };
    shedule.push(temp);
    console.log(shedule);
    var newData=new WaterIntake({userId:userId,dailyTarget:intakeInml,date:today,shedule:shedule,dailyUsage:0});
    await newData.save();
    console.log(intakeInml);
    return newData;
}

module.exports = router;