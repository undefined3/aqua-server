const express = require("express");
const router = express.Router();

const WaterRequirement = require("../modules/waterintake");
const SensorRead = require("../modules/sensorread");

router.get("/summary/:id", (req, res) => {
  console.log("hi")
  var meta = {};
  var now =new Date();
  var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
  console.log(today.getTime());
  WaterRequirement.findOne({ userId: req.params.id,date:today.getTime()}).then((waterdata)=>{
    if(waterdata){
      meta.dailyTarget = waterdata.dailyTarget;
      meta.dailyUsage = waterdata.dailyUsage;
      meta.shedule=waterdata.shedule;
    }else{
      res.send({status:false});
      return;
    }
    
    SensorRead.findOne({ userId:req.params.id, date:today.getTime()})
      .then((data) => {
        if(data){
          // console.log(data);
          meta.waterLevel = data.measurement[0].waterLevel;
        }else{
          meta.waterLevel = 0;
        }
        
        res.send({status:true,data:meta});
      })
      .catch((error) => {
        res.send({
          status: false,
          message: "Something went wrong please try again!!!!" + error,
        });
      });
  });
});

router.post("/addWater", (req, res) => {
  var volume = parseInt(req.body.volume);
  var now =new Date();
  var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
  if (req.body.toAdd === "true"){
    // console.log("ADD");
    WaterRequirement.find({ userId: req.body.userId ,date:today.getTime()}).then((data) => {
      var previousVolume = parseInt(data[0]["dailyUsage"]);
      var newVolume = previousVolume + parseInt(req.body.volume);

      WaterRequirement.findOneAndUpdate(
        { userId: req.body.userId, date:today.getTime()},
        { $set: { dailyUsage: newVolume } },
        { new: true, useFindAndModify: false }
      )
        .exec()
        .then((data) => {
          res.json({ usage: data.dailyUsage });
        })
        .catch((error) => {
          res.send({
            status: false,
            message: "Something went wrong please try again!!!!" + error,
          });
        });
    });
  } else {
    // console.log("REMOVE");
    WaterRequirement.find({ userId: req.body.userId,date:today.getTime() }).then((data) => {
      var previousVolume = parseInt(data[0]["dailyUsage"]);
      var newVolume = previousVolume - parseInt(req.body.volume);

      WaterRequirement.findOneAndUpdate(
        { userId: req.body.userId,date:today.getTime() },
        { $set: { dailyUsage: newVolume } },
        { new: true, useFindAndModify: false }
      )
        .exec()
        .then((data) => {
          res.json({ usage: data.dailyUsage });
        })
        .catch((error) => {
          res.send({
            status: false,
            message: "Something went wrong please try again!!!!" + error,
          });
        });
    });
  }
});

module.exports = router;
