var express = require("express");
const path = require("path");
const router = express.Router();
const Group = require("../modules/group");
// const user = require('../modules/user');

const bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
const secrets = require("../config/secrets");
var nodemailer = require("nodemailer");

const saltRounds = secrets.saltRounds;
const jwtSecret = secrets.jwtSecret;

router.get("/groups", (req, res) => {
  Group.getAllGroups((err, groups) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: groups,
        success: true,
        msg: "all user",
      });
    }
  });
});

module.exports = router;
