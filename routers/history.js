const express = require('express');
// const { jwtSecret } = require('../config/secrets');
const WaterIntake = require('../modules/waterintake');
const router = express.Router();
var jwt = require('jsonwebtoken');
const secrets = require('./../config/secrets');


const jwtSecret=secrets.jwtSecret;
// const week = require('../History');
// const month =require('../History');
const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const week = [
    { "day": "Sun", "usage": 0.0 },
    { "day": "Mon", "usage": 0.0 },
    { "day": "Tue", "usage": 0.0 },    
    { "day": "Wed", "usage": 0.0 },
    { "day": "Thu", "usage": 0.0 },
    { "day": "Fri", "usage": 0.0 },
    { "day": "Sat", "usage": 0.0 }
];  

var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const month = [
    { "month": "Jan", "usage": 0.0 },
    { "month": "Feb", "usage": 0.0 },
    { "month": "Mar", "usage": 0.0 },
    { "month": "Apr", "usage": 0.0 },
    { "month": "May", "usage": 0.0 },
    { "month": "Jun", "usage": 0.0 },
    { "month": "Jul", "usage": 0.0 },
    { "month": "Aug", "usage": 0.0 },
    { "month": "Sep", "usage": 0.0 },
    { "month": "Oct", "usage": 0.0 },
    { "month": "Nov", "usage": 0.0 },
    { "month": "Dec", "usage": 0.0 }
];

const dailyUse = [
    {"source" : "water", "usage": 80.0 },
    {"source" : "fruitJuice", "usage": 10.0 },
    {"source" : "cofee", "usage": 5.0 },
    {"source" : "tea", "usage": 5.0 }
];



// get week data
router.get('/week', (req, res) => {
    console.log("hiiiii");
    var token = req.headers.authorization? req.headers.authorization.split(" ")[1] : null;

    jwt.verify(token,jwtSecret,async(error,user) => {
        if(error){
            res.send({status:false});
        }
        else{
            var now = new Date();
            var today = now.getDate();
            var yesterday = today-1;
            var startDate = today-7;

            var start = new Date(now.getFullYear(),now.getMonth(),startDate);
            console.log(start);
            
            var end = new Date(now.getFullYear(),now.getMonth(),yesterday);

            //get data within the range from database
            console.log("start")
            console.log(start);
            console.log(end);
            var data = await WaterIntake.find({email:user.email,date: {$gt: start.getTime(), $lt:end.getTime()}});
            console.log(data);

            // var temparr=[];
            // for(var i=0; i<week.length; i++){
            //     if(data[i] != null){
            //         var x = new Date(parseInt(data[i].date));
            //         var temp={
            //             day:days[x.getDay()],
            //             usage:parseInt(data[i].dailyTarget)
            //         }
            //     }
            //     temparr.push(temp);
                

            // console.log(temparr);
            // res.json(temparr);
            
            // replace the values in week array according to received data
            var tempweek=[];
            for(var i=0;i<7;i++){
                
                // var x = new Date(parseInt(data[i].date));
                var data = await WaterIntake.findOne({email:user.email,date:start.getTime()});
                console.log(data);
                var day = days[start.getDay()];
                if(data){
                    
                    var tempData={
                        "day":day,
                        "usage":(parseFloat(data.dailyUsage?data.dailyUsage:0)).toString(),
                    };
                    tempweek.push(tempData);
                }else{
                    var tempData={
                        "day":day,
                        "usage":'0',
                    };
                    tempweek.push(tempData);
                }
                start.setDate(start.getDate()+1);
            }

            console.log(tempweek);
            console.log("hi");
            res.send(tempweek);
        }
    });

});

// get single day data
router.get('/week/:day', (req,res) => {
    const found = week.some(week => week.day === req.params.day);
    
    if(found){
        res.json(week.filter(week => week.day === req.params.day));
    }
    else{
        res.status(400).json({ msg: 'Invalid day' });
    }
});

// get month data
router.get('/month', (req, res) => {

    var token = req.headers.authorization? req.headers.authorization.split(" ")[1] : null;

    jwt.verify(token, jwtSecret, async(error, user) => {
        if(error){
            res.send({status: false});
        }
        else{
            var now = new Date();
            
            console.log(start);

            
            var tempMonth=[];
            for(var i=1;i<13;i++){
                var start = new Date(now.getFullYear()-1,now.getMonth()+i,1);
                var end = new Date(now.getFullYear()-1,now.getMonth()+i+1,1);
                var data = await WaterIntake.find({email:user.email,date: {$gt: start.getTime(), $lt:end.getTime()}});
                var usage=0;
                for(var j=0;j<data.length;j++){
                    usage=data[j].dailyUsage?(parseFloat(data[j].dailyUsage)+usage):usage;
                }
                // { "month": "Jan", "usage": 0.0 },
                var tempdata={
                    "month": months[start.getMonth()], 
                    "usage": usage.toString()
                };
                
                console.log(tempdata);
                tempMonth.push(tempdata);

                
            }
            res.send(tempMonth);
            console.log(tempMonth);
        }
    })

    // res.json(month);
});

// update day data
router.put('/week/:day', (req, res) => {
    const found = week.some(week => week.day ===req.params);

    if(found){
        const updDay = req.body;
        week.forEach(week => {
            if(week.day === updDay.day){
                week.usage = updDay.usage? updDay.usage : week.usage;
            }
        });
    }
    else{ 
        res.status(400).json({ msg: 'Invalid day' });
    }

});

// get dailyUsage data
router.get('/dailyUse', (req, res) => {
    res.json(dailyUse);
});



module.exports = router;