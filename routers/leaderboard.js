var express = require('express');
var path = require('path');
var router = express.Router();
var Group=require('../modules/group');
var LeaderBoard=require('../modules/leaderboard');
var jwt = require('jsonwebtoken');
var secrets = require('../config/secrets');
var fs = require("fs");
var leaderBoardProfile = require('../modules/leaderBoardProfile');
var LeaderBoardScore=require('../modules/leaderboardscore');
const User = require('../modules/user');
const leaderboard = require('../modules/leaderboard');
const WaterIntake = require('../modules/waterintake');

var jwtSecret=secrets.jwtSecret;





// Get weekly result of given group

function compare(a,b){
    if(a.score>b.score){
        return -1
    }else{
        return 0
    }
}
router.get("/weekly",(req,res)=>{
    var now=new Date();
    var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,(error,tokendata)=>{
        if(error){
            res.send({status:false,message:"authorization failed!!!!"});
        }else{
            Group.findOne({_id:tokendata.groupId}).exec().then(async (groupData)=>{

                

                var ldbscore=await LeaderBoardScore.findOne({groupId:tokendata.groupId,type:'w'}).exec();
                console.log(ldbscore);
                if(ldbscore==null){
                    var newLeadboard={};
                    newLeadboard.groupId=tokendata.groupId;
                    newLeadboard.type='w';
                    newLeadboard.lastUpdate=today.getTime();
                    var day=today.getDay();
                    var tempMember=[];
                    console.log("day:",day);
                    for(var i=0;i<groupData.members.length;i++){
                        var score=0;
                        for(var j=0;j<day;j++){
                            var tempday=new Date(now.getFullYear(),now.getMonth(),now.getDate());
                            

                            tempday.setDate(tempday.getDate()-day+j);
                            console.log("date:",tempday);
                            console.log("USerid:",groupData.members[i].id);
                            var leadbd=await leaderboard.findOne({userId:groupData.members[i].id,date:tempday.getTime()}).exec();
                            if(leadbd){
                                console.log(score)
                                score=(parseFloat(score*j)+parseFloat(leadbd.score))/(j+1);
                            }
                            
                        }
                        var leadbdPr=await leaderBoardProfile.findOne({userId:groupData.members[i].id}).exec();
                        var pro=await User.findById(groupData.members[i].id).exec();
                        var tempmem={
                            id:groupData.members[i].id,
                            name:leadbdPr.userName,
                            score:score.toFixed(2).toString(),
                            gender:pro.gender,
                            avatar:leadbdPr.avatar,
                            badge:leadbdPr.badge
                        };
                        tempMember.push(tempmem);
                        
                    }
                    tempMember.sort(compare);
                    newLeadboard.members=tempMember;
                    var leadboardscore=new LeaderBoardScore(newLeadboard);
                    leadboardscore.save().then(da=>{
                        console.log(leadboardscore);
                    }).catch(e=>{
                        console.log(e);
                    });
                    
                    res.send({status:true,data:tempMember});
                    
                // }else if(ldbscore.lastUpdate==today.getTime()){
                //     console.log("hiiiiiiiiieeeeeee");
                //     var memberArr=[];
                //     for(var i=0;i<ldbscore.members.length;i++){
                        

                //         var templeadboardprofile=await leaderBoardProfile.findOne({userId:ldbscore.members[i].id}).exec();
                        
                //         var tempuser=await User.findById(ldbscore.members[i].id).exec();
                //         let mem={
                //             id:ldbscore.members[i].id,
                //             name:templeadboardprofile.userName,
                //             score:ldbscore.members[i].score.toString(),
                //             gender:tempuser.gender,
                //             avatar:templeadboardprofile.avatar,
                //             badge:templeadboardprofile.badge
                //         };
                //         memberArr.push(mem);
                //     }
                //     console.log(memberArr);
                //     res.send({status:true,data:memberArr});
                }else{
                    console.log("hiiiiiiiiieeeeeeellllllllllllllllll");
                    var tempMember=[];
                    var day=today.getDay()==0?7:today.getDay();
                   
                    for(var i=0;i<groupData.members.length;i++){
                        var tempday=new Date(now.getFullYear(),now.getMonth(),now.getDate());
                            
                        var score=0;
                        for(var j=0;j<day;j++){

                            var tempday=new Date(now.getFullYear(),now.getMonth(),now.getDate());
                            tempday.setDate(tempday.getDate()-day+j);
                            console.log("date:",tempday);
                            console.log("USerid:",groupData.members[i].id);
                            var leadbd=await leaderboard.findOne({userId:groupData.members[i].id,date:tempday.getTime()}).exec();
                            if(leadbd){
                                score=(parseFloat(score*j)+parseFloat(leadbd.score))/(j+1);
                            }
                            
                        }
                        var leadbdPr=await leaderBoardProfile.findOne({userId:groupData.members[i].id}).exec();
                        var pro=await User.findById(groupData.members[i].id).exec();
                        var tempmem={
                            id:groupData.members[i].id,
                            name:leadbdPr.userName,
                            score:score.toFixed(2).toString(),
                            gender:pro.gender,
                            avatar:leadbdPr.avatar,
                            badge:leadbdPr.badge
                        };
                        tempMember.push(tempmem);
                    }
                    tempMember.sort(compare);
                    res.send({status:true,data:tempMember});
                    LeaderBoardScore.updateOne({groupId:tokendata.groupId,type:'w'},{$set:{members:tempMember,lastUpdate:today.getTime()}}).exec();
                   
                }

            }).catch(err=>{
                res.send({status:false});
                
            });
        }
    });
    
});
// Get Monthly result of given group
router.post("/addscore",(req,res)=>{
    console.log("hiiii");
    var leadb=new LeaderBoard(req.body);
    leadb.save().then(data=>{
        res.send(data);
    }).catch(err=>{
        res.send(err);
    });
});
// Get Monthly result of given group
router.get("/monthly",(req,res)=>{
    var now=new Date();
    var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,(error,tokendata)=>{
        if(error){
            res.send({status:false,message:"authorization failed!!!!"});
        }else{
            Group.findOne({_id:tokendata.groupId}).exec().then(async (groupData)=>{

                var ldbscore=await LeaderBoardScore.findOne({groupId:tokendata.groupId,type:'m'}).exec();
                console.log(ldbscore);
                if(ldbscore==null){
                    var newLeadboard={};
                    newLeadboard.groupId=tokendata.groupId;
                    newLeadboard.type='m';
                    newLeadboard.lastUpdate=today.getTime();
                    var day=today.getDate();
                    var tempMember=[];
                    console.log("day:",day);
                    for(var i=0;i<groupData.members.length;i++){
                        var score=0;
                        for(var j=1;j<day;j++){
                            var tempday=new Date(now.getFullYear(),now.getMonth(),j);
                            
                            
                            console.log("date:",tempday);
                            console.log("USerid:",groupData.members[i].id);
                            var leadbd=await leaderboard.findOne({userId:groupData.members[i].id,date:tempday.getTime()}).exec();
                            if(leadbd){
                                console.log(score)
                                score=(parseFloat(score*j)+parseFloat(leadbd.score))/(j+1);
                            }
                        }
                        var leadbdPr=await leaderBoardProfile.findOne({userId:groupData.members[i].id}).exec();
                        var pro=await User.findById(groupData.members[i].id).exec();
                        var tempmem={
                            id:groupData.members[i].id,
                            name:leadbdPr.userName,
                            score:score.toFixed(2).toString(),
                            gender:pro.gender,
                            avatar:leadbdPr.avatar,
                            badge:leadbdPr.badge
                        };
                        tempMember.push(tempmem);
                        
                    }
                    newLeadboard.members=tempMember;
                    var leadboardscore=new LeaderBoardScore(newLeadboard);
                    leadboardscore.save().then(da=>{
                        console.log(leadboardscore);
                    }).catch(e=>{
                        console.log(e);
                    });
                    tempMember.sort(compare);
                    res.send({status:true,data:tempMember});
                    
                // }else if(ldbscore.lastUpdate==today.getTime()){
                    
                //     var memberArr=[];
                //     for(var i=0;i<ldbscore.members.length;i++){
                //         var templeadboardprofile=await leaderBoardProfile.findOne({userId:ldbscore.members[i].id}).exec();
                        
                //         var tempuser=await User.findById(ldbscore.members[i].id).exec();
                //         let mem={
                //             id:ldbscore.members[i].id,
                //             name:templeadboardprofile.userName,
                //             score:ldbscore.members[i].score.toString(),
                //             gender:tempuser.gender,
                //             avatar:templeadboardprofile.avatar,
                //             badge:templeadboardprofile.badge
                //         };
                //         memberArr.push(mem);
                //     }
                //     console.log(memberArr);
                //     res.send({status:true,data:memberArr});
                }else{
                    var tempMember=[];
                    var day=today.getDate();
                   
                    for(var i=0;i<groupData.members.length;i++){
                        
                            
                        var score=0;
                        for(var j=1;j<day;j++){
                            var tempday=new Date(now.getFullYear(),now.getMonth(),j);
                            

                            // console.log("date:",tempday);
                            // console.log("USerid:",groupData.members[i].id);
                            var leadbd=await leaderboard.findOne({userId:groupData.members[i].id,date:tempday.getTime()}).exec();
                            if(leadbd){
                                console.log(score)
                                score=(parseFloat(score*j)+parseFloat(leadbd.score))/(j+1);
                            }
                            
                        }
                        var leadbdPr=await leaderBoardProfile.findOne({userId:groupData.members[i].id}).exec();
                        var pro=await User.findById(groupData.members[i].id).exec();
                        var tempmem = {
                            id:groupData.members[i].id,
                            name:leadbdPr.userName,
                            score:score.toFixed(2).toString(),
                            gender:pro.gender,
                            avatar:leadbdPr.avatar,
                            badge:leadbdPr.badge
                        };
                        tempMember.push(tempmem);
                    }
                    console.log(tempMember);
                    tempMember.sort(compare);
                    console.log(tempMember);
                    res.send({status:true,data:tempMember});
                    LeaderBoardScore.updateOne({groupId:tokendata.groupId,type:'m'},{$set:{members:tempMember,lastUpdate:today.getTime()}}).exec();
                }


            }).catch(err=>{
                res.send({status:false});
            })
        }
    });
});
// Get daily result
router.get("/daily",async (req,res)=>{
    console.log("dddd");
    var now=new Date();
    var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,async(error,tokendata)=>{
        if(error){
            res.send({status:false,message:"authorization failed!!!!"});
        }else{
            var user=await User.findById(tokendata.id).exec();
            if(user.groupId==null){
                var token = jwt.sign({ 
                    email:tokendata.email,
                    id:tokendata.id,
                    name:tokendata.name,
                    gender:tokendata.gender,
                    groupId:null,
                },jwtSecret);
                res.send({status:false,hasGroup:false,token});
                return;
            }
            Group.findOne({_id:tokendata.groupId}).exec().then(async (groupData)=>{    
                var tempMember=[];

                for(var i=0;i<groupData.members.length;i++){
                    
                    var templeadboard=await LeaderBoard.findOne({userId:groupData.members[i].id,date:today.getTime()}).exec();
                    var leadbdPr=await leaderBoardProfile.findOne({userId:groupData.members[i].id}).exec();
                    var pro=await User.findById(groupData.members[i].id).exec();
                    var tempmem={
                        id:groupData.members[i].id,
                        name:leadbdPr.userName,
                        score:templeadboard?templeadboard.score.toString():'0',
                        gender:pro.gender,
                        avatar:leadbdPr.avatar,
                        badge:leadbdPr.badge
                    };
                    tempMember.push(tempmem);
                    
                }
                tempMember.sort(compare);
                res.send({status:true,data:tempMember});
            }).catch(erro=>{
                console.log(erro);
                req.send({status:false})
            });

        }
    });
});


// Get all groups list
router.get("/getall",async(req,res)=>{
    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,(error,data)=>{
        if(error){
            res.send({status:false,message:"authorization failed!!!!"});
        }else{
            Group.find({}).exec().then(async data=>{
                console.log(data);
                let groups=[];
                for(let i in data){
                    var admin=await leaderBoardProfile.findOne({userId:data[i].admin});
                    console.log(admin);
                    let tempGroup={
                        id:data[i]._id,
                        groupName:data[i].groupName,
                        image:data[i].image,
                        admin:admin?admin.userName:"",
                        members:data[i].members.length
                    }
                    groups.push(tempGroup);
                    console.log(tempGroup);
                }
                res.send(groups);
            }).catch(error=>{
                res.send({status:false,message:"Something went wrong please try again!!!!"});
            });
        }
    });
});

// Create new group
router.post("/addgroup",async(req,res)=>{
    // console.log(req.body);
    try{
        var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
        jwt.verify(token,jwtSecret,(error,tokendata)=>{
            if(error){
                res.send({status:false,message:"authorization failed!!!!"});
            }else{
                const newGroup=new Group();
                newGroup.image=req.body.fileName;
                newGroup.groupName=req.body.groupName;
                newGroup.members=[{
                    id:tokendata.id,
                    status:1,
                }];
                newGroup.admin=tokendata.id;
                var name = req.body.fileName;
                var img = req.body.image;
                var realFile = Buffer.from(img,"base64");
                fs.writeFile("images/groups/"+name, realFile, function(err) {
                    if(err){
                        console.log(err);
                        res.send({status:false,msg:"Image upload failed!!!!"});
                    }else{
                        newGroup.save().then(data=>{
                            console.log(data._id);
                            leaderBoardProfile.updateOne({userId:tokendata.id},{$set:{groupId:data._id}}).exec();
                            User.updateOne({_id:tokendata.id},{$set:{groupId:data._id}}).exec().then(user=>{
                                console.log(user);
                            }).catch(errrr=>{
                                console.log(errrr);
                            });
                            var token = jwt.sign({ 
                                email:tokendata.email,
                                id:tokendata.id,
                                name:tokendata.name,
                                gender:tokendata.gender,
                                groupId:data._id,
                            },jwtSecret);
                            res.send({status:true,msg:"Success",token:token});
                        }).catch(err=>{
                            console.log(err);
                            res.send({status:false,msg:"Creation failed!!!"});
                        });
                    }
                        
                });
            }
        });
    }catch(err){
        res.send({status:false,msg:"Internal Server error try again!!!"});
    }    
});

// Get leaderboard profile data
router.get("/getprofile",async(req,res)=>{
    console.log("huuuuuu");
    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,(error,tokenData)=>{
        if(error){
            res.send({status:false,message:"authorization failed!!!!"});
        }else{
            var now=new Date();
            var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
            leaderBoardProfile.find({userId:tokenData.id}).exec().then(async data=>{
               if(data.length==0){
                console.log("huuuuuu");

                   var newProfile=new leaderBoardProfile();
                   newProfile.userId=tokenData.id;
                   let tempNamearr=tokenData.name.split(" ");
                   let tempName="";
                   console.log(tempNamearr);
                   let i=0;
                   while(tempNamearr.length>i){
                    tempName=tempName+tempNamearr[i];
                    i++;
                   }
                   console.log(tempName);
                   newProfile.userName=tempName.substr(0,10).toLocaleLowerCase();
                   newProfile.score="0";
                   newProfile.numberOfDays="0";
                   newProfile.lastUpdate=today.getTime();
                   newProfile.rank="NONE";
                   newProfile.badge='0';
                   newProfile.avatar='1';
                   newProfile.groupId=null;
                   newProfile.groupStatus=false;
                   
                   
                   newProfile.save().then(()=>{
                    var profileDate={
                        userId:newProfile.userId,
                        userName:newProfile.userName,
                        score:newProfile.score,
                        numberOfDays:newProfile.numberOfDays,
                        lastUpdate:newProfile.lastUpdate,
                        rank:newProfile.rank,
                        badge:newProfile.badge,
                        avatar:newProfile.avatar,
                        groupStatus:newProfile.groupStatus,
                        groupName:"NONE"
                    };
                    
                    newProfile.groupName="NONE";
                    res.send({status:true,data:profileDate});
                   }).catch((error)=>{
                    res.send({error,status:false,msg:"Somthing went wrong Please try again!!!"});
                   });
               }else{
                 if(data[0].lastUpdate==today.getTime()){
                    var profileDate={
                        userId:data[0].userId,
                        userName:data[0].userName,
                        score:data[0].score,
                        numberOfDays:data[0].numberOfDays,
                        lastUpdate:data[0].lastUpdate,
                        rank:data[0].rank,
                        badge:data[0].badge,
                        avatar:data[0].avatar,
                        groupStatus:data[0].groupStatus
                    };
                    if(data[0]['groupId']!=null){
                        console.log(data[0]['groupId']);
                        var group=await Group.findById(data[0]['groupId']).exec();
                        profileDate['groupName']=group['groupName'];
                    }else{
                        console.log("hhhhhhh");
                        profileDate['groupName']='NONE';
                    }

                    res.send({status:true,data:profileDate});
                 }else{
                    var lastUpdate=new Date(parseInt(data[0].lastUpdate));
                    console.log(lastUpdate);
                    let score=data[0].score;

                    console.log(score)
                    let numberOfDays=data[0].numberOfDays;
                    console.log(numberOfDays)
                    while(true){
                        lastUpdate.setDate(lastUpdate.getDate()+1);
                        console.log(lastUpdate);
                        if(lastUpdate>today){
                            console.log("lastUpdate");
                            break;
                        }
                        console.log(lastUpdate)
                        const leadboard= await LeaderBoard.find({userId:tokenData.id,date:lastUpdate.getTime()}).exec();
                        console.log(leadboard);
                        if(leadboard.length>0){
                            score=(parseFloat(score*numberOfDays++)+parseFloat(leadboard[0].score))/numberOfDays;
                        }
                    }
                    console.log(score);
                    score=parseFloat(score);
                    data[0].score=score.toFixed(2);
                    data[0].numberOfDays=numberOfDays;
                    var profile={
                        userId:data[0].userId,
                        userName:data[0].userName,
                        score:data[0].score,
                        numberOfDays:data[0].numberOfDays,
                        lastUpdate:data[0].lastUpdate,
                        rank:data[0].rank,
                        badge:data[0].badge,
                        avatar:data[0].avatar,
                        groupStatus:data[0].groupStatus
                    };
                    if(data[0]['groupId']!=null){
                        console.log(data[0]['groupId']);
                        var group=await Group.findById(data[0]['groupId']).exec();
                        profile['groupName']=group['groupName'];
                    }else{
                        console.log("hhhhhhh");
                        profile['groupName']='NONE';
                    }
                    leaderBoardProfile.updateOne({userId:tokenData.id},{$set:{score:profile.score,numberOfDays:profile.numberOfDays,lastUpdate:today.getTime()}}).exec();
                    res.send({status:true,data:profile});
                 }  
               }
                // res.send();
            }).catch(error=>{
                console.log(error);
                res.send({error:error,status:false,message:"Something went wrong please try again!!!!"});
            });
        }
    });
});

router.post("/avatarupdate",function (req,res){

    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,function(error,user){
        if(error){
            res.send({status:false});
        }else{
            var newData={
                avatar:req.body.avatar,
                
            };
            console.log(user)
            leaderBoardProfile.updateOne({userId:user.id},{$set:newData}).then(data=>{
                console.log(data);
                if(data==null){
                    res.send({status:false});
                }else{
                    res.send({status:true,data:data});
                }
            }).catch(err=>{
                res.send({status:false})
                console.log(err);
            });

        }
    });

});

router.post("/usernameupdate",function (req,res){

    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,function(error,user){
        if(error){
            res.send({status:false});
        }else{
            var newData={
                userName:req.body.userName,
            };
            console.log(user)
            leaderBoardProfile.updateOne({userId:user.id},{$set:newData}).then(data=>{
                console.log(data);
                if(data==null){
                    res.send({status:false});
                }else{
                    res.send({status:true,data:data});
                }
            }).catch(err=>{
                res.send({status:false})
                console.log(err);
            });

        }
    });

});

router.get("/getgroupdetail",async function (req,res){

    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
    jwt.verify(token,jwtSecret,function(error,tokendata){
        if(error){
            res.send({status:false});
        }else{
            console.log(tokendata)
            Group.findById(tokendata.groupId).then(async data=>{
                // console.log(data);
                var groupdata={
                    _id:data._id,
                    groupName:data.groupName,
                    admin:data.admin,
                    image:data.image,
                    status:false,
                    members:[]
                };
                for(var i=0;i<data.members.length;i++){
                    try{
                        console.log("hi")
                        var user=await leaderBoardProfile.findOne({userId:data.members[i].id}).exec();
                        var user2=await User.findById(data.members[i].id).exec();
                        console.log(user);
                        var member={
                            gender:user2.gender,
                            name:user.userName,
                            avatar:user.avatar,
                            id:user._id,
                            status:data.members[i].status
                        };
                        if(tokendata.id==data.members[i].id){
                            groupdata.status=data.members[i].status;
                        }
                        groupdata.members.push(member);
                    }catch(err){
                        console.log(err);
                        res.send({status:false});
                        return;        
                    }
                    
                    
                }
                console.log("hu")
                console.log(groupdata);
                res.send({status:true,data:groupdata});

            }).catch(err=>{
                res.send({status:false})
                console.log(err);
            });

        }
    });

});


router.post("/groupnameupdate",function (req,res){

    console.log(req.body)
    Group.updateOne({_id:req.body.groupId},{$set:{groupName:req.body.groupName}}).then(data=>{
        console.log(data);
        res.send({status:true,data});

    }).catch(err=>{
        res.send({status:false})
        console.log(err);
    });


});

router.post("/updategroupImage",function (req,res){

    console.log(req.body.groupId)
    console.log(req.body.fileName)
    Group.updateOne({_id:req.body.groupId},{$set:{image:req.body.fileName}}).then(data=>{
        console.log(data);
        var name = req.body.fileName;
        var img = req.body.image;
        var realFile = Buffer.from(img,"base64");
        fs.writeFile("images/groups/"+name, realFile, function(err) {
            if(err){
                console.log(err);
            }else{
                res.send({status:true,data:{image:req.body.fileName}});
            }   
        });
    }).catch(err=>{
        res.send({status:false})
        console.log(err);
    });
});

router.post("/acceptgrouprequest",function (req,res){

    console.log(req.body);
    Group.findById(req.body.groupId).then(group=>{
        console.log(group);
        // group.members[req.body.index].status=true;
        var members=[];
        for(var i=0;i<group.members.length;i++){
            var mem={
                id:group.members[i].id,
                status:group.members[i].status,
            }
            members.push(mem);
        }

        members[req.body.index].status=true;

        Group.updateOne({_id:req.body.groupId},{$set:{members}}).then(updateData=>{
            LeaderBoard.updateOne({userId:members[req.body.index].id},{$set:{groupStatus:true}}).exec();
            res.send({status:true});
            console.log(updateData);
        }).catch(err=>{
            console.log(err)
            res.send({status:false});
        });
    }).catch(errr=>{
        console.log(errr);
        res.send({status:false});
    });
});

router.post("/removemember",async function (req,res){

    console.log(req.body);
    Group.findById(req.body.groupId).then(async group=>{
        console.log(group);
        // group.members[req.body.index].status=true;
        var members=[];
        for(var i=0;i<group.members.length;i++){
            if(i==req.body.index){
                await leaderBoardProfile.updateOne({userId:group.members[i].id},{$set:{groupId:null}}).exec();
                await User.updateOne({_id:group.members[i].id},{$set:{groupId:null}}).exec();
            }else{
                var mem={
                    id:group.members[i].id,
                    status:group.members[i].status,
                }
                members.push(mem);
            }
            
        }

        Group.updateOne({_id:req.body.groupId},{$set:{members}}).then(updateData=>{
            res.send({status:true});
            console.log(updateData);
        }).catch(err=>{
            console.log(err)
            res.send({status:false});
        });
    }).catch(errr=>{
        console.log(errr);
        res.send({status:false});
    });
});
// addmember/$id

router.get("/addmember/:groupId",function (req,res){

    console.log(req.body);
    Group.findById(req.params.groupId).then(group=>{
        console.log(group);
        // group.members[req.body.index].status=true;
        var members=[];
        for(var i=0;i<group.members.length;i++){
            
            var mem={
                id:group.members[i].id,
                status:group.members[i].status,
            }
            members.push(mem);
               
        }
        var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;
        jwt.verify(token,jwtSecret,function(error,tokendata){
            if(error){
                res.send({status:false});
            }else{
                var mem={
                    id:tokendata.id,
                    status:false
                }
                members.push(mem);
                
                Group.updateOne({_id:req.params.groupId},{$set:{members}}).then(updateData=>{
                    var token = jwt.sign({ 
                        email:tokendata.email,
                        id:tokendata.id,
                        name:tokendata.name,
                        gender:tokendata.gender,
                        groupId:req.params.groupId,
                    },jwtSecret);
                    User.updateOne({_id:tokendata.id},{$set:{groupId:req.params.groupId}}).exec();
                    leaderBoardProfile.updateOne({userId:tokendata.id},{$set:{groupId:req.params.groupId}}).exec();    
                    res.send({status:true,token:token});
                    console.log(updateData);
                }).catch(err=>{
                    console.log(err)
                    res.send({status:false});
                });
                
            }
        });
        
        
    }).catch(errr=>{
        console.log(errr);
        res.send({status:false});
    });
});


router.post("/addlead",(req,res)=>{
    var lead=new LeaderBoard();
    lead.date=req.body.date;
    lead.groupName=req.body.groupName;
    lead.userId=req.body.userId;
    lead.score=req.body.score;
    lead.save().then(data=>{
        res.send(data);
    });

});

module.exports = router;




