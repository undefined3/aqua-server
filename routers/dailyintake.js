var express = require('express');
const path = require('path');
const router = express.Router();
const User=require('../modules/user');
const WaterIntake=require('../modules/waterintake')
var jwt = require('jsonwebtoken');
const secrets = require('../config/secrets');


const jwtSecret=secrets.jwtSecret;



const ounce=29.57;

const pound=2.20462;

router.get('/dailyshedule',(req,res)=>{
    var now=new Date();
    
    var token=req.headers.authorization ? req.headers.authorization.split(" ")[1] : null ;

    var today=new Date(now.getFullYear(),now.getMonth(),now.getDate()).getTime();
    
    jwt.verify(token,jwtSecret,(error,user)=>{
        if(error){
            res.send({status:false});
        }else{
            WaterIntake.findOne({userId:user.id,date:today}).then(data=>{
                
                if(data==null){
                    console.log("hi");
                    User.findOne({_id:user.id}).then(user=>{
                        console.log(user);
                        let poundweight=user.weight*pound;  
                        let intakeInOunce=poundweight*(2/3);
                        let intakeInml=(intakeInOunce*ounce).toFixed(0);
                        let wakupTime={
                            hour:user.wakupTime.split(" ")[1]=='PM'?
                            parseInt((user.wakupTime.split(" ")[0]).split(":")[0])+12:
                            parseInt((user.wakupTime.split(" ")[0]).split(":")[0]),
                            
                            minute:parseInt((user.wakupTime.split(" ")[0]).split(":")[1])
                        };
                        
                        let sleepTime={
                            hour:user.sleepTime.split(" ")[1]=='PM'?
                            parseInt((user.sleepTime.split(" ")[0]).split(":")[0])+12:
                            parseInt((user.sleepTime.split(" ")[0]).split(":")[0]),
        
                            minute:parseInt((user.sleepTime.split(" ")[0]).split(":")[1])
                        }
                        
                        let breakfastTime={
                            hour:user.breakfastTime.split(" ")[1]=='PM'?
                            parseInt((user.breakfastTime.split(" ")[0]).split(":")[0])+12:
                            parseInt((user.breakfastTime.split(" ")[0]).split(":")[0]),
                            // hour:(user.breakfastTime.split(" ")[0]).split(":")[0],
                            minute:parseInt((user.breakfastTime.split(" ")[0]).split(":")[1])
                        }
                        
                        let lunchTime={
                            hour:user.lunchTime.split(" ")[1]=='PM'?
                            parseInt((user.lunchTime.split(" ")[0]).split(":")[0])+12:
                            parseInt((user.lunchTime.split(" ")[0]).split(":")[0]),
                            // hour:(user.lunchTime.split(" ")[0]).split(":")[0],
                            minute:parseInt((user.lunchTime.split(" ")[0]).split(":")[1])
                        }
                        let dinnerTime={
                            hour:user.dinnerTime.split(" ")[1]=='PM'?
                            parseInt((user.dinnerTime.split(" ")[0]).split(":")[0])+12:
                            parseInt((user.dinnerTime.split(" ")[0]).split(":")[0]),
                            // hour:(user.dinnerTime.split(" ")[0]).split(":")[0],
                            minute:parseInt((user.dinnerTime.split(" ")[0]).split(":")[1])
                        }
                        
                        let shedule=[];
        
                        let ftarget=(((intakeInml-600)*2/5).toFixed(0)).split('.')[0];
        
                        // After wakup
                        let temp={
                            target:300,
                            archived:0,
                            startTime:wakupTime,
                            endTime:addTime(wakupTime,{hour:0,minute:30}),
                        };
                        shedule.push(temp);
                        // During breakfast
                        temp={
                            target:0,
                            archived:0,
                            startTime:addTime(wakupTime,{hour:0,minute:30}),
                            endTime:addTime(breakfastTime,{hour:1,minute:0}),
                        };
        
                        shedule.push(temp);
                        // After breakfast
                        temp={
                            target:parseInt(ftarget),
                            archived:0,
                            startTime:addTime(breakfastTime,{hour:1,minute:0}),
                            endTime:addTime(lunchTime,{hour:-1,minute:30}),
                        };
                        shedule.push(temp);
                        // During lunch
                        temp={
                            target:0,
                            archived:0,
                            startTime:addTime(lunchTime,{hour:-1,minute:30}),
                            endTime:addTime(lunchTime,{hour:1,minute:0}),
                        };
                        shedule.push(temp);
        
                        // After lunch
                        temp={
                            target:parseInt((intakeInml-ftarget-600).toFixed(0).split('.')[0]),
                            archived:0,
                            startTime:addTime(lunchTime,{hour:1,minute:0}),
                            endTime:addTime(dinnerTime,{hour:-1,minute:0}),
                        };
                        shedule.push(temp);
        
                        // During dinner
                        temp={
                            target:0,
                            archived:0,
                            startTime:addTime(dinnerTime,{hour:-1,minute:0}),
                            endTime:addTime(dinnerTime,{hour:0,minute:30}),
                        };
                        shedule.push(temp);
        
                        // After dinner
                        temp={
                            target:300,
                            archived:0,
                            startTime:addTime(dinnerTime,{hour:0,minute:30}),
                            endTime:addTime(sleepTime,{hour:-1,minute:40}),
                        };
                        shedule.push(temp);
                        console.log(shedule);
                        var newData=new WaterIntake({userId:user.id,dailyTarget:intakeInml,date:today,shedule:shedule,dailyUsage:0});
                        console.log(newData);
                        newData.save().then(data=>{
                            console.log(data);
                        }).catch(error=>{
                            console.log(error);
                        }
                        );
                        res.send({status:true,data:newData});
                        // res.send({dailyIntake:intakeInml,status:true});
                        console.log(intakeInml);
                    });
    
                }else{
                    console.log(data.date);
                    console.log("heee");
                    res.send({status:true,data:data});
                }
            }).catch(err=>{
                console.log(err);
                res.send({status:false});
            });
        }
    });
    

});
function addTime(a,b){
    let min=parseInt(a.minute)+parseInt(b.minute);
    let hour=parseInt(a.hour)+parseInt(b.hour)+((min-(min%60))/60);
    min=min%60;
    return{hour,minute:min};
}


module.exports = router;




