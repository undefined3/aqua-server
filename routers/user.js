var express = require("express");
const path = require("path");
const router = express.Router();
const User = require("../modules/user");
// const user = require('../modules/user');

const bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
const secrets = require("../config/secrets");
var nodemailer = require("nodemailer");

const saltRounds = secrets.saltRounds;
const jwtSecret = secrets.jwtSecret;

router.post("/test", (req, res) => {});

router.post("/login", (req, res) => {
  User.findOne({ email: req.body.email })
    .then((data) => {
      if (data != null) {
        console.log(data);
        // console.log(hashPassword)
        // console.log(data.password)
        if (bcrypt.compareSync(req.body.password, data.password)) {
          var token = jwt.sign(
            {
              email: data.email,
              id: data._id,
              name: data.name,
              gender: data.gender,
              groupId: data.groupId,
            },
            jwtSecret
          );
          console.log(token);
          res.send({ status: true, token });
        } else {
          res.send({ status: false });
        }
      } else {
        res.send({ status: false });
      }
    })
    .catch((error) => {
      res.send({ status: false });
      console.log(error);
    });
});

router.post("/register", (req, res) => {
  console.log(req.body);
  // return;
  const salt = bcrypt.genSaltSync(saltRounds);
  const hashPassword = bcrypt.hashSync(req.body.password, salt);
  var bod = req.body.birthday.split(" ")[0];
  console.log(bod);
  const user = {
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
    weight: req.body.weight,
    height: req.body.height,
    gender: req.body.gender,
    breakfastTime: req.body.breakfastTime,
    lunchTime: req.body.lunchTime,
    dinnerTime: req.body.dinnerTime,
    wakupTime: req.body.wakupTime,
    sleepTime: req.body.sleepTime,
    birthday: bod,
    CKD: req.body.CKD,
    groupId: null,
  };
  const newUser = new User(user);
  console.log(newUser);
  // res.send({status:true});
  newUser
    .save()
    .then((data) => {
      var token = jwt.sign(
        {
          email: data.email,
          id: data._id,
          name: data.name,
          gender: data.gender,
          groupId: data.groupId,
        },
        jwtSecret
      );
      console.log(token);
      res.send({ status: true, token: token });
    })
    .catch((error) => {
      console.log(error);
      res.send({ status: false });
    });
  // console.log(req.body);
});
router.get("/getUser", (req, res) => {
  var token = req.headers.authorization
    ? req.headers.authorization.split(" ")[1]
    : null;
  jwt.verify(token, jwtSecret, (error, data) => {
    if (error) {
      res.send({ status: false });
    } else {
      User.findById(data.id)
        .then((data) => {
          if (data == null) {
            res.send({ status: false });
          } else {
            res.send({ status: true, data: data });
          }
          console.log(data);
        })
        .catch((error) => {
          res.send({ status: false });
          console.log(error);
        });
    }
  });
});

router.get("/isexist/:email", (req, res) => {
  console.log("email checked");
  console.log(req.params);

  User.findOne({ email: req.params.email })
    .then((data) => {
      if (data != null) {
        res.send({ status: true });
      } else {
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "teamaqua.thridyear",
            pass: "ucsc@123",
          },
        });
        console.log(req.params);
        let code = Math.floor(Math.random() * 100000);
        code = parseInt(
          code.toString().length == 4 ? code.toString() + "0" : code
        );
        console.log(code);
        var mailOptions = {
          from: "Team Aqua",
          to: req.params.email,
          subject: "Verify Your Email",
          text: "You have registered for Aqua\nYour code is " + code,
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
          } else {
            console.log("Email sent: " + info.response);
          }
          res.send({ status: false, code });
        });
      }
      console.log(data);
    })
    .catch((error) => {
      res.send({ status: true });
      console.log(error);
    });
});
router.post("/sheduleUpdate", (req, res) => {
  console.log(req.body);

  var token = req.headers.authorization
    ? req.headers.authorization.split(" ")[1]
    : null;
  jwt.verify(token, jwtSecret, (error, user) => {
    if (error) {
      res.send({ status: false });
    } else {
      var newData = {
        wakupTime: req.body.wakupTime,
        sleepTime: req.body.sleepTime,
        breakfastTime: req.body.breakfastTime,
        lunchTime: req.body.lunchTime,
        dinnerTime: req.body.dinnerTime,
      };
      console.log(user);
      User.updateOne({ _id: user.id }, { $set: newData })
        .then((data) => {
          console.log(data);
          if (data == null) {
            res.send({ status: false });
          } else {
            res.send({ status: true, data: data });
          }
        })
        .catch((error) => {
          res.send({ status: false });
          console.log(error);
        });
    }
  });
});
router.post("/updateProfile", (req, res) => {
  var token = req.headers.authorization
    ? req.headers.authorization.split(" ")[1]
    : null;
  jwt.verify(token, jwtSecret, (error, user) => {
    if (error) {
      res.send({ status: false });
    } else {
      var newData = {
        name: req.body.name,
        height: req.body.height,
        weight: req.body.weight,
      };
      console.log(user);
      user.name = req.body.name;
      token = jwt.sign(name, jwtSecret);
      console.log(token);
      User.updateOne({ _id: user.id }, { $set: newData })
        .then((data) => {
          if (data == null) {
            res.send({ status: false });
          } else {
            res.send({ status: true, token: token });
          }
          console.log(data);
        })
        .catch((error) => {
          res.send({ status: false });
          console.log(error);
        });
    }
  });
});

router.get("/users", (req, res) => {
  User.getAllUsers((err, users) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: users,
        success: true,
        msg: "all user",
      });
    }
  });
});

router.get("/userbyid/:id", (req, res) => {
  User.getUserById(req.params.id, (err, user) => {
    if (err) {
      res.json({
        data: err,
        success: false,
        msg: "failed",
      });
    } else {
      res.json({
        data: user,
        success: true,
        msg: "get user",
      });
    }
  });
});

router.post("/barcode", (req, res) => {
  var barcode = req.body.barcode;

  User.findOneAndUpdate(
    { _id: req.body.userId },
    { $set: { barcode: barcode } },
    { new: true, useFindAndModify: false }
  )
    .exec()
    .then((data) => {
      res.json({"result":data});
    })
    .catch((error) => {
      res.send({
        status: false,
        message: "Something went wrong please try again!!!!" + error,
      });
    });
});

module.exports = router;
