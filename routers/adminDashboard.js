const express = require("express");
const router = express.Router();

const weather = require("weather-js");
const leaderboard = require("../modules/leaderboard");
const User = require("../modules/user");
const WaterRequirement = require("../modules/waterintake");
const Group = require("../modules/group");

router.post("/weather", (req, res) => {
  weather.find(
    { search: `${req.body.city}, SL`, degreeType: "C" },
    (err, result) => {
      if (err) console.log(err);

      // console.log(JSON.stringify(result, null, 2));
      res.send(result);
    }
  );
});

router.get("/leadboard", (req, res) => {
  leaderboard
    .find({})
    .sort({ score: "desc" })
    .exec()
    .then((data) => {
      res.send(data);

      var values = [];

      data.forEach(async (element) => {
        var user=await User.findById(element.userId);
        values.push(user);
        
      });
      console.log(values);
    })
    .catch((error) => {
      res.send({
        status: false,
        message: "Something went wrong please try again!!!!" + error,
      });
    });
});

router.get("/summary", (req, res) => {
  var summary = {};

  User.count({}, (err, count) => {
    summary["userCount"] = count;
  });

  Group.count({}, (err, count) => {
    summary["groupCount"] = count;
  });

  WaterRequirement.find({}, (err, data) => {
    var dailyTarget = 0;
    var dailyUsage = 0;
    data.forEach((element) => {
      dailyTarget = dailyTarget + parseInt(element.dailyTarget);
      dailyUsage = dailyUsage + parseInt(element.dailyUsage);
    });
    summary["dailyTarget"] = dailyTarget;
    summary["dailyUsage"] = dailyUsage;

    res.json(summary);
  });
});

module.exports = router;


// const express = require("express");
// const router = express.Router();

// const weather = require("weather-js");
// const leaderboard = require("../modules/leaderboard");
// const User = require("../modules/user");
// const WaterRequirement = require("../modules/waterintake");
// const Group = require("../modules/group");

// router.post("/weather", (req, res) => {
//   weather.find(
//     { search: `${req.body.city}, SL`, degreeType: "C" },
//     (err, result) => {
//       if (err) console.log(err);

//       // console.log(JSON.stringify(result, null, 2));
//       res.send(result);
//     }
//   );
// });

// router.get("/leadboard", (req, res) => {
//   leaderboard
//     .find({})
//     .sort({ score: "desc" })
//     .exec()
//     .then((data) => {
//       res.send(data);

//       var values = [];

//       data.forEach((element) => {
//         User.findById(element.userId).then((result) => {
//           values.push(result);
//         });
//       });
//       console.log(values);
//     })
//     .catch((error) => {
//       res.send({
//         status: false,
//         message: "Something went wrong please try again!!!!" + error,
//       });
//     });
// });

// router.get("/summary", (req, res) => {
//   var summary = {};

//   User.count({}, (err, count) => {
//     summary["userCount"] = count;
//   });

//   Group.count({}, (err, count) => {
//     summary["groupCount"] = count;
//   });

//   WaterRequirement.find({}, (err, data) => {
//     var dailyTarget = 0;
//     var dailyUsage = 0;
//     data.forEach((element) => {
//       dailyTarget = dailyTarget + parseInt(element.dailyTarget);
//       dailyUsage = dailyUsage + parseInt(element.dailyUsage);
//     });
//     summary["dailyTarget"] = dailyTarget;
//     summary["dailyUsage"] = dailyUsage;

//     res.json(summary);
//   });
// });

// module.exports = router;
