var express = require('express');
const path = require('path');
const router = express.Router();
const Admin=require('../../modules/admin');
// const bcrypt = require('bcrypt');
// const saltRounds = 10;


router.post("/login",(req,res)=>{
    
    Admin.findOne({email:req.body.email}).then(data=>{
        if(data!=null){
            if(req.body.password==data.password){
                var admin={
                    name:data.name,
                    email:data.email,
                };
                res.send({status:true,data:admin});
                
            }else{
                setTimeout(()=>{
                    res.send({status:false});
                },2000);
                
            }
        }else{
            res.send({status:false});
        }
        
    }).catch(error=>{
      res.send({status:false});
      console.log(error);
    });
});

router.post("/register",(req,res)=>{
    // const salt = bcrypt.genSaltSync(saltRounds);
    // const hash = bcrypt.hashSync(req.body.password, salt);
    const user={
        name:req.body.name,
        email:req.body.email,
        password:req.body.password,
    }
    const newAdmin=new Admin(user);
    console.log(newAdmin);
    // res.send({status:true});
    newAdmin.save().then(data=>{
      console.log(data);
      res.send({status:true});
    }).catch(error=>{
      console.log(error);
      res.send({status:false});
    });
    // console.log(req.body);
    
});



module.exports = router;