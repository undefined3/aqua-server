"use strict";

var express = require('express');

var path = require('path');

var router = express.Router();

var User = require('../modules/user');

var user = require('../modules/user');

var bcrypt = require("bcryptjs");

var jwt = require('jsonwebtoken');

var secrets = require('../config/secrets');

var nodemailer = require('nodemailer');

var saltRounds = secrets.saltRounds;
var jwtSecret = secrets.jwtSecret;
router.post('/test', function (req, res) {});
router.post("/login", function (req, res) {
  User.findOne({
    email: req.body.email
  }).then(function (data) {
    if (data != null) {
      console.log(data); // console.log(hashPassword)
      // console.log(data.password)

      if (bcrypt.compareSync(req.body.password, data.password)) {
        var token = jwt.sign({
          email: data.email,
          id: data._id,
          name: data.name,
          gender: data.gender,
          groupId: data.groupId
        }, jwtSecret);
        console.log(token);
        res.send({
          status: true,
          token: token
        });
      } else {
        res.send({
          status: false
        });
      }
    } else {
      res.send({
        status: false
      });
    }
  })["catch"](function (error) {
    res.send({
      status: false
    });
    console.log(error);
  });
});
router.post("/register", function (req, res) {
  console.log(req.body); // return;

  var salt = bcrypt.genSaltSync(saltRounds);
  var hashPassword = bcrypt.hashSync(req.body.password, salt);
  var bod = req.body.birthday.split(" ")[0];
  console.log(bod);
  var user = {
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
    weight: req.body.weight,
    height: req.body.height,
    gender: req.body.gender,
    breakfastTime: req.body.breakfastTime,
    lunchTime: req.body.lunchTime,
    dinnerTime: req.body.dinnerTime,
    wakupTime: req.body.wakupTime,
    sleepTime: req.body.sleepTime,
    birthday: bod,
    CKD: req.body.CKD,
    groupId: null
  };
  var newUser = new User(user);
  console.log(newUser); // res.send({status:true});

  newUser.save().then(function (data) {
    var token = jwt.sign({
      email: data.email,
      id: data._id,
      name: data.name,
      gender: data.gender,
      groupId: data.groupId
    }, jwtSecret);
    console.log(token);
    res.send({
      status: true,
      token: token
    });
  })["catch"](function (error) {
    console.log(error);
    res.send({
      status: false
    });
  }); // console.log(req.body);
});
router.get("/getUser", function (req, res) {
  var token = req.headers.authorization ? req.headers.authorization.split(" ")[1] : null;
  jwt.verify(token, jwtSecret, function (error, data) {
    if (error) {
      res.send({
        status: false
      });
    } else {
      User.findById(data.id).then(function (data) {
        if (data == null) {
          res.send({
            status: false
          });
        } else {
          res.send({
            status: true,
            data: data
          });
        }

        console.log(data);
      })["catch"](function (error) {
        res.send({
          status: false
        });
        console.log(error);
      });
    }
  });
});
router.get("/isexist/:email", function (req, res) {
  console.log("email checked");
  console.log(req.params);
  User.findOne({
    email: req.params.email
  }).then(function (data) {
    if (data != null) {
      res.send({
        status: true
      });
    } else {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'teamaqua.thridyear',
          pass: 'ucsc@123'
        }
      });
      console.log(req.params);
      var code = Math.floor(Math.random() * 100000);
      code = parseInt(code.toString().length == 4 ? code.toString() + "0" : code);
      console.log(code);
      var mailOptions = {
        from: 'Team Aqua',
        to: req.params.email,
        subject: 'Verify Your Email',
        text: 'You have registered for Aqua\nYour code is ' + code
      };
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }

        res.send({
          status: false,
          code: code
        });
      });
    }

    console.log(data);
  })["catch"](function (error) {
    res.send({
      status: true
    });
    console.log(error);
  });
});
router.post("/sheduleUpdate", function (req, res) {
  console.log(req.body);
  var token = req.headers.authorization ? req.headers.authorization.split(" ")[1] : null;
  jwt.verify(token, jwtSecret, function (error, user) {
    if (error) {
      res.send({
        status: false
      });
    } else {
      var newData = {
        wakupTime: req.body.wakupTime,
        sleepTime: req.body.sleepTime,
        breakfastTime: req.body.breakfastTime,
        lunchTime: req.body.lunchTime,
        dinnerTime: req.body.dinnerTime
      };
      console.log(user);
      User.updateOne({
        _id: user.id
      }, {
        $set: newData
      }).then(function (data) {
        console.log(data);

        if (data == null) {
          res.send({
            status: false
          });
        } else {
          res.send({
            status: true,
            data: data
          });
        }
      })["catch"](function (error) {
        res.send({
          status: false
        });
        console.log(error);
      });
    }
  });
});
router.post("/updateProfile", function (req, res) {
  var token = req.headers.authorization ? req.headers.authorization.split(" ")[1] : null;
  jwt.verify(token, jwtSecret, function (error, user) {
    if (error) {
      res.send({
        status: false
      });
    } else {
      var newData = {
        name: req.body.name,
        height: req.body.height,
        weight: req.body.weight
      };
      console.log(user);
      token = jwt.sign({
        email: user.email,
        id: user.id,
        name: newData.name,
        gender: user.gender
      }, jwtSecret);
      console.log(token);
      User.updateOne({
        _id: user.id
      }, {
        $set: newData
      }).then(function (data) {
        if (data == null) {
          res.send({
            status: false
          });
        } else {
          res.send({
            status: true,
            token: token
          });
        }

        console.log(data);
      })["catch"](function (error) {
        res.send({
          status: false
        });
        console.log(error);
      });
    }
  });
});
module.exports = router;