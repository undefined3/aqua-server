"use strict";

var express = require('express');

var path = require('path');

var router = express.Router();

var User = require('../modules/user');

var WaterIntake = require('../modules/waterintake');

var jwt = require('jsonwebtoken');

var secrets = require('../config/secrets');

var jwtSecret = secrets.jwtSecret;
var ounce = 29.57;
var pound = 2.20462;
router.get('/dailyshedule', function (req, res) {
  var now = new Date();
  var token = req.headers.authorization ? req.headers.authorization.split(" ")[1] : null;
  var today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime();
  jwt.verify(token, jwtSecret, function (error, user) {
    if (error) {
      res.send({
        status: false
      });
    } else {
      WaterIntake.findOne({
        email: user.email,
        date: today
      }).then(function (data) {
        if (data == null) {
          console.log("hi");
          User.findOne({
            email: user.email
          }).then(function (user) {
            console.log(user);
            var poundweight = user.weight * pound;
            var intakeInOunce = poundweight * (2 / 3);
            var intakeInml = (intakeInOunce * ounce).toFixed(0);
            var wakupTime = {
              hour: user.wakupTime.split(" ")[1] == 'PM' ? parseInt(user.wakupTime.split(" ")[0].split(":")[0]) + 12 : parseInt(user.wakupTime.split(" ")[0].split(":")[0]),
              minute: parseInt(user.wakupTime.split(" ")[0].split(":")[1])
            };
            var sleepTime = {
              hour: user.sleepTime.split(" ")[1] == 'PM' ? parseInt(user.sleepTime.split(" ")[0].split(":")[0]) + 12 : parseInt(user.sleepTime.split(" ")[0].split(":")[0]),
              minute: parseInt(user.sleepTime.split(" ")[0].split(":")[1])
            };
            var breakfastTime = {
              hour: user.breakfastTime.split(" ")[1] == 'PM' ? parseInt(user.breakfastTime.split(" ")[0].split(":")[0]) + 12 : parseInt(user.breakfastTime.split(" ")[0].split(":")[0]),
              // hour:(user.breakfastTime.split(" ")[0]).split(":")[0],
              minute: parseInt(user.breakfastTime.split(" ")[0].split(":")[1])
            };
            var lunchTime = {
              hour: user.lunchTime.split(" ")[1] == 'PM' ? parseInt(user.lunchTime.split(" ")[0].split(":")[0]) + 12 : parseInt(user.lunchTime.split(" ")[0].split(":")[0]),
              // hour:(user.lunchTime.split(" ")[0]).split(":")[0],
              minute: parseInt(user.lunchTime.split(" ")[0].split(":")[1])
            };
            var dinnerTime = {
              hour: user.dinnerTime.split(" ")[1] == 'PM' ? parseInt(user.dinnerTime.split(" ")[0].split(":")[0]) + 12 : parseInt(user.dinnerTime.split(" ")[0].split(":")[0]),
              // hour:(user.dinnerTime.split(" ")[0]).split(":")[0],
              minute: parseInt(user.dinnerTime.split(" ")[0].split(":")[1])
            };
            var shedule = [];
            var ftarget = ((intakeInml - 600) * 2 / 5).toFixed(0).split('.')[0]; // After wakup

            var temp = {
              target: 300,
              archived: 0,
              startTime: wakupTime,
              endTime: addTime(wakupTime, {
                hour: 0,
                minute: 30
              })
            };
            shedule.push(temp); // During breakfast

            temp = {
              target: 0,
              archived: 0,
              startTime: addTime(wakupTime, {
                hour: 0,
                minute: 30
              }),
              endTime: addTime(breakfastTime, {
                hour: 1,
                minute: 0
              })
            };
            shedule.push(temp); // After breakfast

            temp = {
              target: parseInt(ftarget),
              archived: 0,
              startTime: addTime(breakfastTime, {
                hour: 1,
                minute: 0
              }),
              endTime: addTime(lunchTime, {
                hour: -1,
                minute: 30
              })
            };
            shedule.push(temp); // During lunch

            temp = {
              target: 0,
              archived: 0,
              startTime: addTime(lunchTime, {
                hour: -1,
                minute: 30
              }),
              endTime: addTime(lunchTime, {
                hour: 1,
                minute: 0
              })
            };
            shedule.push(temp); // After lunch

            temp = {
              target: parseInt((intakeInml - ftarget - 600).toFixed(0).split('.')[0]),
              archived: 0,
              startTime: addTime(lunchTime, {
                hour: 1,
                minute: 0
              }),
              endTime: addTime(dinnerTime, {
                hour: -1,
                minute: 0
              })
            };
            shedule.push(temp); // During dinner

            temp = {
              target: 0,
              archived: 0,
              startTime: addTime(dinnerTime, {
                hour: -1,
                minute: 0
              }),
              endTime: addTime(dinnerTime, {
                hour: 0,
                minute: 30
              })
            };
            shedule.push(temp); // After dinner

            temp = {
              target: 300,
              archived: 0,
              startTime: addTime(dinnerTime, {
                hour: 0,
                minute: 30
              }),
              endTime: addTime(sleepTime, {
                hour: -1,
                minute: 40
              })
            };
            shedule.push(temp);
            console.log(shedule);
            var newData = new WaterIntake({
              email: user.email,
              dailyTarget: intakeInml,
              date: today,
              shedule: shedule
            });
            console.log(newData);
            newData.save().then(function (data) {
              console.log(data);
            })["catch"](function (error) {
              console.log(error);
            });
            res.send({
              status: true,
              data: newData
            }); // res.send({dailyIntake:intakeInml,status:true});

            console.log(intakeInml);
          });
        } else {
          console.log(data.date);
          console.log("heee");
          res.send({
            status: true,
            data: data
          });
        }
      })["catch"](function (err) {
        console.log(err);
        res.send({
          status: false
        });
      });
    }
  });
});

function addTime(a, b) {
  var min = parseInt(a.minute) + parseInt(b.minute);
  var hour = parseInt(a.hour) + parseInt(b.hour) + (min - min % 60) / 60;
  min = min % 60;
  return {
    hour: hour,
    minute: min
  };
}

module.exports = router;