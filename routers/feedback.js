const express = require('express');
const FeedBack = require('../modules/feedback');
const AskQuestion=require('../modules/question');
const User=require('../modules/user');

const router = express.Router();


router.post('/add', (req, res) => {
    console.log(req.body);
    var newFeedBack=new FeedBack();
    newFeedBack.name=req.body.firstName+" "+req.body.lastName;
    newFeedBack.email=req.body.email;
    newFeedBack.feedback=req.body.feedback;
    newFeedBack.status=false;
    newFeedBack.save().then(data=>{
        res.send({status:true});
    }).catch(err=>{
        res.send({status:false});

    })

});
router.post('/askquestion', (req, res) => {
    console.log(req.body);
    console.log("req.body\n\n\n\n\n\n\n\n\n\n");
    var askquestion=new AskQuestion();
    askquestion.userId=req.body.userId;
    askquestion.question=req.body.question;
    askquestion.type=req.body.type;
    askquestion.status=false;

    askquestion.save();
    res.send({status:true});

});
router.get('/getquestion', async(req, res) => {
    AskQuestion.find({status:false}).then(async data=>{
        var temp=[];
        for(let i=0;i<data.length;i++){

            var u=await User.findById(data[i].userId).exec();
            var a={
                name:u.name,
                _id:data[i]._id,
                question:data[i].question,
                type:data[i].type,
            }
            console.log(a);
            temp.push(a);
        }
        console.log(temp);
        res.send({status:true,data:temp});

    }).catch(err=>{
        res.send({status:false});
    });
    

});
router.get('/updatequestion/:id', async(req, res) => {
    
    await AskQuestion.updateOne({_id:req.params.id},{$set:{status:true}}).exec();


    AskQuestion.find({status:false}).then(data=>{
        res.send({status:true,data:data});
    }).catch(err=>{
        res.send({status:false});

    });
});
router.get('/getall', (req, res) => {
    
    FeedBack.find({status:false}).then(data=>{
        res.send({status:true,data:data});
    }).catch(err=>{
        res.send({status:false});

    })

});
router.get('/update/:id', async(req, res) => {
    
    await FeedBack.updateOne({_id:req.params.id},{$set:{status:true}}).exec();


    FeedBack.find({status:false}).then(data=>{
        res.send({status:true,data:data});
    }).catch(err=>{
        res.send({status:false});

    })
});
module.exports = router;
