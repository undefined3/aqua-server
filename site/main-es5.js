(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! E:\Uni works\Group Projects 2\aqua-server\Dashboard\Aqua\src\main.ts */
      "zUnb");
      /***/
    },

    /***/
    "03M9":
    /*!*************************************************************!*\
      !*** ./src/app/web/features/features/features.component.ts ***!
      \*************************************************************/

    /*! exports provided: FeaturesComponent */

    /***/
    function M9(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FeaturesComponent", function () {
        return FeaturesComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _intro_intro_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../intro/intro.component */
      "TWix");
      /* harmony import */


      var _dash_features_dash_features_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../dash-features/dash-features.component */
      "T3bd");
      /* harmony import */


      var _leaderboard_leaderboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../leaderboard/leaderboard.component */
      "B4AA");
      /* harmony import */


      var _charts_charts_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../charts/charts.component */
      "IK3O");
      /* harmony import */


      var _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../profile/profile.component */
      "mTO2");

      var FeaturesComponent = /*#__PURE__*/function () {
        function FeaturesComponent() {
          _classCallCheck(this, FeaturesComponent);
        }

        _createClass(FeaturesComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FeaturesComponent;
      }();

      FeaturesComponent.ɵfac = function FeaturesComponent_Factory(t) {
        return new (t || FeaturesComponent)();
      };

      FeaturesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: FeaturesComponent,
        selectors: [["app-features"]],
        decls: 7,
        vars: 0,
        consts: [["href", "https://fonts.googleapis.com/css?family=Amaranth", "rel", "stylesheet"], [1, "page", 2, "width", "100vw!important"]],
        template: function FeaturesComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-intro");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-dash-features");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-leaderboard");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-charts");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "app-profile");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_intro_intro_component__WEBPACK_IMPORTED_MODULE_1__["IntroComponent"], _dash_features_dash_features_component__WEBPACK_IMPORTED_MODULE_2__["DashFeaturesComponent"], _leaderboard_leaderboard_component__WEBPACK_IMPORTED_MODULE_3__["LeaderboardComponent"], _charts_charts_component__WEBPACK_IMPORTED_MODULE_4__["ChartsComponent"], _profile_profile_component__WEBPACK_IMPORTED_MODULE_5__["ProfileComponent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi9mZWF0dXJlcy9mZWF0dXJlcy9mZWF0dXJlcy5jb21wb25lbnQuY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FeaturesComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-features',
            templateUrl: './features.component.html',
            styleUrls: ['./features.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "3mqr":
    /*!********************************************************!*\
      !*** ./src/app/web/normal-nav/normal-nav.component.ts ***!
      \********************************************************/

    /*! exports provided: NormalNavComponent */

    /***/
    function mqr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NormalNavComponent", function () {
        return NormalNavComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var NormalNavComponent = /*#__PURE__*/function () {
        function NormalNavComponent() {
          _classCallCheck(this, NormalNavComponent);
        }

        _createClass(NormalNavComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return NormalNavComponent;
      }();

      NormalNavComponent.ɵfac = function NormalNavComponent_Factory(t) {
        return new (t || NormalNavComponent)();
      };

      NormalNavComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: NormalNavComponent,
        selectors: [["app-normal-nav"]],
        decls: 25,
        vars: 0,
        consts: [[1, "navbar", "navbar-default", "navbar-trans", "navbar-expand-lg", "fixed-top", 2, "width", "100% !important"], [1, "container"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarDefault", "aria-controls", "navbarDefault", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", "collapsed"], ["href", "index.html", 1, "navbar-brand", "logo"], ["src", "../../../assets/images/new.png", 2, "height", "80px", "margin-bottom", "5px", "margin-top", "10px", "margin-left", "-100px"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarTogglerDemo01", "aria-expanded", "false", 1, "btn", "btn-link", "nav-search", "navbar-toggle-box-collapse", "d-md-none"], ["aria-hidden", "true", 1, "fa", "fa-search"], ["id", "navbarDefault", 1, "navbar-collapse", "collapse"], [1, "navbar-nav"], [1, "nav-item"], ["href", "index.html", 1, "nav-link", "active"], ["href", "aboutus", 1, "nav-link"], ["href", "features", 1, "nav-link"], ["href", "contactus", 1, "nav-link"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarTogglerDemo01", "aria-expanded", "false", 1, "btn", "btn-b-n", "navbar-toggle-box-collapse", "d-none", "d-md-block"]],
        template: function NormalNavComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "span", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "ul", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "li", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Home");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "About Us");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Features");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Contact Us");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "button", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".navbar-default[_ngcontent-%COMP%] {\r\n    transition: all .5s ease-in-out;\r\n    background-color: #ffffff;\r\n    padding-top: 28px;\r\n    padding-bottom: 28px;\r\n    -webkit-backface-visibility: hidden;\r\n    backface-visibility: hidden;\r\n    box-shadow: 1px 2px 15px rgba(100, 100, 100, 0.3);\r\n    font-family:Amaranth ;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .nav-search[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n    font-size: 1.5rem;\r\n\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%] {\r\n    box-shadow: 1px 2px 15px rgba(100, 100, 100, 0.3);\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%] {\r\n    -webkit-backface-visibility: hidden;\r\n    backface-visibility: hidden;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-item[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-item[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    padding-right: 10px;\r\n    padding-bottom: 8px;\r\n    margin-left: 0;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\r\n    font-size: 1.1rem;\r\n    color: #000000;\r\n    font-weight: 600;\r\n    letter-spacing: 0.030em;\r\n    transition: all 0.1s ease-in-out;\r\n    position: relative;\r\n    padding-left: 0;\r\n    padding-right: 0;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before {\r\n    content: '';\r\n    position: absolute;\r\n    bottom: 5px;\r\n    left: 0;\r\n    width: 100%;\r\n    height: 2px;\r\n    z-index: 0;\r\n    background-color: #4dddff;\r\n    transform: scaleX(0);\r\n    transform-origin: right;\r\n    transition: transform .2s ease-out, opacity .2s ease-out 0.3s;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover:before {\r\n    transform: scaleX(1);\r\n    transform-origin: left;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-trans[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%]:before, .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%]:before {\r\n    transform: scaleX(1);\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before {\r\n    background-color: #4dddff;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-trans[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%], .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%] {\r\n    transition: all .5s ease-in-out;\r\n    padding-top: 19px;\r\n    padding-bottom: 19px;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before {\r\n    background-color: #4dddff;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .navbar-brand[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%] {\r\n    border-top: 0;\r\n    border-left: 4px solid #4dddff;\r\n    border-right: 0;\r\n    border-bottom: 0;\r\n    transform: translate3d(0px, -40px, 0px);\r\n    opacity: 0;\r\n    filter: alpha(opacity=0);\r\n    visibility: hidden;\r\n    transition: all 0.5s cubic-bezier(0.3, 0.65, 0.355, 1) 0s, opacity 0.31s ease 0s, height 0s linear 0.36s;\r\n    margin: 0;\r\n    border-radius: 0;\r\n    padding: 12px 0;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%] {\r\n    padding: 12px 18px;\r\n    transition: all 500ms ease;\r\n    font-weight: 600;\r\n    min-width: 220px;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%]:hover {\r\n    background-color: #ffffff;\r\n    color: #4dddff;\r\n    transition: all 500ms ease;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .dropdown-item.active[_ngcontent-%COMP%] {\r\n    background-color: #ffffff;\r\n    color: #4dddff;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]:hover   .dropdown-menu[_ngcontent-%COMP%] {\r\n    transform: translate3d(0px, 0px, 0px);\r\n    visibility: visible;\r\n    opacity: 1;\r\n    filter: alpha(opacity=1);\r\n  }\r\n\r\n  .intro-img[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height:100ch;\r\n}\r\n\r\n  .centered[_ngcontent-%COMP%] {\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform: translate(-50%, -50%);\r\n    color: white;\r\n  }\r\n\r\n  .nav-item[_ngcontent-%COMP%]{\r\n    padding-left: 30px;\r\n}\r\n\r\n  .navbar[_ngcontent-%COMP%]{\r\n  height: 100px;\r\n  \r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL25vcm1hbC1uYXYvbm9ybWFsLW5hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJDQUFDOzt5Q0FFd0M7O0VBRXZDO0lBQ0UsK0JBQStCO0lBQy9CLHlCQUF5QjtJQUN6QixpQkFBaUI7SUFDakIsb0JBQW9CO0lBQ3BCLG1DQUFtQztJQUNuQywyQkFBMkI7SUFDM0IsaURBQWlEO0lBQ2pELHFCQUFxQjtFQUN2Qjs7RUFFQTtJQUNFLGNBQWM7SUFDZCxpQkFBaUI7O0VBRW5COztFQUVBO0lBQ0UsaURBQWlEO0VBQ25EOztFQUVBOztJQUVFLG1DQUFtQztJQUNuQywyQkFBMkI7RUFDN0I7O0VBRUE7O0lBRUUsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsY0FBYztFQUNoQjs7RUFFQTs7SUFFRSxpQkFBaUI7SUFDakIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQix1QkFBdUI7SUFDdkIsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsZ0JBQWdCO0VBQ2xCOztFQUVBOztJQUVFLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLE9BQU87SUFDUCxXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7SUFDVix5QkFBeUI7SUFFekIsb0JBQW9CO0lBRXBCLHVCQUF1QjtJQUV2Qiw2REFBNkQ7RUFFL0Q7O0VBRUE7O0lBRUUsY0FBYztFQUNoQjs7RUFFQTs7SUFHRSxvQkFBb0I7SUFFcEIsc0JBQXNCO0VBQ3hCOztFQUVBOzs7Ozs7OztJQVNFLG9CQUFvQjtFQUN0Qjs7RUFFQTtJQUNFLHlCQUF5QjtFQUMzQjs7RUFFQTtJQUNFLGNBQWM7RUFDaEI7O0VBRUE7Ozs7SUFJRSxjQUFjO0VBQ2hCOztFQUVBO0lBQ0UsK0JBQStCO0lBQy9CLGlCQUFpQjtJQUNqQixvQkFBb0I7RUFDdEI7O0VBRUE7SUFDRSxjQUFjO0VBQ2hCOztFQUVBO0lBQ0UseUJBQXlCO0VBQzNCOztFQUVBO0lBQ0UsY0FBYztFQUNoQjs7RUFFQTs7OztJQUlFLGNBQWM7RUFDaEI7O0VBRUE7SUFDRSxjQUFjO0VBQ2hCOztFQUVBO0lBQ0UsYUFBYTtJQUNiLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsZ0JBQWdCO0lBRWhCLHVDQUF1QztJQUN2QyxVQUFVO0lBQ1Ysd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQix3R0FBd0c7SUFDeEcsU0FBUztJQUNULGdCQUFnQjtJQUNoQixlQUFlO0VBQ2pCOztFQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLDBCQUEwQjtJQUMxQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0VBQ2xCOztFQUVBO0lBQ0UseUJBQXlCO0lBQ3pCLGNBQWM7SUFDZCwwQkFBMEI7RUFDNUI7O0VBRUE7SUFDRSx5QkFBeUI7SUFDekIsY0FBYztFQUNoQjs7RUFFQTtJQUVFLHFDQUFxQztJQUNyQyxtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLHdCQUF3QjtFQUMxQjs7RUFFRjtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCOztFQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1QsZ0NBQWdDO0lBQ2hDLFlBQVk7RUFDZDs7RUFFRjtJQUNJLGtCQUFrQjtBQUN0Qjs7RUFDQTtFQUNFLGFBQWE7RUFDYiw4QkFBOEI7QUFDaEMiLCJmaWxlIjoic3JjL2FwcC93ZWIvbm9ybWFsLW5hdi9ub3JtYWwtbmF2LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgLyo9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gIC8vLS0vLy0tPiAgIE5BVkJBUlxyXG4gID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Ki9cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0IHtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZy10b3A6IDI4cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMjhweDtcclxuICAgIC13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDJweCAxNXB4IHJnYmEoMTAwLCAxMDAsIDEwMCwgMC4zKTtcclxuICAgIGZvbnQtZmFtaWx5OkFtYXJhbnRoIDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdCAubmF2LXNlYXJjaCB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG5cclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIHtcclxuICAgIGJveC1zaGFkb3c6IDFweCAycHggMTVweCByZ2JhKDEwMCwgMTAwLCAxMDAsIDAuMyk7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIHtcclxuICAgIC13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWl0ZW0sXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1pdGVtIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogOHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDA7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5uYXYtbGluayxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWxpbmsge1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMC4wMzBlbTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjFzIGVhc2UtaW4tb3V0O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgcGFkZGluZy1yaWdodDogMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rOmJlZm9yZSxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWxpbms6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiA1cHg7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDJweDtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGRkZGZmO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlWCgwKTtcclxuICAgIHRyYW5zZm9ybTogc2NhbGVYKDApO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOiByaWdodDtcclxuICAgIHRyYW5zZm9ybS1vcmlnaW46IHJpZ2h0O1xyXG4gICAgdHJhbnNpdGlvbjogb3BhY2l0eSAuMnMgZWFzZS1vdXQgMC4zcywgLXdlYmtpdC10cmFuc2Zvcm0gLjJzIGVhc2Utb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC4ycyBlYXNlLW91dCwgb3BhY2l0eSAuMnMgZWFzZS1vdXQgMC4zcztcclxuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuMnMgZWFzZS1vdXQsIG9wYWNpdHkgLjJzIGVhc2Utb3V0IDAuM3MsIC13ZWJraXQtdHJhbnNmb3JtIC4ycyBlYXNlLW91dDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rOmhvdmVyLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluazpob3ZlciB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rOmhvdmVyOmJlZm9yZSxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWxpbms6aG92ZXI6YmVmb3JlIHtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZVgoMSk7XHJcbiAgICB0cmFuc2Zvcm06IHNjYWxlWCgxKTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjogbGVmdDtcclxuICAgIHRyYW5zZm9ybS1vcmlnaW46IGxlZnQ7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5zaG93ID4gLm5hdi1saW5rOmJlZm9yZSxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5hY3RpdmUgPiAubmF2LWxpbms6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rLnNob3c6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rLmFjdGl2ZTpiZWZvcmUsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLnNob3cgPiAubmF2LWxpbms6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5hY3RpdmUgPiAubmF2LWxpbms6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluay5zaG93OmJlZm9yZSxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWxpbmsuYWN0aXZlOmJlZm9yZSB7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGVYKDEpO1xyXG4gICAgdHJhbnNmb3JtOiBzY2FsZVgoMSk7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5uYXYtbGluazpiZWZvcmUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzRkZGRmZjtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rOmhvdmVyIHtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAuc2hvdyA+IC5uYXYtbGluayxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5hY3RpdmUgPiAubmF2LWxpbmssXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbmsuc2hvdyxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5uYXYtbGluay5hY3RpdmUge1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSB7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjVzIGVhc2UtaW4tb3V0O1xyXG4gICAgcGFkZGluZy10b3A6IDE5cHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTlweDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluayB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluazpiZWZvcmUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzRkZGRmZjtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluazpob3ZlciB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5zaG93ID4gLm5hdi1saW5rLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5hY3RpdmUgPiAubmF2LWxpbmssXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rLnNob3csXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rLmFjdGl2ZSB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXZiYXItYnJhbmQge1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQgLmRyb3Bkb3duIC5kcm9wZG93bi1tZW51IHtcclxuICAgIGJvcmRlci10b3A6IDA7XHJcbiAgICBib3JkZXItbGVmdDogNHB4IHNvbGlkICM0ZGRkZmY7XHJcbiAgICBib3JkZXItcmlnaHQ6IDA7XHJcbiAgICBib3JkZXItYm90dG9tOiAwO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDBweCwgLTQwcHgsIDBweCk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDBweCwgLTQwcHgsIDBweCk7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTApO1xyXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNXMgY3ViaWMtYmV6aWVyKDAuMywgMC42NSwgMC4zNTUsIDEpIDBzLCBvcGFjaXR5IDAuMzFzIGVhc2UgMHMsIGhlaWdodCAwcyBsaW5lYXIgMC4zNnM7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgcGFkZGluZzogMTJweCAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0IC5kcm9wZG93biAuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbSB7XHJcbiAgICBwYWRkaW5nOiAxMnB4IDE4cHg7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgNTAwbXMgZWFzZTtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBtaW4td2lkdGg6IDIyMHB4O1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0IC5kcm9wZG93biAuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbTpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgY29sb3I6ICM0ZGRkZmY7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgNTAwbXMgZWFzZTtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdCAuZHJvcGRvd24gLmRyb3Bkb3duLW1lbnUgLmRyb3Bkb3duLWl0ZW0uYWN0aXZlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBjb2xvcjogIzRkZGRmZjtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdCAuZHJvcGRvd246aG92ZXIgLmRyb3Bkb3duLW1lbnUge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDBweCwgMHB4LCAwcHgpO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwcHgsIDBweCwgMHB4KTtcclxuICAgIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTEpO1xyXG4gIH1cclxuXHJcbi5pbnRyby1pbWd7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDoxMDBjaDtcclxufVxyXG4uY2VudGVyZWQge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcblxyXG4ubmF2LWl0ZW17XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDMwcHg7XHJcbn1cclxuLm5hdmJhcntcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHllbGxvdzsgKi9cclxufSJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NormalNavComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-normal-nav',
            templateUrl: './normal-nav.component.html',
            styleUrls: ['./normal-nav.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "4uWM":
    /*!****************************************************!*\
      !*** ./src/app/Admin/sidebar/sidebar.component.ts ***!
      \****************************************************/

    /*! exports provided: SidebarComponent */

    /***/
    function uWM(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SidebarComponent", function () {
        return SidebarComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/icon */
      "TY1r");

      var _c0 = function _c0() {
        return ["./dashboard"];
      };

      var SidebarComponent = /*#__PURE__*/function () {
        function SidebarComponent() {
          _classCallCheck(this, SidebarComponent);
        }

        _createClass(SidebarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return SidebarComponent;
      }();

      SidebarComponent.ɵfac = function SidebarComponent_Factory(t) {
        return new (t || SidebarComponent)();
      };

      SidebarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SidebarComponent,
        selectors: [["app-sidebar"]],
        decls: 16,
        vars: 2,
        consts: [[1, "side-main"], ["routerLinkActive", "active", 1, "side-nav-item", 3, "routerLink"], [1, "side-nav-icon"], [1, "side-nav-text"], [1, "side-nav-item"], ["routerLink", "./addadmin", "routerLinkActive", "active", 1, "side-nav-item"]],
        template: function SidebarComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-icon", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "admin_panel_settings");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Dashboard ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-icon", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "people");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " Users ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-icon", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "person_add_alt_1");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " Add Admin ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
          }
        },
        directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_2__["MatIcon"]],
        styles: [".side-main[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 100ch;\r\n  background-color: rgb(48,48,48);\r\n}\r\n.side-nav-item[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 100px;\r\n  \r\n  position: relative;\r\n}\r\n.side-nav-icon[_ngcontent-%COMP%]{\r\n  position: absolute;\r\n  font-size: 40px;\r\n  top: 25px;\r\n  left: 10px;\r\n  color: white;\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n.side-nav-text[_ngcontent-%COMP%]{\r\n  color: white;\r\n  padding-left: 80px;\r\n  padding-top: 33px;\r\n  font-size: 18px;\r\n  transition: all 0.3s ease-in-out;\r\n}\r\n.active[_ngcontent-%COMP%]{\r\n  background-color: black;\r\n}\r\n.active[_ngcontent-%COMP%]   .side-nav-icon[_ngcontent-%COMP%]{\r\n  left: 50px;\r\n}\r\n.active[_ngcontent-%COMP%]   .side-nav-text[_ngcontent-%COMP%]{\r\n  padding-left: 120px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4vc2lkZWJhci9zaWRlYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsYUFBYTtFQUNiLCtCQUErQjtBQUNqQztBQUNBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYiw4QkFBOEI7RUFDOUIsa0JBQWtCO0FBQ3BCO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFNBQVM7RUFDVCxVQUFVO0VBQ1YsWUFBWTtFQUNaLGdDQUFnQztBQUNsQztBQUNBO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdDQUFnQztBQUNsQztBQUNBO0VBQ0UsdUJBQXVCO0FBQ3pCO0FBQ0E7RUFDRSxVQUFVO0FBQ1o7QUFDQTtFQUNFLG1CQUFtQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL0FkbWluL3NpZGViYXIvc2lkZWJhci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGUtbWFpbntcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMGNoO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYig0OCw0OCw0OCk7XHJcbn1cclxuLnNpZGUtbmF2LWl0ZW17XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiB5ZWxsb3c7ICovXHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5zaWRlLW5hdi1pY29ue1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgdG9wOiAyNXB4O1xyXG4gIGxlZnQ6IDEwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG59XHJcbi5zaWRlLW5hdi10ZXh0e1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBwYWRkaW5nLWxlZnQ6IDgwcHg7XHJcbiAgcGFkZGluZy10b3A6IDMzcHg7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2UtaW4tb3V0O1xyXG59XHJcbi5hY3RpdmV7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbn1cclxuLmFjdGl2ZSAuc2lkZS1uYXYtaWNvbntcclxuICBsZWZ0OiA1MHB4O1xyXG59XHJcbi5hY3RpdmUgLnNpZGUtbmF2LXRleHR7XHJcbiAgcGFkZGluZy1sZWZ0OiAxMjBweDtcclxufVxyXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SidebarComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-sidebar',
            templateUrl: './sidebar.component.html',
            styleUrls: ['./sidebar.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "6OIh":
    /*!********************************************!*\
      !*** ./src/app/web/home/home.component.ts ***!
      \********************************************/

    /*! exports provided: HomeComponent */

    /***/
    function OIh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeComponent", function () {
        return HomeComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/material/button */
      "Xlwt");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/icon */
      "TY1r");

      function HomeComponent_img_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 41);
        }
      }

      function HomeComponent_img_11_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 42);
        }
      }

      function HomeComponent_img_12_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 43);
        }
      }

      function HomeComponent_div_20_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Welcome to AQUA");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Leading to a Healthy Generation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function HomeComponent_div_21_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Track your daily water intake with Aqua.");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Achieve your hydration goals with a simple tap!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function HomeComponent_div_22_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Drink water and take care of yourself!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Join with us now!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var HomeComponent = /*#__PURE__*/function () {
        function HomeComponent() {
          var _this = this;

          _classCallCheck(this, HomeComponent);

          this.slider = [true, false, false];
          this.i = 0;
          setInterval(function () {
            _this.slider = [false, false, false];
            _this.slider[_this.i % 3] = true;
            _this.i++;
          }, 6000);
        }

        _createClass(HomeComponent, [{
          key: "sliderNext",
          value: function sliderNext() {
            console.log(this.i % 3);
            this.slider = [false, false, false];
            this.slider[this.i % 3] = true;
            this.i++;
          }
        }, {
          key: "sliderPre",
          value: function sliderPre() {
            this.slider = [false, false, false];
            this.slider[this.i % 3] = true;

            if (this.i == 0) {
              this.i = 2;
            } else {
              this.i++;
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return HomeComponent;
      }();

      HomeComponent.ɵfac = function HomeComponent_Factory(t) {
        return new (t || HomeComponent)();
      };

      HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: HomeComponent,
        selectors: [["app-home"]],
        decls: 102,
        vars: 6,
        consts: [["lang", "en"], ["href", "https://fonts.googleapis.com/css?family=Aclonica", "rel", "stylesheet"], ["href", "https://fonts.googleapis.com/css?family=Amaranth", "rel", "stylesheet"], ["href", "https://fonts.googleapis.com/css?family=Amita", "rel", "stylesheet"], ["href", "https://fonts.googleapis.com/css?family=Acme", "rel", "stylesheet"], ["rel", "stylesheet", "href", "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"], [2, "width", "100% !important"], [1, "slider"], ["class", "intro-img", "src", "../../../assets/images/istock-519369740.jpg", 4, "ngIf"], ["class", "intro-img", "src", "../../../assets/images/C01YDt-XcAA2IuZ.jpg", 4, "ngIf"], ["class", "intro-img", "src", "../../../assets/images/8e35f60b-2b4e-4518-b980-b167de74de58-fotolia_175041593_subscription_monthly_m.jpg", 4, "ngIf"], [1, "centered", "overlay"], ["mat-icon-button", "", 1, "slider-pre", 3, "click"], ["mat-icon-button", "", 1, "slider-next", 3, "click"], ["class", "overlay-txt", 4, "ngIf"], [1, "row", "section1-container_a"], [1, "col-sm-3", "col-md-6", "col-lg-5"], ["src", "../../../assets/images/835202653d0abbb2f47698b60faa51a7.jpg", 1, "image-section1"], [1, "col-sm-9", "col-md-6", "col-lg-7"], [1, "text-heading_a"], [2, "color", "#4dddff", "font-family", "Amita"], [1, "text-subheading"], [2, "padding-top", "20px"], [1, "section1-container", "img-fluid"], [1, "col-12", "parallax"], [1, "row", "features"], [1, "text-heading"], [2, "color", "#4dddff"], [1, "row", 2, "padding-top", "50px"], [1, "fa", "fa-check-square-o", "colour", "fa-2x"], [2, "padding-left", "10px", "font-weight", "100"], [1, "row"], [2, "padding-left", "10px", "font-weight", "100", "font-weight", "100"], ["src", "../../../assets/images/whatweDo.png", 1, "image-section1"], [1, "section1-container_a", "test"], [1, ""], [1, "text-heading", "text-center", 2, "color", "black"], [1, "text-download", "text-center", 2, "color", "black"], [1, "col"], ["src", "../../../assets/images/playstore.png", 1, "download"], ["src", "../../../assets/images/appstore.jpg", 1, "download"], ["src", "../../../assets/images/istock-519369740.jpg", 1, "intro-img"], ["src", "../../../assets/images/C01YDt-XcAA2IuZ.jpg", 1, "intro-img"], ["src", "../../../assets/images/8e35f60b-2b4e-4518-b980-b167de74de58-fotolia_175041593_subscription_monthly_m.jpg", 1, "intro-img"], [1, "overlay-txt"], [1, "text1"], [1, "text2", "text-center"], [1, "text1", "justify-content-center"]],
        template: function HomeComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "head");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "link", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "link", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "link", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "link", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "link", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "body", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "header");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, HomeComponent_img_10_Template, 1, 0, "img", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, HomeComponent_img_11_Template, 1, 0, "img", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, HomeComponent_img_12_Template, 1, 0, "img", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_14_listener() {
              return ctx.sliderPre();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "arrow_back_ios");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_17_listener() {
              return ctx.sliderNext();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "arrow_forward_ios");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, HomeComponent_div_20_Template, 5, 0, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, HomeComponent_div_21_Template, 5, 0, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, HomeComponent_div_22_Template, 5, 0, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "section");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Who are ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "span", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "We");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Not having enough water on a daily basis will lead to many diseases. By concerning all these problems we are designing a water bottle with high quality and beautiful design with many sizes according to the user type along with an app to gather data on user to customize the amount of water one should take per day. ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "When designing the bottle we include sensors for tracking the drinking water usage and then by connecting those two together we remind the user to drink water if the user did not achieve the target. We also organize certain types of interesting games and competitions in order to motivate the user to drink water. Not only the water level but also we track the quality measurement of the water contained in the bottle. We get the pH value, Fluoride percentage, Mineral concentration, heavy metal percentages using sensors, and notify the user through the app indicating whether it is healthy to drink or not.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "section");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "What we ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "span", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "do");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Our Goal is to create a healthy lifestyle avoiding diseases in busy schedules by drinking sufficient amounts of water daily");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Measure the remaining water level");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Check the quality of the water with pH value, Fluoride percentage, ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Mineral concentration, Heavy metals percentage");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Customize the amount of water which should drink by you according");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, " to the collected data");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Replace the raw water usage amount by the beverages\u2019 if you select ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, " one from the given beverage list");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "p", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Track the yours' drinking history");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Organize competitions among users in order to encourage");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Leader boards and badges");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](83, "i", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Remind you all the time");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "img", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "section");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "p", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, " Download AQUA");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "p", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Hydrate Today !");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "div", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "img", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "div", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.slider[0]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.slider[1]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.slider[2]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.slider[0]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.slider[1]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.slider[2]);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIcon"]],
        styles: [".navbar-default[_ngcontent-%COMP%] {\r\n    transition: all .5s ease-in-out;\r\n    background-color: #ffffff;\r\n    padding-top: 28px;\r\n    padding-bottom: 28px;\r\n    -webkit-backface-visibility: hidden;\r\n    backface-visibility: hidden;\r\n    box-shadow: 1px 2px 15px rgba(100, 100, 100, 0.3);\r\n    font-family:Aclonica ;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .nav-search[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n    font-size: 1.5rem;\r\n\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%] {\r\n    box-shadow: 1px 2px 15px rgba(100, 100, 100, 0.3);\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%] {\r\n    -webkit-backface-visibility: hidden;\r\n    backface-visibility: hidden;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-item[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-item[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    padding-right: 10px;\r\n    padding-bottom: 8px;\r\n    margin-left: 0;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\r\n    font-size: 1.1rem;\r\n    color: #000000;\r\n    font-weight: 600;\r\n    letter-spacing: 0.030em;\r\n    transition: all 0.1s ease-in-out;\r\n    position: relative;\r\n    padding-left: 0;\r\n    padding-right: 0;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before {\r\n    content: '';\r\n    position: absolute;\r\n    bottom: 5px;\r\n    left: 0;\r\n    width: 100%;\r\n    height: 2px;\r\n    z-index: 0;\r\n    background-color: #4dddff;\r\n    transform: scaleX(0);\r\n    transform-origin: right;\r\n    transition: transform .2s ease-out, opacity .2s ease-out 0.3s;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover:before {\r\n    transform: scaleX(1);\r\n    transform-origin: left;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-trans[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%]:before, .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%]:before, .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%]:before {\r\n    transform: scaleX(1);\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before {\r\n    background-color: #4dddff;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-trans[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-trans[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%], .navbar-default.navbar-trans[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%] {\r\n    transition: all .5s ease-in-out;\r\n    padding-top: 19px;\r\n    padding-bottom: 19px;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:before {\r\n    background-color: #4dddff;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .show[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .active[_ngcontent-%COMP%]    > .nav-link[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.show[_ngcontent-%COMP%], .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default.navbar-reduce[_ngcontent-%COMP%]   .navbar-brand[_ngcontent-%COMP%] {\r\n    color: #000000;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%] {\r\n    border-top: 0;\r\n    border-left: 4px solid #4dddff;\r\n    border-right: 0;\r\n    border-bottom: 0;\r\n    transform: translate3d(0px, -40px, 0px);\r\n    opacity: 0;\r\n    filter: alpha(opacity=0);\r\n    visibility: hidden;\r\n    transition: all 0.5s cubic-bezier(0.3, 0.65, 0.355, 1) 0s, opacity 0.31s ease 0s, height 0s linear 0.36s;\r\n    margin: 0;\r\n    border-radius: 0;\r\n    padding: 12px 0;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%] {\r\n    padding: 12px 18px;\r\n    transition: all 500ms ease;\r\n    font-weight: 600;\r\n    min-width: 220px;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .dropdown-item[_ngcontent-%COMP%]:hover {\r\n    background-color: #ffffff;\r\n    color: #4dddff;\r\n    transition: all 500ms ease;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-menu[_ngcontent-%COMP%]   .dropdown-item.active[_ngcontent-%COMP%] {\r\n    background-color: #ffffff;\r\n    color: #4dddff;\r\n  }\r\n\r\n  .navbar-default[_ngcontent-%COMP%]   .dropdown[_ngcontent-%COMP%]:hover   .dropdown-menu[_ngcontent-%COMP%] {\r\n    transform: translate3d(0px, 0px, 0px);\r\n    visibility: visible;\r\n    opacity: 1;\r\n    filter: alpha(opacity=1);\r\n  }\r\n\r\n  .intro-img[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height:80ch;\r\n}\r\n\r\n  .centered[_ngcontent-%COMP%] {\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform: translate(-50%, -50%);\r\n    color: white;\r\n  }\r\n\r\n  .nav-item[_ngcontent-%COMP%]{\r\n    padding-left: 30px;\r\n}\r\n\r\n  .navbar[_ngcontent-%COMP%]{\r\n  height: 100px;\r\n  \r\n}\r\n\r\n  \r\n\r\n  .slider[_ngcontent-%COMP%]{\r\n    position: relative;\r\n    height: 80ch;\r\n}\r\n\r\n  .overlay[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height: 80ch;\r\n    position: absolute;\r\n    background-color: rgba(0, 0, 0, 0.3);\r\n}\r\n\r\n  .overlay-txt[_ngcontent-%COMP%]{\r\n    color: white;\r\n    z-index: 100;\r\n    left: 25%;\r\n    top:300px;\r\n    position: absolute;\r\n}\r\n\r\n  .overlay-txt1[_ngcontent-%COMP%]{\r\n  color: white;\r\n  z-index: 100;\r\n  left: 40%;\r\n  top:200px;\r\n  position: absolute;\r\n}\r\n\r\n  .overlay-txt2[_ngcontent-%COMP%]{\r\n  color: white;\r\n  z-index: 100;\r\n  left: 10%;\r\n  top:250px;\r\n  position: absolute;\r\n  padding: 20px 20px 20px 20px;\r\n\r\n}\r\n\r\n  .slider-pre[_ngcontent-%COMP%]{\r\n    top:400px;\r\n    position: absolute;\r\n    left:100px;\r\n    background-color: transparent;\r\n    border: 1px solid white;\r\n    z-index:10000;\r\n    width:60px;\r\n    height: 60px;\r\n}\r\n\r\n  .slider-pre[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%]{\r\n    color: white;\r\n\r\n\r\n}\r\n\r\n  .slider-next[_ngcontent-%COMP%]{\r\n    top:400px;\r\n    position:absolute;\r\n    right: 100px;\r\n    border: 1px solid white;\r\n    z-index:10000;\r\n    width:60px;\r\n    height: 60px;\r\n}\r\n\r\n  .slider-next[_ngcontent-%COMP%]:hover, .slider-pre[_ngcontent-%COMP%]:hover{\r\n  background-color: #4dddff;\r\n  border: 0px;\r\n}\r\n\r\n  .slider-next[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%]{\r\n  color: white;\r\n}\r\n\r\n  .text1[_ngcontent-%COMP%]{\r\n  font-size: 80px;\r\n  font-weight: 500;\r\n  font-family:Amaranth ;\r\n}\r\n\r\n  .text2[_ngcontent-%COMP%]{\r\n  font-size: 30px;\r\n  font-family: Amaranth;\r\n}\r\n\r\n  .image-section1[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 60ch;\r\n}\r\n\r\n  .section1-container[_ngcontent-%COMP%]{\r\n  margin-top: -165px;\r\n  height: 80ch;\r\n  margin-left: -15px;\r\n  margin-right:0px;\r\n}\r\n\r\n  .section1-container_a[_ngcontent-%COMP%]{\r\n  margin-top: -50px;\r\n  height: 80ch;\r\n  margin-left: -15px;\r\n  margin-right:0px;\r\n}\r\n\r\n  .text-heading[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n  font-weight: 600;\r\n  font-family: Amaranth;\r\n  padding-top: 50px;\r\n  padding-left: 50px;\r\n}\r\n\r\n  .text-heading_a[_ngcontent-%COMP%]{\r\n  font-size: 50px;\r\n  font-weight: 600;\r\n  font-family: Amaranth;\r\n  padding-top: 120px;\r\n  padding-left: 50px;\r\n  padding-bottom: 20px;\r\n}\r\n\r\n  .text-subheading[_ngcontent-%COMP%]{\r\n  font-size: 20px;\r\n  font-weight: 100;\r\n  padding-left: 50px;\r\n\r\n}\r\n\r\n  .parallax[_ngcontent-%COMP%] {\r\n  \r\n  background-image: url('brita_experience_personal_hydration_needs_woman_drinking_water.jpg');\r\n\r\n  \r\n  height: 100ch;\r\n  width:100% !important;\r\n\r\n  \r\n  background-attachment: fixed;\r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n}\r\n\r\n  .features[_ngcontent-%COMP%]{\r\n  margin-top:200px ;\r\n}\r\n\r\n  \r\n\r\n  .image-section1[_ngcontent-%COMP%]{\r\n  margin-top: 50px;\r\n  margin-right: -50px;\r\n}\r\n\r\n  .img-water[_ngcontent-%COMP%] {\r\n  position: absolute;\r\n  left: 0px;\r\n  top: 0px;\r\n  z-index: -1;\r\n}\r\n\r\n  .download[_ngcontent-%COMP%]{\r\n  width: 400px;\r\n  height: 150px;\r\n}\r\n\r\n  .text-download[_ngcontent-%COMP%]{\r\n  font-size: 30px;\r\n  font-weight: 400;\r\n  padding-left: 50px;\r\n\r\n}\r\n\r\n  .colour[_ngcontent-%COMP%]{\r\n  color: #4dddff;\r\n  padding-left: 100px;\r\n}\r\n\r\n  .test[_ngcontent-%COMP%]{\r\n  \r\n\r\n  background-image: url('water.png');\r\n  height: 500px;\r\n}\r\n\r\n  .row[_ngcontent-%COMP%]{\r\n  width: 100% !important;\r\n\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7RUFDRTs7eUNBRXVDOztFQUV2QztJQUNFLCtCQUErQjtJQUMvQix5QkFBeUI7SUFDekIsaUJBQWlCO0lBQ2pCLG9CQUFvQjtJQUNwQixtQ0FBbUM7SUFDbkMsMkJBQTJCO0lBQzNCLGlEQUFpRDtJQUNqRCxxQkFBcUI7RUFDdkI7O0VBRUE7SUFDRSxjQUFjO0lBQ2QsaUJBQWlCOztFQUVuQjs7RUFFQTtJQUNFLGlEQUFpRDtFQUNuRDs7RUFFQTs7SUFFRSxtQ0FBbUM7SUFDbkMsMkJBQTJCO0VBQzdCOztFQUVBOztJQUVFLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGNBQWM7RUFDaEI7O0VBRUE7O0lBRUUsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsdUJBQXVCO0lBQ3ZCLGdDQUFnQztJQUNoQyxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGdCQUFnQjtFQUNsQjs7RUFFQTs7SUFFRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxPQUFPO0lBQ1AsV0FBVztJQUNYLFdBQVc7SUFDWCxVQUFVO0lBQ1YseUJBQXlCO0lBRXpCLG9CQUFvQjtJQUVwQix1QkFBdUI7SUFFdkIsNkRBQTZEO0VBRS9EOztFQUVBOztJQUVFLGNBQWM7RUFDaEI7O0VBRUE7O0lBR0Usb0JBQW9CO0lBRXBCLHNCQUFzQjtFQUN4Qjs7RUFFQTs7Ozs7Ozs7SUFTRSxvQkFBb0I7RUFDdEI7O0VBRUE7SUFDRSx5QkFBeUI7RUFDM0I7O0VBRUE7SUFDRSxjQUFjO0VBQ2hCOztFQUVBOzs7O0lBSUUsY0FBYztFQUNoQjs7RUFFQTtJQUNFLCtCQUErQjtJQUMvQixpQkFBaUI7SUFDakIsb0JBQW9CO0VBQ3RCOztFQUVBO0lBQ0UsY0FBYztFQUNoQjs7RUFFQTtJQUNFLHlCQUF5QjtFQUMzQjs7RUFFQTtJQUNFLGNBQWM7RUFDaEI7O0VBRUE7Ozs7SUFJRSxjQUFjO0VBQ2hCOztFQUVBO0lBQ0UsY0FBYztFQUNoQjs7RUFFQTtJQUNFLGFBQWE7SUFDYiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGdCQUFnQjtJQUVoQix1Q0FBdUM7SUFDdkMsVUFBVTtJQUNWLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsd0dBQXdHO0lBQ3hHLFNBQVM7SUFDVCxnQkFBZ0I7SUFDaEIsZUFBZTtFQUNqQjs7RUFFQTtJQUNFLGtCQUFrQjtJQUNsQiwwQkFBMEI7SUFDMUIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtFQUNsQjs7RUFFQTtJQUNFLHlCQUF5QjtJQUN6QixjQUFjO0lBQ2QsMEJBQTBCO0VBQzVCOztFQUVBO0lBQ0UseUJBQXlCO0lBQ3pCLGNBQWM7RUFDaEI7O0VBRUE7SUFFRSxxQ0FBcUM7SUFDckMsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVix3QkFBd0I7RUFDMUI7O0VBRUY7SUFDSSxXQUFXO0lBQ1gsV0FBVztBQUNmOztFQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1QsZ0NBQWdDO0lBQ2hDLFlBQVk7RUFDZDs7RUFFRjtJQUNJLGtCQUFrQjtBQUN0Qjs7RUFDQTtFQUNFLGFBQWE7RUFDYiw4QkFBOEI7QUFDaEM7O0VBQ0U7O3lDQUV1Qzs7RUFJekM7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtBQUNoQjs7RUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLG9DQUFvQztBQUN4Qzs7RUFDQTtJQUNJLFlBQVk7SUFDWixZQUFZO0lBQ1osU0FBUztJQUNULFNBQVM7SUFDVCxrQkFBa0I7QUFDdEI7O0VBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLFNBQVM7RUFDVCxTQUFTO0VBQ1Qsa0JBQWtCO0FBQ3BCOztFQUNBO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixTQUFTO0VBQ1QsU0FBUztFQUNULGtCQUFrQjtFQUNsQiw0QkFBNEI7O0FBRTlCOztFQUdBO0lBQ0ksU0FBUztJQUNULGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsNkJBQTZCO0lBQzdCLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7O0VBQ0E7SUFDSSxZQUFZOzs7QUFHaEI7O0VBQ0E7SUFDSSxTQUFTO0lBQ1QsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLFVBQVU7SUFDVixZQUFZO0FBQ2hCOztFQUNBO0VBQ0UseUJBQXlCO0VBQ3pCLFdBQVc7QUFDYjs7RUFFQTtFQUNFLFlBQVk7QUFDZDs7RUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCO0FBQ3ZCOztFQUNBO0VBQ0UsZUFBZTtFQUNmLHFCQUFxQjtBQUN2Qjs7RUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7O0VBRUE7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7O0VBQ0E7RUFDRSxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixnQkFBZ0I7QUFDbEI7O0VBRUE7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsa0JBQWtCO0FBQ3BCOztFQUNBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixvQkFBb0I7QUFDdEI7O0VBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjs7QUFFcEI7O0VBQ0E7RUFDRSxtQkFBbUI7RUFDbkIsMkZBQWtIOztFQUVsSCwwQkFBMEI7RUFDMUIsYUFBYTtFQUNiLHFCQUFxQjs7RUFFckIseUNBQXlDO0VBQ3pDLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0IsNEJBQTRCO0VBQzVCLHNCQUFzQjtBQUN4Qjs7RUFFQTtFQUNFLGlCQUFpQjtBQUNuQjs7RUFJQTs7R0FFRzs7RUFDRjtFQUNDLGdCQUFnQjtFQUNoQixtQkFBbUI7QUFDckI7O0VBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFFBQVE7RUFDUixXQUFXO0FBQ2I7O0VBQ0E7RUFDRSxZQUFZO0VBQ1osYUFBYTtBQUNmOztFQUNBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7O0FBRXBCOztFQUNBO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtBQUNyQjs7RUFFQTtFQUNFLCtHQUErRzs7RUFFL0csa0NBQXlEO0VBQ3pELGFBQWE7QUFDZjs7RUFFQTtFQUNFLHNCQUFzQjs7QUFFeEIiLCJmaWxlIjoic3JjL2FwcC93ZWIvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuICAvKj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLy8tLS8vLS0+ICAgTkFWQkFSXHJcbiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG5cclxuICAubmF2YmFyLWRlZmF1bHQge1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlLWluLW91dDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBwYWRkaW5nLXRvcDogMjhweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAyOHB4O1xyXG4gICAgLXdlYmtpdC1iYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICBib3gtc2hhZG93OiAxcHggMnB4IDE1cHggcmdiYSgxMDAsIDEwMCwgMTAwLCAwLjMpO1xyXG4gICAgZm9udC1mYW1pbHk6QWNsb25pY2EgO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0IC5uYXYtc2VhcmNoIHtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcblxyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2Uge1xyXG4gICAgYm94LXNoYWRvdzogMXB4IDJweCAxNXB4IHJnYmEoMTAwLCAxMDAsIDEwMCwgMC4zKTtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2Uge1xyXG4gICAgLXdlYmtpdC1iYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5uYXYtaXRlbSxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWl0ZW0ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluayB7XHJcbiAgICBmb250LXNpemU6IDEuMXJlbTtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAwLjAzMGVtO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuMXMgZWFzZS1pbi1vdXQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbms6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluazpiZWZvcmUge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDVweDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMnB4O1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0ZGRkZmY7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGVYKDApO1xyXG4gICAgdHJhbnNmb3JtOiBzY2FsZVgoMCk7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IHJpZ2h0O1xyXG4gICAgdHJhbnNmb3JtLW9yaWdpbjogcmlnaHQ7XHJcbiAgICB0cmFuc2l0aW9uOiBvcGFjaXR5IC4ycyBlYXNlLW91dCAwLjNzLCAtd2Via2l0LXRyYW5zZm9ybSAuMnMgZWFzZS1vdXQ7XHJcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjJzIGVhc2Utb3V0LCBvcGFjaXR5IC4ycyBlYXNlLW91dCAwLjNzO1xyXG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC4ycyBlYXNlLW91dCwgb3BhY2l0eSAuMnMgZWFzZS1vdXQgMC4zcywgLXdlYmtpdC10cmFuc2Zvcm0gLjJzIGVhc2Utb3V0O1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbms6aG92ZXIsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rOmhvdmVyIHtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbms6aG92ZXI6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluazpob3ZlcjpiZWZvcmUge1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlWCgxKTtcclxuICAgIHRyYW5zZm9ybTogc2NhbGVYKDEpO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOiBsZWZ0O1xyXG4gICAgdHJhbnNmb3JtLW9yaWdpbjogbGVmdDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLnNob3cgPiAubmF2LWxpbms6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLmFjdGl2ZSA+IC5uYXYtbGluazpiZWZvcmUsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbmsuc2hvdzpiZWZvcmUsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbmsuYWN0aXZlOmJlZm9yZSxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAuc2hvdyA+IC5uYXYtbGluazpiZWZvcmUsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLmFjdGl2ZSA+IC5uYXYtbGluazpiZWZvcmUsXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rLnNob3c6YmVmb3JlLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIC5uYXYtbGluay5hY3RpdmU6YmVmb3JlIHtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZVgoMSk7XHJcbiAgICB0cmFuc2Zvcm06IHNjYWxlWCgxKTtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rOmJlZm9yZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGRkZGZmO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci10cmFucyAubmF2LWxpbms6aG92ZXIge1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5zaG93ID4gLm5hdi1saW5rLFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLmFjdGl2ZSA+IC5uYXYtbGluayxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXRyYW5zIC5uYXYtbGluay5zaG93LFxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItdHJhbnMgLm5hdi1saW5rLmFjdGl2ZSB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdC5uYXZiYXItcmVkdWNlIHtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICBwYWRkaW5nLXRvcDogMTlweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxOXB4O1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rIHtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rOmJlZm9yZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGRkZGZmO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdi1saW5rOmhvdmVyIHtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLnNob3cgPiAubmF2LWxpbmssXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLmFjdGl2ZSA+IC5uYXYtbGluayxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWxpbmsuc2hvdyxcclxuICAubmF2YmFyLWRlZmF1bHQubmF2YmFyLXJlZHVjZSAubmF2LWxpbmsuYWN0aXZlIHtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0Lm5hdmJhci1yZWR1Y2UgLm5hdmJhci1icmFuZCB7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICB9XHJcblxyXG4gIC5uYXZiYXItZGVmYXVsdCAuZHJvcGRvd24gLmRyb3Bkb3duLW1lbnUge1xyXG4gICAgYm9yZGVyLXRvcDogMDtcclxuICAgIGJvcmRlci1sZWZ0OiA0cHggc29saWQgIzRkZGRmZjtcclxuICAgIGJvcmRlci1yaWdodDogMDtcclxuICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMHB4LCAtNDBweCwgMHB4KTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMHB4LCAtNDBweCwgMHB4KTtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICBmaWx0ZXI6IGFscGhhKG9wYWNpdHk9MCk7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cyBjdWJpYy1iZXppZXIoMC4zLCAwLjY1LCAwLjM1NSwgMSkgMHMsIG9wYWNpdHkgMC4zMXMgZWFzZSAwcywgaGVpZ2h0IDBzIGxpbmVhciAwLjM2cztcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBwYWRkaW5nOiAxMnB4IDA7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQgLmRyb3Bkb3duIC5kcm9wZG93bi1tZW51IC5kcm9wZG93bi1pdGVtIHtcclxuICAgIHBhZGRpbmc6IDEycHggMThweDtcclxuICAgIHRyYW5zaXRpb246IGFsbCA1MDBtcyBlYXNlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIG1pbi13aWR0aDogMjIwcHg7XHJcbiAgfVxyXG5cclxuICAubmF2YmFyLWRlZmF1bHQgLmRyb3Bkb3duIC5kcm9wZG93bi1tZW51IC5kcm9wZG93bi1pdGVtOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBjb2xvcjogIzRkZGRmZjtcclxuICAgIHRyYW5zaXRpb246IGFsbCA1MDBtcyBlYXNlO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0IC5kcm9wZG93biAuZHJvcGRvd24tbWVudSAuZHJvcGRvd24taXRlbS5hY3RpdmUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIGNvbG9yOiAjNGRkZGZmO1xyXG4gIH1cclxuXHJcbiAgLm5hdmJhci1kZWZhdWx0IC5kcm9wZG93bjpob3ZlciAuZHJvcGRvd24tbWVudSB7XHJcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMHB4LCAwcHgsIDBweCk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDBweCwgMHB4LCAwcHgpO1xyXG4gICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICBmaWx0ZXI6IGFscGhhKG9wYWNpdHk9MSk7XHJcbiAgfVxyXG5cclxuLmludHJvLWltZ3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OjgwY2g7XHJcbn1cclxuLmNlbnRlcmVkIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG5cclxuLm5hdi1pdGVte1xyXG4gICAgcGFkZGluZy1sZWZ0OiAzMHB4O1xyXG59XHJcbi5uYXZiYXJ7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiB5ZWxsb3c7ICovXHJcbn1cclxuICAvKj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbiAgLy8tLS8vLS0+ICAgU2xpZGVyXHJcbiAgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG5cclxuXHJcblxyXG4uc2xpZGVye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiA4MGNoO1xyXG59XHJcbi5vdmVybGF5e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDgwY2g7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbn1cclxuLm92ZXJsYXktdHh0e1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgei1pbmRleDogMTAwO1xyXG4gICAgbGVmdDogMjUlO1xyXG4gICAgdG9wOjMwMHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG59XHJcbi5vdmVybGF5LXR4dDF7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHotaW5kZXg6IDEwMDtcclxuICBsZWZ0OiA0MCU7XHJcbiAgdG9wOjIwMHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxufVxyXG4ub3ZlcmxheS10eHQye1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICB6LWluZGV4OiAxMDA7XHJcbiAgbGVmdDogMTAlO1xyXG4gIHRvcDoyNTBweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcGFkZGluZzogMjBweCAyMHB4IDIwcHggMjBweDtcclxuXHJcbn1cclxuXHJcblxyXG4uc2xpZGVyLXByZXtcclxuICAgIHRvcDo0MDBweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6MTAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xyXG4gICAgei1pbmRleDoxMDAwMDtcclxuICAgIHdpZHRoOjYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbn1cclxuLnNsaWRlci1wcmUgbWF0LWljb257XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcblxyXG5cclxufVxyXG4uc2xpZGVyLW5leHR7XHJcbiAgICB0b3A6NDAwcHg7XHJcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxMDBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xyXG4gICAgei1pbmRleDoxMDAwMDtcclxuICAgIHdpZHRoOjYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbn1cclxuLnNsaWRlci1uZXh0OmhvdmVyLC5zbGlkZXItcHJlOmhvdmVye1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0ZGRkZmY7XHJcbiAgYm9yZGVyOiAwcHg7XHJcbn1cclxuXHJcbi5zbGlkZXItbmV4dCBtYXQtaWNvbntcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLnRleHQxe1xyXG4gIGZvbnQtc2l6ZTogODBweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIGZvbnQtZmFtaWx5OkFtYXJhbnRoIDtcclxufVxyXG4udGV4dDJ7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBBbWFyYW50aDtcclxufVxyXG5cclxuLmltYWdlLXNlY3Rpb24xe1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNjBjaDtcclxufVxyXG5cclxuLnNlY3Rpb24xLWNvbnRhaW5lcntcclxuICBtYXJnaW4tdG9wOiAtMTY1cHg7XHJcbiAgaGVpZ2h0OiA4MGNoO1xyXG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcclxuICBtYXJnaW4tcmlnaHQ6MHB4O1xyXG59XHJcbi5zZWN0aW9uMS1jb250YWluZXJfYXtcclxuICBtYXJnaW4tdG9wOiAtNTBweDtcclxuICBoZWlnaHQ6IDgwY2g7XHJcbiAgbWFyZ2luLWxlZnQ6IC0xNXB4O1xyXG4gIG1hcmdpbi1yaWdodDowcHg7XHJcbn1cclxuXHJcbi50ZXh0LWhlYWRpbmd7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1mYW1pbHk6IEFtYXJhbnRoO1xyXG4gIHBhZGRpbmctdG9wOiA1MHB4O1xyXG4gIHBhZGRpbmctbGVmdDogNTBweDtcclxufVxyXG4udGV4dC1oZWFkaW5nX2F7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgZm9udC1mYW1pbHk6IEFtYXJhbnRoO1xyXG4gIHBhZGRpbmctdG9wOiAxMjBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDUwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbn1cclxuLnRleHQtc3ViaGVhZGluZ3tcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDEwMDtcclxuICBwYWRkaW5nLWxlZnQ6IDUwcHg7XHJcblxyXG59XHJcbi5wYXJhbGxheCB7XHJcbiAgLyogVGhlIGltYWdlIHVzZWQgKi9cclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2JyaXRhX2V4cGVyaWVuY2VfcGVyc29uYWxfaHlkcmF0aW9uX25lZWRzX3dvbWFuX2RyaW5raW5nX3dhdGVyLmpwZ1wiKTtcclxuXHJcbiAgLyogU2V0IGEgc3BlY2lmaWMgaGVpZ2h0ICovXHJcbiAgaGVpZ2h0OiAxMDBjaDtcclxuICB3aWR0aDoxMDAlICFpbXBvcnRhbnQ7XHJcblxyXG4gIC8qIENyZWF0ZSB0aGUgcGFyYWxsYXggc2Nyb2xsaW5nIGVmZmVjdCAqL1xyXG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG5cclxuLmZlYXR1cmVze1xyXG4gIG1hcmdpbi10b3A6MjAwcHggO1xyXG59XHJcblxyXG5cclxuXHJcbi8qIC5zZWN0aW9uMS1jb250YWluZXJ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG59ICovXHJcbiAuaW1hZ2Utc2VjdGlvbjF7XHJcbiAgbWFyZ2luLXRvcDogNTBweDtcclxuICBtYXJnaW4tcmlnaHQ6IC01MHB4O1xyXG59XHJcbi5pbWctd2F0ZXIge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiAwcHg7XHJcbiAgdG9wOiAwcHg7XHJcbiAgei1pbmRleDogLTE7XHJcbn1cclxuLmRvd25sb2Fke1xyXG4gIHdpZHRoOiA0MDBweDtcclxuICBoZWlnaHQ6IDE1MHB4O1xyXG59XHJcbi50ZXh0LWRvd25sb2Fke1xyXG4gIGZvbnQtc2l6ZTogMzBweDtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIHBhZGRpbmctbGVmdDogNTBweDtcclxuXHJcbn1cclxuLmNvbG91cntcclxuICBjb2xvcjogIzRkZGRmZjtcclxuICBwYWRkaW5nLWxlZnQ6IDEwMHB4O1xyXG59XHJcblxyXG4udGVzdHtcclxuICAvKiA8aW1nIGNsYXNzPVwiaW1nLXdhdGVyXCIgc3JjPVwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9zdG9jay1waG90b2dyYXBoeS13YXRlci1kcm9wLWR5bmFtaWMtd2F0ZXItZHJvcHMuanBnXCIgPiAqL1xyXG5cclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaW1hZ2VzL3dhdGVyLnBuZ1wiKTtcclxuICBoZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4ucm93e1xyXG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcblxyXG59Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-home',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "B4AA":
    /*!*******************************************************************!*\
      !*** ./src/app/web/features/leaderboard/leaderboard.component.ts ***!
      \*******************************************************************/

    /*! exports provided: LeaderboardComponent */

    /***/
    function B4AA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LeaderboardComponent", function () {
        return LeaderboardComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var LeaderboardComponent = /*#__PURE__*/function () {
        function LeaderboardComponent() {
          _classCallCheck(this, LeaderboardComponent);
        }

        _createClass(LeaderboardComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return LeaderboardComponent;
      }();

      LeaderboardComponent.ɵfac = function LeaderboardComponent_Factory(t) {
        return new (t || LeaderboardComponent)();
      };

      LeaderboardComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: LeaderboardComponent,
        selectors: [["app-leaderboard"]],
        decls: 9,
        vars: 0,
        consts: [[1, "row", 2, "background-color", "red", "width", "100vw !important"], [1, "tm-2-col-img-text", "tm-2-col-img-lg-right"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-6", "col-xl-6", "tm-2-col-img"], ["src", "../../assets/img/feature-leaderboard.jpg", "alt", "Image", 1, "img-fluid"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-6", "col-xl-6", "tm-2-col-text"], [1, "tm-2-col-text-title"], [1, "tm-2-col-text-description"]],
        template: function LeaderboardComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Leaderboard");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " You can compete with your colleagues by forming groups with them. If you achive your daily water requirement everday you can lead the leaderboard. And also you can earn badges by leading the leaderboard ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi9mZWF0dXJlcy9sZWFkZXJib2FyZC9sZWFkZXJib2FyZC5jb21wb25lbnQuY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LeaderboardComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-leaderboard',
            templateUrl: './leaderboard.component.html',
            styleUrls: ['./leaderboard.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "CXBi":
    /*!***********************************************!*\
      !*** ./src/app/Service/contact-us.service.ts ***!
      \***********************************************/

    /*! exports provided: ContactUsService */

    /***/
    function CXBi(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactUsService", function () {
        return ContactUsService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var ContactUsService = /*#__PURE__*/function () {
        function ContactUsService(http) {
          _classCallCheck(this, ContactUsService);

          this.http = http;
          this.url = 'http://localhost:3000/send';
        }

        _createClass(ContactUsService, [{
          key: "sendMessage",
          value: function sendMessage(messageContent) {
            console.log("sendmessage service");
            return this.http.post(this.url, JSON.stringify(messageContent), {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
              }),
              responseType: 'text'
            });
          }
        }]);

        return ContactUsService;
      }();

      ContactUsService.ɵfac = function ContactUsService_Factory(t) {
        return new (t || ContactUsService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
      };

      ContactUsService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: ContactUsService,
        factory: ContactUsService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactUsService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "DzUm":
    /*!***********************************************************!*\
      !*** ./src/app/web/contact-us/header/header.component.ts ***!
      \***********************************************************/

    /*! exports provided: HeaderComponent */

    /***/
    function DzUm(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
        return HeaderComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var HeaderComponent = /*#__PURE__*/function () {
        function HeaderComponent() {
          _classCallCheck(this, HeaderComponent);
        }

        _createClass(HeaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return HeaderComponent;
      }();

      HeaderComponent.ɵfac = function HeaderComponent_Factory(t) {
        return new (t || HeaderComponent)();
      };

      HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: HeaderComponent,
        selectors: [["app-header"]],
        decls: 9,
        vars: 0,
        consts: [["href", "https://fonts.googleapis.com/css?family=Amaranth", "rel", "stylesheet"], [1, "container-fluid"], [1, "tm-section-intro"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-12", "col-xl-12"], [1, "tm-wrapper-center", 2, "width", "100% !important"], [1, "tm-section-intro-title", "heading"], [1, "tm-section-intro-text", "subheading"]],
        template: function HeaderComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Contact Us");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Keep in touch with AQUA Team ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".heading[_ngcontent-%COMP%]{\r\n    font-family:Amaranth;\r\n}\r\n.subheading[_ngcontent-%COMP%]{\r\n    font-family:Amaranth;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2NvbnRhY3QtdXMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSxvQkFBb0I7QUFDeEIiLCJmaWxlIjoic3JjL2FwcC93ZWIvY29udGFjdC11cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGluZ3tcclxuICAgIGZvbnQtZmFtaWx5OkFtYXJhbnRoO1xyXG59XHJcbi5zdWJoZWFkaW5ne1xyXG4gICAgZm9udC1mYW1pbHk6QW1hcmFudGg7XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeaderComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "IK3O":
    /*!*********************************************************!*\
      !*** ./src/app/web/features/charts/charts.component.ts ***!
      \*********************************************************/

    /*! exports provided: ChartsComponent */

    /***/
    function IK3O(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartsComponent", function () {
        return ChartsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ChartsComponent = /*#__PURE__*/function () {
        function ChartsComponent() {
          _classCallCheck(this, ChartsComponent);
        }

        _createClass(ChartsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ChartsComponent;
      }();

      ChartsComponent.ɵfac = function ChartsComponent_Factory(t) {
        return new (t || ChartsComponent)();
      };

      ChartsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ChartsComponent,
        selectors: [["app-charts"]],
        decls: 9,
        vars: 0,
        consts: [[1, "row", 2, "width", "100vw !important"], [1, "tm-2-col-img-text", "tm-2-col-img-lg-left"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-6", "col-xl-6", "tm-2-col-img"], ["src", "../../assets/img/feature-charts.jpg", "alt", "Image", 1, "img-fluid"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-6", "col-xl-6", "tm-2-col-text"], [1, "tm-2-col-text-title"], [1, "tm-2-col-text-description"]],
        template: function ChartsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "History Charts");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " Is it enough to know only today status ? Of course not. You can see your water drinking history by weekly or monthly and also your beverages percentages you drank today by charts. This is important to keep you in track ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi9mZWF0dXJlcy9jaGFydHMvY2hhcnRzLmNvbXBvbmVudC5jc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ChartsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-charts',
            templateUrl: './charts.component.html',
            styleUrls: ['./charts.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "PVIJ":
    /*!************************************************************!*\
      !*** ./src/app/Admin/landingpage/landingpage.component.ts ***!
      \************************************************************/

    /*! exports provided: LandingpageComponent */

    /***/
    function PVIJ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LandingpageComponent", function () {
        return LandingpageComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../navbar/navbar.component */
      "dnlu");
      /* harmony import */


      var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../sidebar/sidebar.component */
      "4uWM");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var LandingpageComponent = /*#__PURE__*/function () {
        function LandingpageComponent() {
          _classCallCheck(this, LandingpageComponent);

          this.model = {
            left: true,
            middle: false,
            right: false
          };
        }

        _createClass(LandingpageComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return LandingpageComponent;
      }();

      LandingpageComponent.ɵfac = function LandingpageComponent_Factory(t) {
        return new (t || LandingpageComponent)();
      };

      LandingpageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: LandingpageComponent,
        selectors: [["app-landingpage"]],
        decls: 21,
        vars: 6,
        consts: [[1, "landing-main"], [1, ""], [1, "sidebar"], [1, "content"], [1, "btn-group", "btn-group-toggle"], ["ngbButtonLabel", "", 1, "btn-primary"], ["type", "checkbox", "ngbButton", "", 3, "ngModel", "ngModelChange"]],
        template: function LandingpageComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-navbar");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-sidebar");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function LandingpageComponent_Template_input_ngModelChange_8_listener($event) {
              return ctx.model.left = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Left (pre-checked) ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "label", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "input", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function LandingpageComponent_Template_input_ngModelChange_11_listener($event) {
              return ctx.model.middle = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " Middle ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "label", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function LandingpageComponent_Template_input_ngModelChange_14_listener($event) {
              return ctx.model.right = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " Right ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "pre");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "json");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.model.left);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.model.middle);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.model.right);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](19, 4, ctx.model));
          }
        },
        directives: [_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_2__["SidebarComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["CheckboxControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["JsonPipe"]],
        styles: [".landing-main[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  min-height: 100vh;\r\n}\r\n.navbar[_ngcontent-%COMP%]{\r\n  \r\n  width: 100%;\r\n  height: 70px;\r\n  background-color: red;\r\n}\r\n.sidebar[_ngcontent-%COMP%]{\r\n  top: 70px;\r\n  position: fixed;\r\n  display: inline-block;\r\n  width: 300px;\r\n  height: 100%;\r\n  \r\n}\r\n.content[_ngcontent-%COMP%]{\r\n  margin-top: 70px;\r\n  margin-left: 300px;\r\n  display: inline-block;\r\n  width: calc(100% - 300px);\r\n  \r\n  height: 100%;\r\n  display: inline-block;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4vbGFuZGluZ3BhZ2UvbGFuZGluZ3BhZ2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7RUFDWCxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLHFCQUFxQjtFQUNyQixXQUFXO0VBQ1gsWUFBWTtFQUNaLHFCQUFxQjtBQUN2QjtBQUNBO0VBQ0UsU0FBUztFQUNULGVBQWU7RUFDZixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFlBQVk7RUFDWiw2QkFBNkI7QUFDL0I7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6Qiw4QkFBOEI7RUFDOUIsWUFBWTtFQUNaLHFCQUFxQjtBQUN2QiIsImZpbGUiOiJzcmMvYXBwL0FkbWluL2xhbmRpbmdwYWdlL2xhbmRpbmdwYWdlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGFuZGluZy1tYWlue1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG59XHJcbi5uYXZiYXJ7XHJcbiAgLyogcG9zaXRpb246IGZpeGVkOyAqL1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNzBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbn1cclxuLnNpZGViYXJ7XHJcbiAgdG9wOiA3MHB4O1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgd2lkdGg6IDMwMHB4O1xyXG4gIGhlaWdodDogMTAwJTtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjsgKi9cclxufVxyXG4uY29udGVudHtcclxuICBtYXJnaW4tdG9wOiA3MHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAzMDBweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDMwMHB4KTtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiB5ZWxsb3c7ICovXHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LandingpageComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-landingpage',
            templateUrl: './landingpage.component.html',
            styleUrls: ['./landingpage.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _web_normal_nav_normal_nav_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./web/normal-nav/normal-nav.component */
      "3mqr");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _web_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./web/footer/footer.component */
      "dhdC");

      var AppComponent = function AppComponent() {
        _classCallCheck(this, AppComponent);

        this.title = 'Aqua';
      };

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)();
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        decls: 5,
        vars: 0,
        consts: [[1, "col"]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-normal-nav");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-footer");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_web_normal_nav_normal_nav_component__WEBPACK_IMPORTED_MODULE_1__["NormalNavComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"], _web_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]],
        styles: ["*[_ngcontent-%COMP%]{\r\n  margin:0px !important;\r\n  padding: 0px !important;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBcUI7RUFDckIsdUJBQXVCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xyXG4gIG1hcmdpbjowcHggIWltcG9ydGFudDtcclxuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "T3bd":
    /*!***********************************************************************!*\
      !*** ./src/app/web/features/dash-features/dash-features.component.ts ***!
      \***********************************************************************/

    /*! exports provided: DashFeaturesComponent */

    /***/
    function T3bd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashFeaturesComponent", function () {
        return DashFeaturesComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var DashFeaturesComponent = /*#__PURE__*/function () {
        function DashFeaturesComponent() {
          _classCallCheck(this, DashFeaturesComponent);
        }

        _createClass(DashFeaturesComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return DashFeaturesComponent;
      }();

      DashFeaturesComponent.ɵfac = function DashFeaturesComponent_Factory(t) {
        return new (t || DashFeaturesComponent)();
      };

      DashFeaturesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: DashFeaturesComponent,
        selectors: [["app-dash-features"]],
        decls: 30,
        vars: 0,
        consts: [["id", "tm-section-2", 1, "row", 2, "background-color", "red", "width", "99vw !important"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-12", "col-xl-12"], ["id", "tmCarousel", "data-ride", "carousel", "data-interval", "false", 1, "carousel", "slide", "tm-carousel"], [1, "carousel-indicators"], ["data-target", "#tmCarousel", "data-slide-to", "0", 1, "active"], ["data-target", "#tmCarousel", "data-slide-to", "1", 1, ""], ["data-target", "#tmCarousel", "data-slide-to", "2", 1, ""], ["role", "listbox", 1, "carousel-inner"], [1, "carousel-item", "active"], [1, "carousel-content"], [1, "tm-carousel-item-title"], [1, "tm-carousel-item-text"], [1, "carousel-item"]],
        template: function DashFeaturesComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ol", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "li", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "li", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "li", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h2", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Check Daily Water Intake");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "When you open the app you can easily find out how many liters of water you should drink today! And also you can clearly see which amount of water you have drink already and which amout of water you should drink to achieve the daily target ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "h2", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Check Water Quality");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "You can easily find out whether the water in your bottle is good to drink or not by looking the pH value, flouoride concentration etc. And also you can see the water evel of the bottle and the battery percentage of the bottle ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h2", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Add Additional Beverages");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "You can add beverages that you drink other than water in one tap. Beacuse there is a considerable amount of water in other beverages such as cofee, ea, fruit juices etc ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi9mZWF0dXJlcy9kYXNoLWZlYXR1cmVzL2Rhc2gtZmVhdHVyZXMuY29tcG9uZW50LmNzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashFeaturesComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-dash-features',
            templateUrl: './dash-features.component.html',
            styleUrls: ['./dash-features.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "TWix":
    /*!*******************************************************!*\
      !*** ./src/app/web/features/intro/intro.component.ts ***!
      \*******************************************************/

    /*! exports provided: IntroComponent */

    /***/
    function TWix(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "IntroComponent", function () {
        return IntroComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var IntroComponent = /*#__PURE__*/function () {
        function IntroComponent() {
          _classCallCheck(this, IntroComponent);
        }

        _createClass(IntroComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return IntroComponent;
      }();

      IntroComponent.ɵfac = function IntroComponent_Factory(t) {
        return new (t || IntroComponent)();
      };

      IntroComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: IntroComponent,
        selectors: [["app-intro"]],
        decls: 10,
        vars: 0,
        consts: [[1, "container-fluid", 2, "width", "100vw!important"], [1, "tm-section-intro"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-12", "col-xl-12"], [1, "tm-wrapper-center", 2, "width", "100% !important"], [1, "tm-section-intro-title", "heading"], [1, "tm-section-intro-text", "subheading"], ["href", "#tm-section-2", 1, "tm-btn-white-big"]],
        template: function IntroComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Features");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Let's find out what you can do with Aqua mobile app ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Explore");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".heading[_ngcontent-%COMP%]{\r\n    font-family:Amaranth;\r\n}\r\n.subheading[_ngcontent-%COMP%]{\r\n    font-family:Amaranth;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2ZlYXR1cmVzL2ludHJvL2ludHJvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxvQkFBb0I7QUFDeEI7QUFDQTtJQUNJLG9CQUFvQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL3dlYi9mZWF0dXJlcy9pbnRyby9pbnRyby5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRpbmd7XHJcbiAgICBmb250LWZhbWlseTpBbWFyYW50aDtcclxufVxyXG4uc3ViaGVhZGluZ3tcclxuICAgIGZvbnQtZmFtaWx5OkFtYXJhbnRoO1xyXG59Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IntroComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-intro',
            templateUrl: './intro.component.html',
            styleUrls: ['./intro.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "WB2T":
    /*!********************************************************!*\
      !*** ./src/app/web/contact-us/contact-us.component.ts ***!
      \********************************************************/

    /*! exports provided: ContactUsComponent */

    /***/
    function WB2T(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function () {
        return ContactUsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _Service_contact_us_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../Service/contact-us.service */
      "CXBi");
      /* harmony import */


      var _header_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./header/header.component */
      "DzUm");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "ofXK"); // import { ConnectionService } from 'connection-service';


      function ContactUsComponent_div_26_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Title is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ContactUsComponent_div_26_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactUsComponent_div_26_div_1_Template, 2, 0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.f.title.errors.required);
        }
      }

      function ContactUsComponent_div_31_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "First Name is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ContactUsComponent_div_31_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactUsComponent_div_31_div_1_Template, 2, 0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.f.firstName.errors.required);
        }
      }

      function ContactUsComponent_div_36_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Last Name is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ContactUsComponent_div_36_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactUsComponent_div_36_div_1_Template, 2, 0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.f.lastName.errors.required);
        }
      }

      function ContactUsComponent_div_41_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Email is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ContactUsComponent_div_41_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Email must be a valid email address");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ContactUsComponent_div_41_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactUsComponent_div_41_div_1_Template, 2, 0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ContactUsComponent_div_41_div_2_Template, 2, 0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.f.email.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.f.email.errors.email);
        }
      }

      function ContactUsComponent_div_48_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "feedback is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ContactUsComponent_div_48_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ContactUsComponent_div_48_div_1_Template, 2, 0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r4.f.firstName.errors.required);
        }
      }

      function ContactUsComponent_div_55_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Accept Ts & Cs is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0(a0) {
        return {
          "is-invalid": a0
        };
      }; // import { MustMatch } from '../helpers/must-match.validator';


      var ContactUsComponent = /*#__PURE__*/function () {
        function ContactUsComponent(fb, contactUsService) {
          _classCallCheck(this, ContactUsComponent);

          this.fb = fb;
          this.contactUsService = contactUsService;
          this.submitted = false;
          this.disabledSubmitButton = true;
          this.registerForm = fb.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            firstName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            feedback: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            // password: ['', [Validators.required, Validators.minLength(6)]],
            // confirmPassword: ['', Validators.required],
            acceptTerms: [false, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].requiredTrue]
          }, {// validator: MustMatch('password', 'confirmPassword')
          });
        } // constructor(private formBuilder: FormBuilder) { }


        _createClass(ContactUsComponent, [{
          key: "oninput",
          value: function oninput() {
            if (this.registerForm.valid) {
              this.disabledSubmitButton = false;
            }
          } // convenience getter for easy access to form fields

        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this2 = this;

            this.submitted = true;
            console.log('onsubmit'); // stop here if form is invalid

            if (this.registerForm.invalid) {
              return;
            }

            this.contactUsService.sendMessage(this.registerForm.value).subscribe(function () {
              alert('Your message has been sent.');

              _this2.registerForm.reset();

              _this2.disabledSubmitButton = true;
            }, function (error) {
              console.log('Error', error);
            }); // display form values on success

            alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
          }
        }, {
          key: "onReset",
          value: function onReset() {
            this.submitted = false;
            this.registerForm.reset();
          }
        }, {
          key: "f",
          get: function get() {
            return this.registerForm.controls;
          }
        }]);

        return ContactUsComponent;
      }();

      ContactUsComponent.ɵfac = function ContactUsComponent_Factory(t) {
        return new (t || ContactUsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_Service_contact_us_service__WEBPACK_IMPORTED_MODULE_2__["ContactUsService"]));
      };

      ContactUsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ContactUsComponent,
        selectors: [["app-contact-us"]],
        hostBindings: function ContactUsComponent_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function ContactUsComponent_input_HostBindingHandler() {
              return ctx.oninput();
            });
          }
        },
        decls: 78,
        vars: 25,
        consts: [[1, "page"], [1, "wrapper"], [1, "container-fluid", 2, "background-image", "url(assets/img/ocean-bg.jpg )", "width", "1490px", "margin-bottom", "20px", "margin-left", "15px", "margin-right", "15px"], [1, "row", "wow", "fadeInUp"], [1, "col-lg-6"], [1, "map", "mb-4", "mb-lg-0"], ["src", "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621", "frameborder", "0", "allowfullscreen", "", 2, "border", "0", "width", "100%", "height", "312px"], [1, "card-body"], [3, "formGroup", "ngSubmit"], [1, "form-row"], [1, "form-group", "col"], [2, "color", "white"], ["formControlName", "title", 1, "form-control", 3, "ngClass"], ["value", ""], ["value", "Mr"], ["value", "Mrs"], ["value", "Miss"], ["value", "Ms"], ["class", "invalid-feedback", 4, "ngIf"], [1, "form-group", "col-5"], ["type", "text", "formControlName", "firstName", 1, "form-control", 3, "ngClass"], ["type", "text", "formControlName", "lastName", 1, "form-control", 3, "ngClass"], [1, "form-group"], ["type", "text", "formControlName", "email", 1, "form-control", 3, "ngClass"], ["formControlName", "feedback", 1, "form-control", 3, "ngClass"], ["src", "assets/img/cont.png", "height", "300", "width", "300", 1, "img-responsive"], [1, "form-group", "form-check"], ["type", "checkbox", "formControlName", "acceptTerms", "id", "acceptTerms", 1, "form-check-input", 3, "ngClass"], ["for", "acceptTerms", 1, "form-check-label", 2, "color", "white"], [1, "text-center"], [1, "btn", "btn-primary", "mr-1"], ["type", "reset", 1, "btn", "btn-secondary", 3, "click"], [1, "col-lg-6", 2, "margin-top", "-150px"], [1, "row"], [1, "col-md-5", "info"], [1, "ion-ios-location-outline"], ["src", "assets/img/home.png", "height", "25", "width", "25", 1, "img-responsive"], [1, "col-md-4", "info"], [1, "ion-ios-email-outline"], ["src", "assets/img/mail.png", "height", "25", "width", "25", 1, "img-responsive"], [1, "col-md-3", "info"], [1, "ion-ios-telephone-outline"], ["src", "assets/img/telephone.png", "height", "25", "width", "25", 1, "img-responsive"], [1, "invalid-feedback"], [4, "ngIf"]],
        template: function ContactUsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-header");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "section", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "iframe", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "form", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ContactUsComponent_Template_form_ngSubmit_11_listener() {
              return ctx.onSubmit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Title");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "select", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "option", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "option", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Mr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "option", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Mrs");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "option", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Miss");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "option", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Ms");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, ContactUsComponent_div_26_Template, 2, 1, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "First Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, ContactUsComponent_div_31_Template, 2, 1, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Last Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "input", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](36, ContactUsComponent_div_36_Template, 2, 1, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "input", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](41, ContactUsComponent_div_41_Template, 3, 2, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Your Feedback");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "textarea", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Your Feedback");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](48, ContactUsComponent_div_48_Template, 2, 1, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "img", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "label", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Accept Terms & Conditions");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](55, ContactUsComponent_div_55_Template, 2, 0, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "button", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Submit");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "button", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ContactUsComponent_Template_button_click_59_listener() {
              return ctx.onReset();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Cancel");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "i", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "img", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, " No.164 / A, Thudella, Ja-Ela, Sri Lanka ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "i", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "img", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "info@aqua.com");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](74, "i", 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "img", 42);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, " +94 11-2231668 +94 77-2414931 +94 77-1535759");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.registerForm);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, ctx.submitted && ctx.f.title.errors));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.title.errors);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](15, _c0, ctx.submitted && ctx.f.firstName.errors));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.firstName.errors);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](17, _c0, ctx.submitted && ctx.f.lastName.errors));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.lastName.errors);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](19, _c0, ctx.submitted && ctx.f.email.errors));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.email.errors);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](21, _c0, ctx.submitted && ctx.f.firstName.errors));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.firstName.errors);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](23, _c0, ctx.submitted && ctx.f.acceptTerms.errors));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.acceptTerms.errors);
          }
        },
        directives: [_header_header_component__WEBPACK_IMPORTED_MODULE_3__["HeaderComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["CheckboxControlValueAccessor"]],
        styles: ["section[_ngcontent-%COMP%] {\r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n  }\r\n\r\n  .wrapper[_ngcontent-%COMP%]{background: url('/assets/img/ocean-bg.png') no-repeat   center center fixed;\r\n    background-size: cover;}\r\n\r\n  section-header[_ngcontent-%COMP%]{\r\n      font-size: 20px;\r\n      background-color: #4E4F50;\r\n    }\r\n\r\n  .map[_ngcontent-%COMP%]   mb-4[_ngcontent-%COMP%]   mb-lg-0[_ngcontent-%COMP%] {\r\n      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);\r\n      transition: 0.3s;\r\n      width: 40%;\r\n    }\r\n\r\n  .map[_ngcontent-%COMP%]   mb-4[_ngcontent-%COMP%]   mb-lg-0[_ngcontent-%COMP%]:hover {\r\n      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);\r\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2NvbnRhY3QtdXMvY29udGFjdC11cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksMkJBQTJCO0lBQzNCLDRCQUE0QjtJQUM1QixzQkFBc0I7RUFDeEI7O0VBRUEsU0FBUywyRUFBMkU7SUFJbEYsc0JBQXNCLENBQUM7O0VBRXZCO01BQ0UsZUFBZTtNQUNmLHlCQUF5QjtJQUMzQjs7RUFFQTtNQUNFLHVDQUF1QztNQUN2QyxnQkFBZ0I7TUFDaEIsVUFBVTtJQUNaOztFQUVBO01BQ0Usd0NBQXdDO0lBQzFDIiwiZmlsZSI6InNyYy9hcHAvd2ViL2NvbnRhY3QtdXMvY29udGFjdC11cy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsic2VjdGlvbiB7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICB9XHJcblxyXG4gIC53cmFwcGVye2JhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWcvb2NlYW4tYmcucG5nJykgbm8tcmVwZWF0ICAgY2VudGVyIGNlbnRlciBmaXhlZDtcclxuICAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO31cclxuXHJcbiAgICBzZWN0aW9uLWhlYWRlcntcclxuICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNEU0RjUwO1xyXG4gICAgfVxyXG5cclxuICAgIC5tYXAgbWItNCBtYi1sZy0wIHtcclxuICAgICAgYm94LXNoYWRvdzogMCA0cHggOHB4IDAgcmdiYSgwLDAsMCwwLjIpO1xyXG4gICAgICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gICAgICB3aWR0aDogNDAlO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubWFwIG1iLTQgbWItbGctMDpob3ZlciB7XHJcbiAgICAgIGJveC1zaGFkb3c6IDAgOHB4IDE2cHggMCByZ2JhKDAsMCwwLDAuMik7XHJcbiAgICB9Il19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ContactUsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-contact-us',
            templateUrl: './contact-us.component.html',
            styleUrls: ['./contact-us.component.css']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _Service_contact_us_service__WEBPACK_IMPORTED_MODULE_2__["ContactUsService"]
          }];
        }, {
          oninput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['input']
          }]
        });
      })();
      /***/

    },

    /***/
    "WpjL":
    /*!************************************************!*\
      !*** ./src/app/Admin/login/login.component.ts ***!
      \************************************************/

    /*! exports provided: LoginComponent */

    /***/
    function WpjL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginComponent", function () {
        return LoginComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _Service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../Service/auth.service */
      "enR7");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/form-field */
      "IRfi");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/input */
      "A2Vd");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/icon */
      "TY1r");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/button */
      "Xlwt");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function LoginComponent_div_29_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Invalid Password or Email");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var LoginComponent = /*#__PURE__*/function () {
        function LoginComponent(auth, router) {
          _classCallCheck(this, LoginComponent);

          this.auth = auth;
          this.router = router;
          this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
          });
          this.error = false;
          this.visisbility = true;
        }

        _createClass(LoginComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "submit",
          value: function submit() {
            var _this3 = this;

            if (!this.loginForm.valid) {
              return;
            }

            this.auth.login(this.loginForm.value).subscribe(function (data) {
              console.log(data);

              if (data.status) {
                _this3.router.navigate(['/admin']);
              } else {
                _this3.error = true;
              }
            }, function (error) {
              console.log(error);
            });
            console.log(this.loginForm.value);
          }
        }]);

        return LoginComponent;
      }();

      LoginComponent.ɵfac = function LoginComponent_Factory(t) {
        return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_Service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]));
      };

      LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: LoginComponent,
        selectors: [["app-login"]],
        decls: 33,
        vars: 4,
        consts: [[1, "login-main"], [1, "login-card"], [1, "header-img"], ["src", "../../../assets/images/logo.png"], [1, "login-form", 3, "formGroup", "submit"], [1, "form-field"], ["matInput", "", "type", "text", "formControlName", "email", "required", "", 1, "example-right-align"], ["matPrefix", ""], ["matInput", "", "formControlName", "password", "required", "", 1, "example-right-align", 3, "type"], ["matSuffix", ""], ["mat-icon-button", "", "type", "button"], [3, "click"], ["class", "error", 4, "ngIf"], [1, "signin-btn"], ["type", "submit"], [1, "error"]],
        template: function LoginComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h3");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Admin Login");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "form", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("submit", function LoginComponent_Template_form_submit_6_listener() {
              return ctx.submit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "input", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Required");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-form-field", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "span", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "lock");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "span", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "mat-icon", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LoginComponent_Template_mat_icon_click_25_listener() {
              return ctx.visisbility = !ctx.visisbility;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Required");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, LoginComponent_div_29_Template, 2, 0, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "SignIn");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.loginForm);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("type", ctx.visisbility ? "password" : "text");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.visisbility ? "visibility" : "visibility_off");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatPrefix"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIcon"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatError"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatSuffix"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"]],
        styles: ["@import url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@1,300&family=Roboto:wght@100&family=Yellowtail&display=swap');\r\n\r\n\r\n*[_ngcontent-%COMP%]{\r\n  margin:0px;\r\n  padding:0px;\r\n}\r\n\r\n\r\n.login-main[_ngcontent-%COMP%]{\r\n  \r\n  \r\n  padding-top:50px;\r\n  \r\n  width: 100%;\r\n  height: 100vh;\r\n  box-sizing: border-box;\r\n  background-color: rgba(33, 25, 173, 0.2);\r\n}\r\n\r\n\r\n.login-card[_ngcontent-%COMP%]{\r\n  width: 450px;\r\n  height: 550px;\r\n  \r\n  box-shadow: 5px 10px 18px #888888;\r\n  background-color: white;\r\n  margin: auto;\r\n\r\n  border-radius: 15px;\r\n}\r\n\r\n\r\n.header-img[_ngcontent-%COMP%]{\r\n  padding-top: 10px;\r\n  \r\n  width:200px;\r\n  \r\n  margin: auto;\r\n  margin-top: 10px;\r\n  \r\n}\r\n\r\n\r\n.header-img[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 200px;\r\n}\r\n\r\n\r\n.header-img[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\r\n  margin-top: 20px;\r\n  text-align: center;\r\n  font-weight: 400;\r\n\r\n\r\n  \r\n}\r\n\r\n\r\n.login-form[_ngcontent-%COMP%]{\r\n  margin-top: 20px;\r\n  width: 100%;\r\n  \r\n  padding-left: 10%;\r\n  padding-right: 10%;\r\n  box-sizing: border-box;\r\n}\r\n\r\n\r\n.form-field[_ngcontent-%COMP%]{\r\n  padding-top: 30px;\r\n  \r\n  margin-top: 80px;\r\n  height: 100px;\r\n  width: 100%;\r\n  margin: auto;\r\n}\r\n\r\n\r\n.signin-btn[_ngcontent-%COMP%]{\r\n  margin: auto;\r\n  width: 100%;\r\n  text-align: center;\r\n\r\n}\r\n\r\n\r\n.signin-btn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  padding: 0px;\r\n  padding-top: -10px;\r\n  margin: auto;\r\n  width: 100px;\r\n  height: 45px;\r\n  font-weight: 600;\r\n  border-radius: 10px;\r\n  border: 0px;\r\n  color: white;\r\n  font-size: 14px;\r\n  background-color: rgb(17, 17, 143);\r\n  \r\n}\r\n\r\n\r\n.error[_ngcontent-%COMP%]{\r\n  color: red;\r\n  font-size: 12px;\r\n  margin-bottom: 20px;\r\n  width: 100%;\r\n  text-align: center;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4vbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSx1SUFBdUk7OztBQUd2STtFQUNFLFVBQVU7RUFDVixXQUFXO0FBQ2I7OztBQUdBO0VBQ0UsdUJBQXVCO0VBQ3ZCLDBCQUEwQjtFQUMxQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHdDQUF3QztBQUMxQzs7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixpQ0FBaUM7RUFDakMsdUJBQXVCO0VBQ3ZCLFlBQVk7O0VBRVosbUJBQW1CO0FBQ3JCOzs7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQiwyQkFBMkI7RUFDM0IsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLHdCQUF3QjtBQUMxQjs7O0FBQ0E7RUFDRSxZQUFZO0FBQ2Q7OztBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixnQkFBZ0I7OztFQUdoQix3Q0FBd0M7QUFDMUM7OztBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCwyQkFBMkI7RUFDM0IsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixzQkFBc0I7QUFDeEI7OztBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLFdBQVc7RUFDWCxZQUFZO0FBQ2Q7OztBQUNBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7O0FBRXBCOzs7QUFDQTtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFlBQVk7RUFDWixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQWU7RUFDZixrQ0FBa0M7RUFDbEMsdUNBQXVDO0FBQ3pDOzs7QUFDQTtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9T3BlbitTYW5zOml0YWwsd2dodEAxLDMwMCZmYW1pbHk9Um9ib3RvOndnaHRAMTAwJmZhbWlseT1ZZWxsb3d0YWlsJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuXHJcbip7XHJcbiAgbWFyZ2luOjBweDtcclxuICBwYWRkaW5nOjBweDtcclxufVxyXG5cclxuXHJcbi5sb2dpbi1tYWlue1xyXG4gIC8qIG1hcmdpbi10b3A6IC0xMHB4OyAqL1xyXG4gIC8qIG1hcmdpbi1ib3R0b206IC0xMHB4OyAqL1xyXG4gIHBhZGRpbmctdG9wOjUwcHg7XHJcbiAgLyogcGFkZGluZzogMHB4OyAqL1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwdmg7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDMzLCAyNSwgMTczLCAwLjIpO1xyXG59XHJcbi5sb2dpbi1jYXJke1xyXG4gIHdpZHRoOiA0NTBweDtcclxuICBoZWlnaHQ6IDU1MHB4O1xyXG4gIC8qIGJvcmRlcjogMXB4IHNvbGlkOyAqL1xyXG4gIGJveC1zaGFkb3c6IDVweCAxMHB4IDE4cHggIzg4ODg4ODtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW46IGF1dG87XHJcblxyXG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbn1cclxuLmhlYWRlci1pbWd7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogcmVkOyAqL1xyXG4gIHdpZHRoOjIwMHB4O1xyXG4gIC8qIGJvcmRlci1yYWRpdXM6NTAlOyAqL1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIC8qIG1hcmdpbi1sZWZ0OiAxMDBweDsgKi9cclxufVxyXG4uaGVhZGVyLWltZyBpbWd7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG59XHJcbi5oZWFkZXItaW1nIGgze1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcblxyXG5cclxuICAvKiBmb250LWZhbWlseTogJ1llbGxvd3RhaWwnLCBjdXJzaXZlOyAqL1xyXG59XHJcbi5sb2dpbi1mb3Jte1xyXG4gIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogcmVkOyAqL1xyXG4gIHBhZGRpbmctbGVmdDogMTAlO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEwJTtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcbi5mb3JtLWZpZWxke1xyXG4gIHBhZGRpbmctdG9wOiAzMHB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHJlZDsgKi9cclxuICBtYXJnaW4tdG9wOiA4MHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG59XHJcbi5zaWduaW4tYnRue1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB3aWR0aDogMTAwJTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG59XHJcbi5zaWduaW4tYnRuIGJ1dHRvbntcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgcGFkZGluZy10b3A6IC0xMHB4O1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgaGVpZ2h0OiA0NXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXI6IDBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigxNywgMTcsIDE0Myk7XHJcbiAgLyogYm94LXNoYWRvdzogNXB4IDEwcHggMThweCAjODg4ODg4OyAqL1xyXG59XHJcbi5lcnJvcntcclxuICBjb2xvcjogcmVkO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoginComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
          }]
        }], function () {
          return [{
            type: _Service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app-routing.module */
      "vY5A");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _Admin_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./Admin/login/login.component */
      "WpjL");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "R1ws");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/icon */
      "TY1r");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/form-field */
      "IRfi");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/material/input */
      "A2Vd");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @angular/material/button */
      "Xlwt");
      /* harmony import */


      var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/material/snack-bar */
      "0DX0");
      /* harmony import */


      var _Admin_landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./Admin/landingpage/landingpage.component */
      "PVIJ");
      /* harmony import */


      var _Admin_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ./Admin/sidebar/sidebar.component */
      "4uWM");
      /* harmony import */


      var _Admin_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./Admin/navbar/navbar.component */
      "dnlu");
      /* harmony import */


      var _web_home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./web/home/home.component */
      "6OIh");
      /* harmony import */


      var _web_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./web/about-us/about-us.component */
      "jwnt");
      /* harmony import */


      var _Admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./Admin/dashboard/dashboard.component */
      "oEUC");
      /* harmony import */


      var _Admin_register_register_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./Admin/register/register.component */
      "dEXr");
      /* harmony import */


      var _web_footer_footer_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! ./web/footer/footer.component */
      "dhdC");
      /* harmony import */


      var _web_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! ./web/contact-us/contact-us.component */
      "WB2T");
      /* harmony import */


      var _web_features_features_features_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! ./web/features/features/features.component */
      "03M9");
      /* harmony import */


      var _web_features_intro_intro_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! ./web/features/intro/intro.component */
      "TWix");
      /* harmony import */


      var _web_features_charts_charts_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
      /*! ./web/features/charts/charts.component */
      "IK3O");
      /* harmony import */


      var _web_features_leaderboard_leaderboard_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
      /*! ./web/features/leaderboard/leaderboard.component */
      "B4AA");
      /* harmony import */


      var _web_features_profile_profile_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
      /*! ./web/features/profile/profile.component */
      "mTO2");
      /* harmony import */


      var _web_features_dash_features_dash_features_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
      /*! ./web/features/dash-features/dash-features.component */
      "T3bd");
      /* harmony import */


      var _web_normal_nav_normal_nav_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
      /*! ./web/normal-nav/normal-nav.component */
      "3mqr");
      /* harmony import */


      var _web_about_us_heading_heading_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
      /*! ./web/about-us/heading/heading.component */
      "hdHU");
      /* harmony import */


      var _web_contact_us_header_header_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
      /*! ./web/contact-us/header/header.component */
      "DzUm"); // material imports
      // import {MatFormFieldModule} from '@angular/material/form-field';
      //import { ChartsModule } from "ng2-charts";
      // import {MatFormFieldModule} from '@angular/material/form-field';
      // import { ChartsModule } from 'ng2-charts';


      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function AppModule_Factory(t) {
          return new (t || AppModule)();
        },
        providers: [],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], // ChartsModule,
        // Mat modules
        _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"], _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_12__["MatSnackBarModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _Admin_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"], _Admin_landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_13__["LandingpageComponent"], _Admin_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_14__["SidebarComponent"], _Admin_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_15__["NavbarComponent"], _web_home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"], _web_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_17__["AboutUsComponent"], _Admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_18__["DashboardComponent"], _Admin_register_register_component__WEBPACK_IMPORTED_MODULE_19__["RegisterComponent"], _web_footer_footer_component__WEBPACK_IMPORTED_MODULE_20__["FooterComponent"], _web_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_21__["ContactUsComponent"], _web_features_features_features_component__WEBPACK_IMPORTED_MODULE_22__["FeaturesComponent"], _web_features_intro_intro_component__WEBPACK_IMPORTED_MODULE_23__["IntroComponent"], _web_features_charts_charts_component__WEBPACK_IMPORTED_MODULE_24__["ChartsComponent"], _web_features_leaderboard_leaderboard_component__WEBPACK_IMPORTED_MODULE_25__["LeaderboardComponent"], _web_features_profile_profile_component__WEBPACK_IMPORTED_MODULE_26__["ProfileComponent"], _web_features_dash_features_dash_features_component__WEBPACK_IMPORTED_MODULE_27__["DashFeaturesComponent"], _web_normal_nav_normal_nav_component__WEBPACK_IMPORTED_MODULE_28__["NormalNavComponent"], _web_about_us_heading_heading_component__WEBPACK_IMPORTED_MODULE_29__["HeadingComponent"], _web_contact_us_header_header_component__WEBPACK_IMPORTED_MODULE_30__["HeaderComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], // ChartsModule,
          // Mat modules
          _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"], _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_12__["MatSnackBarModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"], _Admin_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"], _Admin_landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_13__["LandingpageComponent"], _Admin_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_14__["SidebarComponent"], _Admin_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_15__["NavbarComponent"], _web_home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"], _web_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_17__["AboutUsComponent"], _Admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_18__["DashboardComponent"], _Admin_register_register_component__WEBPACK_IMPORTED_MODULE_19__["RegisterComponent"], _web_footer_footer_component__WEBPACK_IMPORTED_MODULE_20__["FooterComponent"], _web_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_21__["ContactUsComponent"], _web_features_features_features_component__WEBPACK_IMPORTED_MODULE_22__["FeaturesComponent"], _web_features_intro_intro_component__WEBPACK_IMPORTED_MODULE_23__["IntroComponent"], _web_features_charts_charts_component__WEBPACK_IMPORTED_MODULE_24__["ChartsComponent"], _web_features_leaderboard_leaderboard_component__WEBPACK_IMPORTED_MODULE_25__["LeaderboardComponent"], _web_features_profile_profile_component__WEBPACK_IMPORTED_MODULE_26__["ProfileComponent"], _web_features_dash_features_dash_features_component__WEBPACK_IMPORTED_MODULE_27__["DashFeaturesComponent"], _web_normal_nav_normal_nav_component__WEBPACK_IMPORTED_MODULE_28__["NormalNavComponent"], _web_about_us_heading_heading_component__WEBPACK_IMPORTED_MODULE_29__["HeadingComponent"], _web_contact_us_header_header_component__WEBPACK_IMPORTED_MODULE_30__["HeaderComponent"]],
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"], // ChartsModule,
            // Mat modules
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"], _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"], _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_12__["MatSnackBarModule"]],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "dEXr":
    /*!******************************************************!*\
      !*** ./src/app/Admin/register/register.component.ts ***!
      \******************************************************/

    /*! exports provided: RegisterComponent */

    /***/
    function dEXr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterComponent", function () {
        return RegisterComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var src_app_Service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/app/Service/auth.service */
      "enR7");
      /* harmony import */


      var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/snack-bar */
      "0DX0");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/form-field */
      "IRfi");
      /* harmony import */


      var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/input */
      "A2Vd");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/material/icon */
      "TY1r");
      /* harmony import */


      var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/button */
      "Xlwt");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function RegisterComponent_div_44_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Password does not match");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var RegisterComponent = /*#__PURE__*/function () {
        function RegisterComponent(auth, _snackBar) {
          _classCallCheck(this, RegisterComponent);

          this.auth = auth;
          this._snackBar = _snackBar;
          this.registerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            repassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
          });
          this.visisbility = true;
          this.error1 = false;
          this.error2 = false;
        }

        _createClass(RegisterComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "addUser",
          value: function addUser() {
            var _this4 = this;

            console.log(this.registerForm.value);

            if (!this.registerForm.valid) {
              return;
            } else if (this.registerForm.value.password != this.registerForm.value.repassword) {
              this.error1 = true;
              return;
            } else {
              this.error1 = false;
              this.error2 = false;
            }

            this.auth.register(this.registerForm.value).subscribe(function (data) {
              if (data.status) {
                _this4._snackBar.open("Success", "close", {
                  duration: 2000
                });

                setTimeout(function () {// window.location.reload();
                });
              }
            }, function (error) {
              console.log(error);
              _this4.error2 = true;
            });
          }
        }]);

        return RegisterComponent;
      }();

      RegisterComponent.ɵfac = function RegisterComponent_Factory(t) {
        return new (t || RegisterComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_Service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]));
      };

      RegisterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RegisterComponent,
        selectors: [["app-register"]],
        decls: 48,
        vars: 5,
        consts: [[1, "register-main"], [1, "register-form", 3, "formGroup", "submit"], [1, "register-header"], [1, "form-field"], ["matInput", "", "type", "text", "formControlName", "name", "required", "", 1, "example-right-align"], ["matPrefix", ""], ["matInput", "", "type", "text", "formControlName", "email", "required", "", 1, "example-right-align"], ["matInput", "", "formControlName", "password", "required", "", 1, "example-right-align", 3, "type"], ["matInput", "", "formControlName", "repassword", "required", "", 1, "example-right-align", 3, "type"], ["matSuffix", ""], ["mat-icon-button", "", "type", "button"], [3, "click"], ["class", "error", 4, "ngIf"], [1, "signin-btn"], ["type", "submit"], [1, "error"]],
        template: function RegisterComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("submit", function RegisterComponent_Template_form_submit_1_listener() {
              return ctx.addUser();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Admin Registration");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-form-field", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Name");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "input", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "person");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Required");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "mat-form-field", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "span", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "email");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "mat-error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Invalid");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "mat-form-field", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "span", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "lock");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "mat-error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Required");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "mat-form-field", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "mat-label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Password Confirmation");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "span", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "mat-icon");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "lock");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "mat-error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Required");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "span", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "mat-icon", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RegisterComponent_Template_mat_icon_click_42_listener() {
              return ctx.visisbility = !ctx.visisbility;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](44, RegisterComponent_div_44_Template, 2, 0, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "button", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Add User");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.registerForm);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("type", ctx.visisbility ? "password" : "text");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("type", ctx.visisbility ? "password" : "text");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.visisbility ? "visibility" : "visibility_off");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.error1);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatPrefix"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIcon"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatError"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatSuffix"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"]],
        styles: [".register-main[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 100ch;\r\n  padding-top:100px;\r\n  \r\n}\r\n\r\n.register-form[_ngcontent-%COMP%]{\r\n  padding: 100px;\r\n  padding-top: 50px;\r\n  box-sizing: border-box;\r\n  margin: auto;\r\n  \r\n  width: 60%;\r\n  height: 700px;\r\n  border-radius: 30px;\r\n  box-shadow: 5px 10px 18px #888888;\r\n  \r\n}\r\n\r\n.form-field[_ngcontent-%COMP%]{\r\n  display: block;\r\n  \r\n  \r\n  height: 100px;\r\n}\r\n\r\n.register-header[_ngcontent-%COMP%]{\r\n  \r\n  color: rgb(17, 17, 143);\r\n  height: 100px;\r\n  font-size: 30px;\r\n  text-align: center;\r\n}\r\n\r\n.signin-btn[_ngcontent-%COMP%]{\r\n  margin: auto;\r\n  width: 100%;\r\n  text-align: center;\r\n\r\n}\r\n\r\n.signin-btn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n  padding: 0px;\r\n  padding-top: -10px;\r\n  margin: auto;\r\n  width: 120px;\r\n  height: 50px;\r\n  font-weight: 600;\r\n  border-radius: 5px;\r\n  border: 0px;\r\n  color: white;\r\n  font-size: 14px;\r\n  background-color: rgb(17, 17, 143);\r\n  \r\n}\r\n\r\n.error[_ngcontent-%COMP%]{\r\n  color: red;\r\n  font-size: 12px;\r\n  margin-bottom: 20px;\r\n  width: 100%;\r\n  text-align: center;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4vcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQVc7RUFDWCxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLDJCQUEyQjtBQUM3Qjs7QUFFQTtFQUNFLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsVUFBVTtFQUNWLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsaUNBQWlDO0VBQ2pDLDRCQUE0QjtBQUM5Qjs7QUFDQTtFQUNFLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGFBQWE7QUFDZjs7QUFDQTtFQUNFLHNDQUFzQztFQUN0Qyx1QkFBdUI7RUFDdkIsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjs7QUFFcEI7O0FBQ0E7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixZQUFZO0VBQ1osWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0NBQWtDO0VBQ2xDLHVDQUF1QztBQUN6Qzs7QUFDQTtFQUNFLFVBQVU7RUFDVixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxrQkFBa0I7QUFDcEIiLCJmaWxlIjoic3JjL2FwcC9BZG1pbi9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlZ2lzdGVyLW1haW57XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDBjaDtcclxuICBwYWRkaW5nLXRvcDoxMDBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7ICovXHJcbn1cclxuXHJcbi5yZWdpc3Rlci1mb3Jte1xyXG4gIHBhZGRpbmc6IDEwMHB4O1xyXG4gIHBhZGRpbmctdG9wOiA1MHB4O1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIC8qIG1hcmdpbi10b3A6MjAwcHg7ICovXHJcbiAgd2lkdGg6IDYwJTtcclxuICBoZWlnaHQ6IDcwMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgYm94LXNoYWRvdzogNXB4IDEwcHggMThweCAjODg4ODg4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IGFxdWE7ICovXHJcbn1cclxuLmZvcm0tZmllbGR7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgLyogd2lkdGg6IDgwJTsgKi9cclxuICAvKiBtYXJnaW4tbGVmdDogNSU7ICovXHJcbiAgaGVpZ2h0OiAxMDBweDtcclxufVxyXG4ucmVnaXN0ZXItaGVhZGVye1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHJlYmVjY2FwdXJwbGUgOyAqL1xyXG4gIGNvbG9yOiByZ2IoMTcsIDE3LCAxNDMpO1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnNpZ25pbi1idG57XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbn1cclxuLnNpZ25pbi1idG4gYnV0dG9ue1xyXG4gIHBhZGRpbmc6IDBweDtcclxuICBwYWRkaW5nLXRvcDogLTEwcHg7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgYm9yZGVyOiAwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTcsIDE3LCAxNDMpO1xyXG4gIC8qIGJveC1zaGFkb3c6IDVweCAxMHB4IDE4cHggIzg4ODg4ODsgKi9cclxufVxyXG4uZXJyb3J7XHJcbiAgY29sb3I6IHJlZDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RegisterComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-register',
            templateUrl: './register.component.html',
            styleUrls: ['./register.component.css']
          }]
        }], function () {
          return [{
            type: src_app_Service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
          }, {
            type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "dhdC":
    /*!************************************************!*\
      !*** ./src/app/web/footer/footer.component.ts ***!
      \************************************************/

    /*! exports provided: FooterComponent */

    /***/
    function dhdC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FooterComponent", function () {
        return FooterComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var FooterComponent = /*#__PURE__*/function () {
        function FooterComponent() {
          _classCallCheck(this, FooterComponent);
        }

        _createClass(FooterComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FooterComponent;
      }();

      FooterComponent.ɵfac = function FooterComponent_Factory(t) {
        return new (t || FooterComponent)();
      };

      FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: FooterComponent,
        selectors: [["app-footer"]],
        decls: 48,
        vars: 0,
        consts: [["id", "footer"], [1, "footer-top"], [1, "container"], [1, "row"], [1, "col-lg-6", "col-md-6", "footer-newsletter"], [1, "footer-info"], [2, "margin-bottom", "20px"], ["src", "../../../assets/images/new.png", 1, "logo"], [1, "col-lg-3", "col-md-6"], [1, "copyright"]],
        template: function FooterComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h3");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "OUR COMPANY ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, ".");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "To create a healthy lifestyle avoiding diseases in busy schedules by drinking sufficient amounts of water daily ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "VISIT US");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " No.164 / A, Thudella, ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, " Ja-Ela, ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Sri Lanka");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "EMAIL US");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, " info@aqua.com ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "CONTACT US");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, " +94 11-2231668");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " +94 77-2414931");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " +94 77-1535759");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " \xA9 Copyright ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "strong");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "aqua.lk");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, ". All Rights Reserved ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["#footer[_ngcontent-%COMP%] {\r\n    position:relative;\r\n    background: black;\r\n    padding: 0 0 30px 0;\r\n    color: #fff;\r\n    font-size: 14px;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%] {\r\n    background: #151515;\r\n    border-bottom: 1px solid #222222;\r\n    padding: 60px 0 30px 0;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-info[_ngcontent-%COMP%] {\r\n    margin-bottom: 30px;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-info[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n    font-size: 28px;\r\n    margin: 0 0 20px 0;\r\n    padding: 2px 0 2px 0;\r\n    line-height: 1;\r\n    font-weight: 700;\r\n    text-transform: uppercase;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-info[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\r\n    color: #4eacd4;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-info[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\r\n    font-size: 14px;\r\n    line-height: 24px;\r\n    margin-bottom: 0;\r\n    font-family: \"Raleway\", sans-serif;\r\n    color: #fff;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .social-links[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    font-size: 18px;\r\n    display: inline-block;\r\n    background: #292929;\r\n    color: #fff;\r\n    line-height: 1;\r\n    padding: 8px 0;\r\n    margin-right: 4px;\r\n    border-radius: 4px;\r\n    text-align: center;\r\n    width: 36px;\r\n    height: 36px;\r\n    transition: 0.3s;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .social-links[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\r\n    background: #ffc451;\r\n    color: #151515;\r\n    text-decoration: none;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\r\n    font-size: 16px;\r\n    font-weight: 600;\r\n    color: #fff;\r\n    position: relative;\r\n    padding-bottom: 12px;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%] {\r\n    margin-bottom: 30px;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\r\n    list-style: none;\r\n    padding: 0;\r\n    margin: 0;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\r\n    padding-right: 2px;\r\n    color: #ffc451;\r\n    font-size: 18px;\r\n    line-height: 1;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    padding: 10px 0;\r\n    display: flex;\r\n    align-items: center;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:first-child {\r\n    padding-top: 0;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    color: #fff;\r\n    transition: 0.3s;\r\n    display: inline-block;\r\n    line-height: 1;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-links[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\r\n    color: #ffc451;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-newsletter[_ngcontent-%COMP%]   form[_ngcontent-%COMP%] {\r\n    margin-top: 30px;\r\n    background: #fff;\r\n    padding: 6px 10px;\r\n    position: relative;\r\n    border-radius: 4px;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-newsletter[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   input[type=\"email\"][_ngcontent-%COMP%] {\r\n    border: 0;\r\n    padding: 4px;\r\n    width: calc(100% - 110px);\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-newsletter[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   input[type=\"submit\"][_ngcontent-%COMP%] {\r\n    position: absolute;\r\n    top: 0;\r\n    right: -2px;\r\n    bottom: 0;\r\n    border: 0;\r\n    background: none;\r\n    font-size: 16px;\r\n    padding: 0 20px;\r\n    background: #ffc451;\r\n    color: #151515;\r\n    transition: 0.3s;\r\n    border-radius: 0 4px 4px 0;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .footer-top[_ngcontent-%COMP%]   .footer-newsletter[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   input[type=\"submit\"][_ngcontent-%COMP%]:hover {\r\n    background: #ffcd6b;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .copyright[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n    padding-top: 30px;\r\n  }\r\n#footer[_ngcontent-%COMP%]   .credits[_ngcontent-%COMP%] {\r\n    padding-top: 10px;\r\n    text-align: center;\r\n    font-size: 13px;\r\n    color: #fff;\r\n  }\r\n.logo[_ngcontent-%COMP%]{\r\n      height: 80px;\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7K0RBRStEO0FBQy9EO0lBQ0ksaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLGVBQWU7RUFDakI7QUFFQTtJQUNFLG1CQUFtQjtJQUNuQixnQ0FBZ0M7SUFDaEMsc0JBQXNCO0VBQ3hCO0FBRUE7SUFDRSxtQkFBbUI7RUFDckI7QUFFQTtJQUNFLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIseUJBQXlCO0VBQzNCO0FBRUE7SUFDRSxjQUFjO0VBQ2hCO0FBRUE7SUFDRSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGdCQUFnQjtJQUNoQixrQ0FBa0M7SUFDbEMsV0FBVztFQUNiO0FBRUE7SUFDRSxlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsY0FBYztJQUNkLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGdCQUFnQjtFQUNsQjtBQUVBO0lBQ0UsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxxQkFBcUI7RUFDdkI7QUFFQTtJQUNFLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixvQkFBb0I7RUFDdEI7QUFFQTtJQUNFLG1CQUFtQjtFQUNyQjtBQUVBO0lBQ0UsZ0JBQWdCO0lBQ2hCLFVBQVU7SUFDVixTQUFTO0VBQ1g7QUFFQTtJQUNFLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsZUFBZTtJQUNmLGNBQWM7RUFDaEI7QUFFQTtJQUNFLGVBQWU7SUFDZixhQUFhO0lBQ2IsbUJBQW1CO0VBQ3JCO0FBRUE7SUFDRSxjQUFjO0VBQ2hCO0FBRUE7SUFDRSxXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLHFCQUFxQjtJQUNyQixjQUFjO0VBQ2hCO0FBRUE7SUFDRSxjQUFjO0VBQ2hCO0FBRUE7SUFDRSxnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsa0JBQWtCO0VBQ3BCO0FBRUE7SUFDRSxTQUFTO0lBQ1QsWUFBWTtJQUNaLHlCQUF5QjtFQUMzQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixXQUFXO0lBQ1gsU0FBUztJQUNULFNBQVM7SUFDVCxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLGdCQUFnQjtJQUNoQiwwQkFBMEI7RUFDNUI7QUFFQTtJQUNFLG1CQUFtQjtFQUNyQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLGlCQUFpQjtFQUNuQjtBQUVBO0lBQ0UsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsV0FBVztFQUNiO0FBQ0E7TUFDSSxZQUFZO0VBQ2hCIiwiZmlsZSI6InNyYy9hcHAvd2ViL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIyBGb290ZXJcclxuLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0qL1xyXG4jZm9vdGVyIHtcclxuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICBwYWRkaW5nOiAwIDAgMzBweCAwO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgfVxyXG4gIFxyXG4gICNmb290ZXIgLmZvb3Rlci10b3Age1xyXG4gICAgYmFja2dyb3VuZDogIzE1MTUxNTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMjIyMjIyO1xyXG4gICAgcGFkZGluZzogNjBweCAwIDMwcHggMDtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCAuZm9vdGVyLWluZm8ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCAuZm9vdGVyLWluZm8gaDMge1xyXG4gICAgZm9udC1zaXplOiAyOHB4O1xyXG4gICAgbWFyZ2luOiAwIDAgMjBweCAwO1xyXG4gICAgcGFkZGluZzogMnB4IDAgMnB4IDA7XHJcbiAgICBsaW5lLWhlaWdodDogMTtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItaW5mbyBoMyBzcGFuIHtcclxuICAgIGNvbG9yOiAjNGVhY2Q0O1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItaW5mbyBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlJhbGV3YXlcIiwgc2Fucy1zZXJpZjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5zb2NpYWwtbGlua3MgYSB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjkyOTI5O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBsaW5lLWhlaWdodDogMTtcclxuICAgIHBhZGRpbmc6IDhweCAwO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMzZweDtcclxuICAgIGhlaWdodDogMzZweDtcclxuICAgIHRyYW5zaXRpb246IDAuM3M7XHJcbiAgfVxyXG4gIFxyXG4gICNmb290ZXIgLmZvb3Rlci10b3AgLnNvY2lhbC1saW5rcyBhOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNmZmM0NTE7XHJcbiAgICBjb2xvcjogIzE1MTUxNTtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCBoNCB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTJweDtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCAuZm9vdGVyLWxpbmtzIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbiAgfVxyXG4gIFxyXG4gICNmb290ZXIgLmZvb3Rlci10b3AgLmZvb3Rlci1saW5rcyB1bCB7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCAuZm9vdGVyLWxpbmtzIHVsIGkge1xyXG4gICAgcGFkZGluZy1yaWdodDogMnB4O1xyXG4gICAgY29sb3I6ICNmZmM0NTE7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMTtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCAuZm9vdGVyLWxpbmtzIHVsIGxpIHtcclxuICAgIHBhZGRpbmc6IDEwcHggMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItbGlua3MgdWwgbGk6Zmlyc3QtY2hpbGQge1xyXG4gICAgcGFkZGluZy10b3A6IDA7XHJcbiAgfVxyXG4gIFxyXG4gICNmb290ZXIgLmZvb3Rlci10b3AgLmZvb3Rlci1saW5rcyB1bCBhIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgdHJhbnNpdGlvbjogMC4zcztcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAxO1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItbGlua3MgdWwgYTpob3ZlciB7XHJcbiAgICBjb2xvcjogI2ZmYzQ1MTtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuZm9vdGVyLXRvcCAuZm9vdGVyLW5ld3NsZXR0ZXIgZm9ybSB7XHJcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDZweCAxMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItbmV3c2xldHRlciBmb3JtIGlucHV0W3R5cGU9XCJlbWFpbFwiXSB7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBwYWRkaW5nOiA0cHg7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTEwcHgpO1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItbmV3c2xldHRlciBmb3JtIGlucHV0W3R5cGU9XCJzdWJtaXRcIl0ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgcmlnaHQ6IC0ycHg7XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgcGFkZGluZzogMCAyMHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmYzQ1MTtcclxuICAgIGNvbG9yOiAjMTUxNTE1O1xyXG4gICAgdHJhbnNpdGlvbjogMC4zcztcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgNHB4IDRweCAwO1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5mb290ZXItdG9wIC5mb290ZXItbmV3c2xldHRlciBmb3JtIGlucHV0W3R5cGU9XCJzdWJtaXRcIl06aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmY2Q2YjtcclxuICB9XHJcbiAgXHJcbiAgI2Zvb3RlciAuY29weXJpZ2h0IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmctdG9wOiAzMHB4O1xyXG4gIH1cclxuICBcclxuICAjZm9vdGVyIC5jcmVkaXRzIHtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gIC5sb2dve1xyXG4gICAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgfSJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FooterComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-footer',
            templateUrl: './footer.component.html',
            styleUrls: ['./footer.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "dnlu":
    /*!**************************************************!*\
      !*** ./src/app/Admin/navbar/navbar.component.ts ***!
      \**************************************************/

    /*! exports provided: NavbarComponent */

    /***/
    function dnlu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NavbarComponent", function () {
        return NavbarComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");

      var NavbarComponent = /*#__PURE__*/function () {
        function NavbarComponent(config) {
          _classCallCheck(this, NavbarComponent);

          this.sidebarOpened = false;
          config.placement = 'bottom-right';
        }

        _createClass(NavbarComponent, [{
          key: "toggleOffcanvas",
          value: function toggleOffcanvas() {
            this.sidebarOpened = !this.sidebarOpened;

            if (this.sidebarOpened) {
              document.querySelector('.sidebar-offcanvas').classList.add('active');
            } else {
              document.querySelector('.sidebar-offcanvas').classList.remove('active');
            }
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return NavbarComponent;
      }();

      NavbarComponent.ɵfac = function NavbarComponent_Factory(t) {
        return new (t || NavbarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbDropdownConfig"]));
      };

      NavbarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: NavbarComponent,
        selectors: [["app-navbar"]],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbDropdownConfig"]])],
        decls: 131,
        vars: 0,
        consts: [[1, "navbar", "default-layout", "col-lg-12", "col-12", "p-0", "fixed-top", "d-flex", "flex-row"], [1, "text-center", "navbar-brand-wrapper", "d-flex", "align-items-top", "justify-content-center"], ["href", "index.html", 1, "navbar-brand", "brand-logo"], ["src", "assets/images/logo.svg", "alt", "logo"], ["href", "index.html", 1, "navbar-brand", "brand-logo-mini"], ["src", "assets/images/logo-mini.svg", "alt", "logo"], [1, "navbar-menu-wrapper", "d-flex", "align-items-center"], [1, "navbar-nav", "navbar-nav-left", "header-links", "d-none", "d-md-flex"], [1, "nav-item"], ["href", "#", 1, "nav-link"], [1, "badge", "badge-primary", "ml-1"], [1, "nav-item", "active"], [1, "mdi", "mdi-elevation-rise"], [1, "mdi", "mdi-bookmark-plus-outline"], [1, "navbar-nav", "navbar-nav-right"], ["ngbDropdown", "", 1, "nav-item", "dropdown"], ["id", "messageDropdown", "ngbDropdownToggle", "", 1, "nav-link", "count-indicator", "dropdown-toggle"], [1, "mdi", "mdi-file-document-box"], [1, "count"], ["ngbDropdownMenu", "", "aria-labelledby", "messageDropdown", 1, "dropdown-menu", "dropdown-menu-right", "navbar-dropdown", "preview-list"], [1, "dropdown-item"], [1, "mb-0", "font-weight-normal", "float-left"], [1, "badge", "badge-info", "badge-pill", "float-right"], [1, "dropdown-divider"], [1, "dropdown-item", "preview-item"], [1, "preview-thumbnail"], ["src", "assets/images/faces/face4.jpg", "alt", "image", 1, "profile-pic"], [1, "preview-item-content", "flex-grow"], [1, "preview-subject", "ellipsis", "font-weight-medium"], [1, "float-right", "font-weight-light", "small-text"], [1, "font-weight-light", "small-text"], ["src", "assets/images/faces/face2.jpg", "alt", "image", 1, "profile-pic"], ["src", "assets/images/faces/face3.jpg", "alt", "image", 1, "profile-pic"], ["id", "notificationDropdown", "ngbDropdownToggle", "", 1, "nav-link", "count-indicator", "dropdown-toggle"], [1, "mdi", "mdi", "mdi-bell"], ["ngbDropdownMenu", "", "aria-labelledby", "notificationDropdown", 1, "dropdown-menu", "dropdown-menu-right", "navbar-dropdown", "preview-list"], [1, "badge", "badge-pill", "badge-warning", "float-right"], [1, "preview-icon", "bg-success"], [1, "mdi", "mdi-alert-circle-outline", "mx-0"], [1, "preview-item-content"], [1, "preview-subject", "font-weight-medium"], [1, "preview-icon", "bg-warning"], [1, "mdi", "mdi-tune-vertical", "mx-0"], [1, "preview-icon", "bg-info"], [1, "mdi", "mdi-email-outline", "mx-0"], ["ngbDropdown", "", 1, "nav-item", "dropdown", "d-none", "d-xl-inline-block"], ["id", "UserDropdown", "ngbDropdownToggle", "", 1, "nav-link", "dropdown-toggle"], [1, "profile-text"], ["src", "assets/images/faces/face1.jpg", "alt", "Profile image", 1, "img-xs", "rounded-circle"], ["ngbDropdownMenu", "", "aria-labelledby", "UserDropdown", 1, "dropdown-menu", "dropdown-menu-right", "navbar-dropdown"], [1, "dropdown-item", "p-0"], [1, "d-flex", "border-bottom"], [1, "py-3", "px-4", "d-flex", "align-items-center", "justify-content-center"], [1, "mdi", "mdi-bookmark-plus-outline", "mr-0", "text-gray"], [1, "py-3", "px-4", "d-flex", "align-items-center", "justify-content-center", "border-left", "border-right"], [1, "mdi", "mdi-account-outline", "mr-0", "text-gray"], [1, "mdi", "mdi-alarm-check", "mr-0", "text-gray"], [1, "dropdown-item", "mt-2"], ["type", "button", 1, "navbar-toggler", "navbar-toggler-right", "d-lg-none", "align-self-center", 3, "click"], [1, "mdi", "mdi-menu"]],
        template: function NavbarComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "img", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ul", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "li", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Schedule ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "New");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Reports");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Score");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "ul", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "li", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "span", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "7");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "You have 7 unread mails ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "span", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "View all");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "img", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h6", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "David Grey ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "span", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "1 Minutes ago");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, " The meeting is cancelled ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "a", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "img", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "h6", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Tim Cook ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "span", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "15 Minutes ago");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, " New product launch ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "a", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "img", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "h6", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, " Johnson ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "span", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "18 Minutes ago");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " Upcoming board meeting ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "li", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "a", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "i", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "span", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "p", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "You have 4 new notifications ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "span", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "View all");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "a", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "i", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "h6", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Application Error");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, " Just now ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "a", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](91, "i", 42);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "h6", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Settings");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, " Private message ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "a", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "i", 44);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "h6", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "New user registration");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "p", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, " 2 days ago ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "li", 45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "a", 46);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "span", 47);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "Hello, Richard V.Welsh !");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](111, "img", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 49);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "a", 50);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 51);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 52);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "i", 53);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 54);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "i", 55);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "div", 52);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](120, "i", 56);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "a", 57);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, " Manage Accounts ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, " Change Password ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, " Check Inbox ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "a", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, " Sign Out ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "button", 58);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_129_listener() {
              return ctx.toggleOffcanvas();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "span", 59);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["*[_ngcontent-%COMP%]{\r\n  padding: 0px;\r\n  margin: 0px;\r\n}\r\n\r\n.navbar-main[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n  height: 70px;\r\n  background-color:black;\r\n  position: fixed;\r\n}\r\n\r\n.nav-logo[_ngcontent-%COMP%]{\r\n  padding-top: 3px;\r\n  display: inline-block;\r\n  width: 160px;\r\n  \r\n  padding-left: 30px;\r\n}\r\n\r\n.nav-logo[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 130px;\r\n}\r\n\r\n.nav-data[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  float: right;\r\n  display: inline-block;\r\n  width: 240px;\r\n  height: 70px;\r\n  padding-right: 30px;\r\n  \r\n}\r\n\r\n.nav-profile-photo[_ngcontent-%COMP%]{\r\n\r\n  margin-top: 5px;\r\n  width: 60px;\r\n  height: 60px;\r\n  border-radius: 50%;\r\n  float: right;\r\n  margin-right: 30px;\r\n  \r\n}\r\n\r\n.nav-data-name[_ngcontent-%COMP%]{\r\n  padding-top: 25px;\r\n  \r\n  display: inline-block;\r\n  height:70px;\r\n  left: 100px;\r\n  color: white;\r\n  box-sizing: border-box;\r\n  \r\n  \r\n}\r\n\r\n.logout-btn[_ngcontent-%COMP%]{\r\n  margin-top:20px ;\r\n  float: right;\r\n  width: 60px;\r\n  height: 30px;\r\n\r\n  background-color: transparent;\r\n  color: white;\r\n  \r\n  border: 1px solid white;\r\n  border-radius:10px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQWRtaW4vbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLGVBQWU7QUFDakI7O0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWiw2QkFBNkI7RUFDN0Isa0JBQWtCO0FBQ3BCOztBQUNBO0VBQ0UsWUFBWTtBQUNkOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsOEJBQThCO0FBQ2hDOztBQUNBOztFQUVFLGVBQWU7RUFDZixXQUFXO0VBQ1gsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLDZCQUE2QjtBQUMvQjs7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQix3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxXQUFXO0VBQ1gsWUFBWTtFQUNaLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekIsNEJBQTRCO0FBQzlCOztBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixXQUFXO0VBQ1gsWUFBWTs7RUFFWiw2QkFBNkI7RUFDN0IsWUFBWTtFQUNaLHVDQUF1QztFQUN2Qyx1QkFBdUI7RUFDdkIsa0JBQWtCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvQWRtaW4vbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbn1cclxuXHJcbi5uYXZiYXItbWFpbntcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjpibGFjaztcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuLm5hdi1sb2dve1xyXG4gIHBhZGRpbmctdG9wOiAzcHg7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHdpZHRoOiAxNjBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTsgKi9cclxuICBwYWRkaW5nLWxlZnQ6IDMwcHg7XHJcbn1cclxuLm5hdi1sb2dvIGltZ3tcclxuICB3aWR0aDogMTMwcHg7XHJcbn1cclxuLm5hdi1kYXRhe1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHdpZHRoOiAyNDBweDtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgcGFkZGluZy1yaWdodDogMzBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiB5ZWxsb3c7ICovXHJcbn1cclxuLm5hdi1wcm9maWxlLXBob3Rve1xyXG5cclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgd2lkdGg6IDYwcHg7XHJcbiAgaGVpZ2h0OiA2MHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBmbG9hdDogcmlnaHQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAzMHB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHdoaXRlOyAqL1xyXG59XHJcbi5uYXYtZGF0YS1uYW1le1xyXG4gIHBhZGRpbmctdG9wOiAyNXB4O1xyXG4gIC8qIHBvc2l0aW9uOiBhYnNvbHV0ZTsgKi9cclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgaGVpZ2h0OjcwcHg7XHJcbiAgbGVmdDogMTAwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgLyogbWFyZ2luLWJvdHRvbTogMzBweDsgKi9cclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhOyAqL1xyXG59XHJcbi5sb2dvdXQtYnRue1xyXG4gIG1hcmdpbi10b3A6MjBweCA7XHJcbiAgZmxvYXQ6IHJpZ2h0O1xyXG4gIHdpZHRoOiA2MHB4O1xyXG4gIGhlaWdodDogMzBweDtcclxuXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHJnYig5LCAyMywgMTUxKTsgKi9cclxuICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOjEwcHg7XHJcbn1cclxuIl19 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-navbar',
            templateUrl: './navbar.component.html',
            styleUrls: ['./navbar.component.css'],
            providers: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbDropdownConfig"]]
          }]
        }], function () {
          return [{
            type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbDropdownConfig"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "enR7":
    /*!*****************************************!*\
      !*** ./src/app/Service/auth.service.ts ***!
      \*****************************************/

    /*! exports provided: AuthService */

    /***/
    function enR7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var AuthService = /*#__PURE__*/function () {
        // api=ServerDetail()
        function AuthService(http) {
          _classCallCheck(this, AuthService);

          this.http = http;
        }

        _createClass(AuthService, [{
          key: "login",
          value: function login(value) {
            var url = "/api/admin/login";
            return this.http.post(url, value);
          }
        }, {
          key: "register",
          value: function register(value) {
            var url = "/api/admin/register";
            return this.http.post(url, value);
          }
        }]);

        return AuthService;
      }();

      AuthService.ɵfac = function AuthService_Factory(t) {
        return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
      };

      AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: AuthService,
        factory: AuthService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "hdHU":
    /*!***********************************************************!*\
      !*** ./src/app/web/about-us/heading/heading.component.ts ***!
      \***********************************************************/

    /*! exports provided: HeadingComponent */

    /***/
    function hdHU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HeadingComponent", function () {
        return HeadingComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var HeadingComponent = /*#__PURE__*/function () {
        function HeadingComponent() {
          _classCallCheck(this, HeadingComponent);
        }

        _createClass(HeadingComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return HeadingComponent;
      }();

      HeadingComponent.ɵfac = function HeadingComponent_Factory(t) {
        return new (t || HeadingComponent)();
      };

      HeadingComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: HeadingComponent,
        selectors: [["app-heading"]],
        decls: 9,
        vars: 0,
        consts: [["href", "https://fonts.googleapis.com/css?family=Amaranth", "rel", "stylesheet"], [1, "container-fluid"], [1, "tm-section-intro"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-12", "col-xl-12"], [1, "tm-wrapper-center", 2, "width", "100% !important"], [1, "tm-section-intro-title", "heading"], [1, "tm-section-intro-text", "subheading"]],
        template: function HeadingComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "section", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "About Us");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Let's find more about Aqua mobile app ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: [".heading[_ngcontent-%COMP%]{\r\n    font-family:Amaranth;\r\n}\r\n.subheading[_ngcontent-%COMP%]{\r\n    font-family:Amaranth;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2Fib3V0LXVzL2hlYWRpbmcvaGVhZGluZy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSxvQkFBb0I7QUFDeEIiLCJmaWxlIjoic3JjL2FwcC93ZWIvYWJvdXQtdXMvaGVhZGluZy9oZWFkaW5nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGluZ3tcclxuICAgIGZvbnQtZmFtaWx5OkFtYXJhbnRoO1xyXG59XHJcbi5zdWJoZWFkaW5ne1xyXG4gICAgZm9udC1mYW1pbHk6QW1hcmFudGg7XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HeadingComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-heading',
            templateUrl: './heading.component.html',
            styleUrls: ['./heading.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "jwnt":
    /*!****************************************************!*\
      !*** ./src/app/web/about-us/about-us.component.ts ***!
      \****************************************************/

    /*! exports provided: AboutUsComponent */

    /***/
    function jwnt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AboutUsComponent", function () {
        return AboutUsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _heading_heading_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./heading/heading.component */
      "hdHU");

      var AboutUsComponent = /*#__PURE__*/function () {
        function AboutUsComponent() {
          _classCallCheck(this, AboutUsComponent);
        }

        _createClass(AboutUsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AboutUsComponent;
      }();

      AboutUsComponent.ɵfac = function AboutUsComponent_Factory(t) {
        return new (t || AboutUsComponent)();
      };

      AboutUsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AboutUsComponent,
        selectors: [["app-about-us"]],
        decls: 2,
        vars: 0,
        consts: [[1, "page"]],
        template: function AboutUsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-heading");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        directives: [_heading_heading_component__WEBPACK_IMPORTED_MODULE_1__["HeadingComponent"]],
        styles: ["*[_ngcontent-%COMP%]{\r\n    margin: 0;\r\n    padding: 0;\r\n    text-align: center;\r\n    font-family: \"Open Sans\",sans-serif;\r\n    box-sizing: border-box;\r\n}\r\n\r\nbody[_ngcontent-%COMP%]{\r\n    min-height: 100vh;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    background-color: whitesmoke;\r\n}\r\n\r\n.about-section[_ngcontent-%COMP%]{\r\n    background: url(water.jpg) no-repeat left;\r\n    background-size: 55%;\r\n    background-color:#4ddbff;\r\n    overflow: hidden;\r\n    padding: 100px 0;\r\n}\r\n\r\n.inner-container[_ngcontent-%COMP%]{\r\n    width: 55%;\r\n    float: right;\r\n    background-color:#4ddbff;\r\n    padding: 150px;\r\n}\r\n\r\n.inner-container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    margin-bottom: 30px;\r\n    font-size: 30px;\r\n    font-weight: 900;\r\n}\r\n\r\n.text[_ngcontent-%COMP%]{\r\n    font-size: 13px;\r\n    color: rgba(15, 15, 15, 0.637);\r\n    line-height: 30px;\r\n    text-align: justify;\r\n    margin-bottom: 40px;\r\n}\r\n\r\n.text1[_ngcontent-%COMP%]{\r\n    font-size: 13px;\r\n    color: rgba(12, 12, 12, 0.973);\r\n    line-height: 30px;\r\n    text-align: justify;\r\n    margin-bottom: 40px;\r\n}\r\n\r\n.skills[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    text-align: center;\r\n    justify-content: space-between;\r\n    font-weight: 700;\r\n    font-size: 13px;\r\n}\r\n\r\n@media screen and (max-width:1200px){\r\n    .inner-container[_ngcontent-%COMP%]{\r\n        padding: 80px;\r\n    }\r\n}\r\n\r\n@media screen and (max-width:100px){\r\n    .about-section[_ngcontent-%COMP%]{\r\n        background-size: 100%;\r\n        padding: 100px 40px;\r\n    }\r\n    .inner-container[_ngcontent-%COMP%]{\r\n        width: 100%;\r\n    }\r\n}\r\n\r\n@media screen and (max-width:600px){\r\n    .about-section[_ngcontent-%COMP%]{\r\n        padding: 0;\r\n    }\r\n    .inner-container[_ngcontent-%COMP%]{\r\n        padding: 60px;\r\n    }\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2ViL2Fib3V0LXVzL2Fib3V0LXVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxTQUFTO0lBQ1QsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixtQ0FBbUM7SUFDbkMsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsdUJBQXVCO0lBQ3ZCLDRCQUE0QjtBQUNoQzs7QUFFQTtJQUNJLHlDQUF5QztJQUN6QyxvQkFBb0I7SUFDcEIsd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsWUFBWTtJQUNaLHdCQUF3QjtJQUN4QixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxlQUFlO0lBQ2YsOEJBQThCO0lBQzlCLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLDhCQUE4QjtJQUM5QixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsOEJBQThCO0lBQzlCLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25COztBQUVBO0lBQ0k7UUFDSSxhQUFhO0lBQ2pCO0FBQ0o7O0FBQ0E7SUFDSTtRQUNJLHFCQUFxQjtRQUNyQixtQkFBbUI7SUFDdkI7SUFDQTtRQUNJLFdBQVc7SUFDZjtBQUNKOztBQUVBO0lBQ0k7UUFDSSxVQUFVO0lBQ2Q7SUFDQTtRQUNJLGFBQWE7SUFDakI7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3dlYi9hYm91dC11cy9hYm91dC11cy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIixzYW5zLXNlcmlmO1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxufVxyXG5cclxuYm9keXtcclxuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlc21va2U7XHJcbn1cclxuXHJcbi5hYm91dC1zZWN0aW9ue1xyXG4gICAgYmFja2dyb3VuZDogdXJsKHdhdGVyLmpwZykgbm8tcmVwZWF0IGxlZnQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDU1JTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzRkZGJmZjtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBwYWRkaW5nOiAxMDBweCAwO1xyXG59XHJcblxyXG4uaW5uZXItY29udGFpbmVye1xyXG4gICAgd2lkdGg6IDU1JTtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzRkZGJmZjtcclxuICAgIHBhZGRpbmc6IDE1MHB4O1xyXG59XHJcblxyXG4uaW5uZXItY29udGFpbmVyIGgxe1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA5MDA7XHJcbn1cclxuXHJcbi50ZXh0e1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgY29sb3I6IHJnYmEoMTUsIDE1LCAxNSwgMC42MzcpO1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxufVxyXG5cclxuLnRleHQxe1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgY29sb3I6IHJnYmEoMTIsIDEyLCAxMiwgMC45NzMpO1xyXG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDBweDtcclxufVxyXG5cclxuLnNraWxsc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjEyMDBweCl7XHJcbiAgICAuaW5uZXItY29udGFpbmVye1xyXG4gICAgICAgIHBhZGRpbmc6IDgwcHg7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDoxMDBweCl7XHJcbiAgICAuYWJvdXQtc2VjdGlvbntcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogMTAwcHggNDBweDtcclxuICAgIH1cclxuICAgIC5pbm5lci1jb250YWluZXJ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NjAwcHgpe1xyXG4gICAgLmFib3V0LXNlY3Rpb257XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIC5pbm5lci1jb250YWluZXJ7XHJcbiAgICAgICAgcGFkZGluZzogNjBweDtcclxuICAgIH1cclxufVxyXG4iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AboutUsComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-about-us',
            templateUrl: './about-us.component.html',
            styleUrls: ['./about-us.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "mTO2":
    /*!***********************************************************!*\
      !*** ./src/app/web/features/profile/profile.component.ts ***!
      \***********************************************************/

    /*! exports provided: ProfileComponent */

    /***/
    function mTO2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProfileComponent", function () {
        return ProfileComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ProfileComponent = /*#__PURE__*/function () {
        function ProfileComponent() {
          _classCallCheck(this, ProfileComponent);
        }

        _createClass(ProfileComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ProfileComponent;
      }();

      ProfileComponent.ɵfac = function ProfileComponent_Factory(t) {
        return new (t || ProfileComponent)();
      };

      ProfileComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ProfileComponent,
        selectors: [["app-profile"]],
        decls: 9,
        vars: 0,
        consts: [[1, "row", 2, "width", "100vw !important"], [1, "tm-2-col-img-text", "tm-2-col-img-lg-right"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-6", "col-xl-6", "tm-2-col-img"], ["src", "../../assets/img/feature-profile.png", "alt", "Image", 1, "img-fluid", 2, "width", "100% !important"], [1, "col-xs-12", "col-sm-12", "col-md-12", "col-lg-6", "col-xl-6", "tm-2-col-text"], [1, "tm-2-col-text-title"], [1, "tm-2-col-text-description"]],
        template: function ProfileComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Profile");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " This is very valuable feature to you! You can adjust your schedule changing your breakfast, lunch and dinner times, wake-up and sleeping times. Then the app suggests you a water schedule onsidering your daily schedule. It helps you to drink the daily target throughout the day ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dlYi9mZWF0dXJlcy9wcm9maWxlL3Byb2ZpbGUuY29tcG9uZW50LmNzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProfileComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-profile',
            templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "oEUC":
    /*!********************************************************!*\
      !*** ./src/app/Admin/dashboard/dashboard.component.ts ***!
      \********************************************************/

    /*! exports provided: DashboardComponent */

    /***/
    function oEUC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DashboardComponent", function () {
        return DashboardComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var DashboardComponent = /*#__PURE__*/function () {
        function DashboardComponent() {
          _classCallCheck(this, DashboardComponent);
        }

        _createClass(DashboardComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return DashboardComponent;
      }();

      DashboardComponent.ɵfac = function DashboardComponent_Factory(t) {
        return new (t || DashboardComponent)();
      };

      DashboardComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: DashboardComponent,
        selectors: [["app-dashboard"]],
        decls: 2,
        vars: 0,
        template: function DashboardComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "dashboard works!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0FkbWluL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DashboardComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.css']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "vY5A":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function vY5A(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _Admin_login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./Admin/login/login.component */
      "WpjL");
      /* harmony import */


      var _Admin_landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./Admin/landingpage/landingpage.component */
      "PVIJ");
      /* harmony import */


      var _Admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./Admin/dashboard/dashboard.component */
      "oEUC");
      /* harmony import */


      var _Admin_register_register_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./Admin/register/register.component */
      "dEXr");
      /* harmony import */


      var _web_home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./web/home/home.component */
      "6OIh");
      /* harmony import */


      var _web_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./web/about-us/about-us.component */
      "jwnt");
      /* harmony import */


      var _web_features_features_features_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./web/features/features/features.component */
      "03M9");
      /* harmony import */


      var _web_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./web/contact-us/contact-us.component */
      "WB2T");

      var routes = [{
        path: "",
        component: _web_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]
      }, {
        path: "aboutus",
        component: _web_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_7__["AboutUsComponent"]
      }, {
        path: "login",
        component: _Admin_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]
      }, {
        path: "admin",
        component: _Admin_landingpage_landingpage_component__WEBPACK_IMPORTED_MODULE_3__["LandingpageComponent"],
        children: [{
          path: '',
          redirectTo: 'dashboard',
          pathMatch: 'full'
        }, {
          path: "dashboard",
          component: _Admin_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"],
          pathMatch: 'full'
        }, {
          path: "addadmin",
          component: _Admin_register_register_component__WEBPACK_IMPORTED_MODULE_5__["RegisterComponent"]
        }]
      }, {
        path: "features",
        component: _web_features_features_features_component__WEBPACK_IMPORTED_MODULE_8__["FeaturesComponent"]
      }, {
        path: "contactus",
        component: _web_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_9__["ContactUsComponent"]
      }, {
        path: "aboutus",
        component: _web_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_7__["AboutUsComponent"]
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppRoutingModule
      });
      AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppRoutingModule_Factory(t) {
          return new (t || AppRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! hammerjs */
      "yLV6");
      /* harmony import */


      var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/

    },

    /***/
    "zn8P":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function zn8P(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "zn8P";
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map